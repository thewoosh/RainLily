The contributors of RainLily hereby acknowledge the following:

(1) Unicode and the Unicode Logo are registered trademarks of Unicode, Inc. in
    the United States and other countries.

(2) JavaScript is a trademark of Oracle and/or its affiliates.

(3) ECMAScript is a trademark of Ecma International.