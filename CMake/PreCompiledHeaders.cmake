# Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
# All Rights Reserved
#
# SPDX-License-Identifier: BSD-3-Clause

option(ENABLE_PCH "Enable Precompiled Headers" OFF)

if(ENABLE_PCH)
    target_precompile_headers(
            project_options
            INTERFACE

            "Include/spdlog.hpp"

            <map>
            <string>
            <utility>
            <vector>
    )
endif()