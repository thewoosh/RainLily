# Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
# All Rights Reserved
#
# SPDX-License-Identifier: BSD-3-Clause

set(VERSION_MAJOR 1)
set(VERSION_MINOR 2)
set(VERSION_PATCH 0)

add_compile_definitions(RAINLILY_VERSION_MAJOR=${VERSION_MAJOR}
        RAINLILY_VERSION_MINOR=${VERSION_MINOR}
        RAINLILY_VERSION_PATCH=${VERSION_PATCH}
        RAINLILY_VERSION_STRING="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
