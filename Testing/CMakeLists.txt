# Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
# All Rights Reserved
#
# SPDX-License-Identifier: BSD-3-Clause

set(TestSources
        ASTGenerator/LexicalDeclarations.cpp
        ASTGenerator/OperatorPrecedence.cpp

        ASTInterpreter/ComparisonEngine.cpp
        ASTInterpreter/RelationalExpressions.cpp

        Lexer/Punctuators.cpp
)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

foreach(TestFile ${TestSources})
    message(STATUS "${TestFile}")

    get_filename_component(TestName ${TestFile} NAME_WE)
    get_filename_component(DirectoryName ${TestFile} DIRECTORY)

    add_executable(${TestName} ${TestFile})
    set_target_properties(${TestName} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${DirectoryName})
    target_link_libraries(${TestName}
            gtest_main
            JSLibrary
            JSIOLibrary
            JSMainApp
            JSObjectsLibrary
            JSPrototypesLibrary
            JSUtilsLibrary
            project_options)

    target_compile_options(${TestName} PRIVATE ${COMPILER_DIAGNOSTICS})
    set_target_properties(${TestName} PROPERTIES FOLDER Tests)

    add_test(NAME ${TestName}
            COMMAND ${TestName})
endforeach()

