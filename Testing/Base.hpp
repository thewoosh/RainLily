/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <gtest/gtest.h>

#ifndef TESTING_BASE_DONT_CREATE_MAIN
int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
#endif // TESTING_BASE_DONT_CREATE_MAIN

using namespace std::string_literals;
using namespace std::string_view_literals;

#include "Source/Base.hpp"
#undef JS_PRIVATE_VISIBILITY
#define JS_PRIVATE_VISIBILITY public
