/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <vector>

#include "Source/Expressions/AssignmentExpression.hpp"
#include "Source/Expressions/CallExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"
#include "Source/ASTGenerator.hpp"

#include "Testing/Base.hpp"

using namespace JS;
using namespace JS::AST;

constexpr const SourceLocation g_location{};

void compareExpressions(const Expression *lhs, const Expression *rhs) {
    if (lhs == rhs && lhs == nullptr)
        return;

    ASSERT_NE(lhs, rhs);
    ASSERT_NE(lhs, nullptr);
    ASSERT_NE(rhs, nullptr);

    ASSERT_EQ(lhs->expressionType(), rhs->expressionType());
    ASSERT_NE(lhs->expressionType(), ExpressionType::INVALID);

    switch (lhs->expressionType()) {
        case ExpressionType::ASSIGNMENT: {
            const auto *left = static_cast<const AssignmentExpression *>(lhs);
            const auto *right = static_cast<const AssignmentExpression *>(rhs);
            EXPECT_EQ(left->assignmentType(), right->assignmentType());
            compareExpressions(left->lhs(), right->lhs());
            compareExpressions(left->rhs(), right->rhs());
        } [[fallthrough]];
        case ExpressionType::BI_ADD:
        case ExpressionType::BI_DIVIDE:
        case ExpressionType::BI_LEFT_SHIFT:
        case ExpressionType::BI_MULTIPLY:
        case ExpressionType::BI_REMAINDER:
        case ExpressionType::BI_RIGHT_SHIFT:
        case ExpressionType::BI_SUBTRACT:
        case ExpressionType::BI_UNSIGNED_RIGHT_SHIFT:
        {
            const auto *left = static_cast<const BiExpression *>(lhs);
            const auto *right = static_cast<const BiExpression *>(rhs);
            compareExpressions(left->lhs(), right->lhs());
            compareExpressions(left->rhs(), right->rhs());
            break;
        }
        case ExpressionType::PRIMARY_IDENTIFIER:
        case ExpressionType::PRIMARY_STRING: {
            const auto *left = static_cast<const PrimaryExpression<std::string> *>(lhs);
            const auto *right = static_cast<const PrimaryExpression<std::string> *>(rhs);
            EXPECT_EQ(left->data(), right->data());
            break;
        }
        case ExpressionType::PRIMARY_SIGNED_INT:{
            const auto *left = static_cast<const PrimaryExpression<SignedInt> *>(lhs);
            const auto *right = static_cast<const PrimaryExpression<SignedInt> *>(rhs);
            EXPECT_EQ(left->data(), right->data());
            break;
        }
        case ExpressionType::CALL_REGULAR: {
            const auto *left = static_cast<const CallExpression *>(lhs);
            const auto *right = static_cast<const CallExpression *>(rhs);
            compareExpressions(left->callee(), right->callee());
            ASSERT_EQ(std::size(left->parameters()), std::size(right->parameters()));
            for (std::size_t i = 0; i < std::size(left->parameters()); ++i) {
                compareExpressions(left->parameters()[i].get(), right->parameters()[i].get());
            }
            break;
        }
        case ExpressionType::LITERAL_NULL:
        case ExpressionType::LITERAL_FALSE:
        case ExpressionType::LITERAL_TRUE:
            break;
        default:
            ASSERT_TRUE(false);
            break;
    }
}

void compareDeclarations(const std::vector<Declaration> &lhs, const std::vector<Declaration> &rhs) {
    ASSERT_EQ(std::size(lhs), std::size(rhs));

    for (std::size_t i = 0; i < std::size(lhs); ++i) {
        EXPECT_EQ(lhs[i].identifier(), rhs[i].identifier());
        compareExpressions(lhs[i].expression(), rhs[i].expression());
    }
}

void compareNode(const Node *lhs, const Node *rhs);

void compareParentNode(const ParentNode *lhs, const ParentNode *rhs) {
    if (lhs == rhs && lhs == nullptr)
        return;

    ASSERT_NE(lhs, rhs);
    ASSERT_NE(lhs, nullptr);
    ASSERT_NE(rhs, nullptr);

    ASSERT_EQ(std::size(lhs->children()), std::size(rhs->children()));
    for (std::size_t i = 0; i < std::size(lhs->children()); ++i) {
        compareNode(lhs->children()[i].get(), rhs->children()[i].get());
    }
}

void compareNode(const Node *lhs, const Node *rhs) {
    if (lhs == rhs && lhs == nullptr)
        return;

    ASSERT_NE(lhs, rhs);
    ASSERT_NE(lhs, nullptr);
    ASSERT_NE(rhs, nullptr);

    ASSERT_EQ(lhs->nodeType(), rhs->nodeType());
    switch (lhs->nodeType()) {
        case NodeType::ROOT:
            compareParentNode(static_cast<const RootNode *>(lhs), static_cast<const RootNode *>(rhs));
            break;
        case NodeType::BLOCK:
            compareParentNode(static_cast<const BlockNode *>(lhs), static_cast<const BlockNode *>(rhs));
            break;
        case NodeType::DECLARATION_STATEMENT: {
            const auto *lhsStatement = static_cast<const DeclarationStatementNode *>(lhs);
            const auto *rhsStatement = static_cast<const DeclarationStatementNode *>(rhs);
            EXPECT_EQ(lhsStatement->declarationType(), rhsStatement->declarationType());
            compareDeclarations(lhsStatement->declarations(), rhsStatement->declarations());
        } break;
        case NodeType::EXPRESSION_STATEMENT: {
            const auto *lhsStatement = static_cast<const ExpressionStatementNode *>(lhs);
            const auto *rhsStatement = static_cast<const ExpressionStatementNode *>(rhs);
            compareExpressions(lhsStatement->expression(), rhsStatement->expression());
        } break;
        case NodeType::FOR_STATEMENT: {
            const auto *lhsStatement = static_cast<const ForStatementNode *>(lhs);
            const auto *rhsStatement = static_cast<const ForStatementNode *>(rhs);
            if (lhsStatement->declaration()) {
                ASSERT_NE(rhsStatement->declaration(), nullptr);
                ASSERT_EQ(lhsStatement->declaration()->declarationType(),
                          rhsStatement->declaration()->declarationType());
                compareDeclarations(lhsStatement->declaration()->declarations(),
                                    rhsStatement->declaration()->declarations());
            } else {
                ASSERT_NE(lhsStatement->firstExpression(), nullptr);
                ASSERT_NE(rhsStatement->firstExpression(), nullptr);
                compareExpressions(lhsStatement->firstExpression(), rhsStatement->firstExpression());
            }
            compareExpressions(lhsStatement->secondExpression(), rhsStatement->secondExpression());
            compareExpressions(lhsStatement->thirdExpression(), rhsStatement->thirdExpression());
        } break;
        case NodeType::FUNCTION_DECLARATION: {
            const auto *lhsDeclaration = static_cast<const FunctionDeclarationNode *>(lhs);
            const auto *rhsDeclaration = static_cast<const FunctionDeclarationNode *>(rhs);
            EXPECT_EQ(lhsDeclaration->functionExpression().toString(),
                      rhsDeclaration->functionExpression().toString());
            EXPECT_EQ(lhsDeclaration->functionExpression().name(),
                      rhsDeclaration->functionExpression().name());
            EXPECT_EQ(lhsDeclaration->functionExpression().parameterNames(),
                      rhsDeclaration->functionExpression().parameterNames());
            compareParentNode(lhsDeclaration, rhsDeclaration);
        } break;
        case NodeType::FUNCTION_EXPRESSION:
            ASSERT_TRUE(false);
            break;
        case NodeType::IF_STATEMENT: {
            const auto *lhsStatement = static_cast<const IfStatementNode *>(lhs);
            const auto *rhsStatement = static_cast<const IfStatementNode *>(rhs);
            compareExpressions(lhsStatement->condition(), rhsStatement->condition());
            compareNode(lhsStatement->firstStatement(), rhsStatement->firstStatement());
            compareNode(lhsStatement->secondStatement(), rhsStatement->secondStatement());
        } break;
        case NodeType::RETURN_STATEMENT:
            compareExpressions(static_cast<const ReturnStatementNode *>(lhs)->expression(),
                               static_cast<const ReturnStatementNode *>(rhs)->expression());
            break;
        case NodeType::WHILE_STATEMENT: {
            const auto *lhsStatement = static_cast<const WhileStatementNode *>(lhs);
            const auto *rhsStatement = static_cast<const WhileStatementNode *>(rhs);
            compareExpressions(lhsStatement->condition(), rhsStatement->condition());
            compareNode(lhsStatement->body(), rhsStatement->body());
        } break;
    }
}

void runASTGeneratorTests(const std::vector<Token> &input, const RootNode &expected) {
    JS::AST::Generator generator{input};
    ASSERT_TRUE(generator.generate());
    compareNode(&generator.rootNode(), &expected);
}

template <typename...T>
[[nodiscard]] inline std::vector<Declaration>
createDeclarations(T &&...data) {
    std::vector<Declaration> declarations;
    declarations.reserve(sizeof...(data));
    declarations.push_back(std::move(data...));
    return declarations;
}
