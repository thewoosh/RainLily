/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Testing/Base.hpp"

#include <array>
#include <utility>

#include "Testing/ASTGenerator/Base.hpp"

TEST(ASTGenerator, OperatorPrecedence_AddMultiply) {
    std::vector<Token> tokens{
            Token::createSignedInt(g_location, 1),
            Token::createPunctuator(g_location, PunctuatorType::ADD),
            Token::createSignedInt(g_location, 2),
            Token::createPunctuator(g_location, PunctuatorType::MULTIPLY),
            Token::createSignedInt(g_location, 3),
            Token::createPunctuator(g_location, PunctuatorType::SEMICOLON)
    };

    RootNode rootNode;
    rootNode.children().push_back(std::make_unique<ExpressionStatementNode>(
            std::make_unique<BiExpression>(ExpressionType::BI_ADD,
                std::make_unique<PrimaryExpression<SignedInt>>(1),
                std::make_unique<BiExpression>(ExpressionType::BI_MULTIPLY,
                    std::make_unique<PrimaryExpression<SignedInt>>(2),
                    std::make_unique<PrimaryExpression<SignedInt>>(3)
                )
            )
    ));

    runASTGeneratorTests(tokens, rootNode);
}

