/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Testing/Base.hpp"

#include <array>
#include <utility>

#include "Testing/ASTGenerator/Base.hpp"

TEST(ASTGenerator, LexicalDeclarations_LetUndefined) {
    std::vector<Token> tokens{
            Token::createIdentifier(g_location, "let"),
            Token::createIdentifier(g_location, "test"),
            Token::createPunctuator(g_location, PunctuatorType::SEMICOLON)
    };

    RootNode rootNode;
    rootNode.children().push_back(std::make_unique<DeclarationStatementNode>(
            DeclarationType::LEXICAL_LET,
            createDeclarations(
                    Declaration{"test"sv}
            )
    ));

    runASTGeneratorTests(tokens, rootNode);
}

TEST(ASTGenerator, LexicalDeclarations_LetWithValue) {
    std::array<std::pair<Token, std::unique_ptr<Expression>>, 3> pairs
    {{
            {Token::createSignedInt(g_location, 10), std::make_unique<PrimaryExpression<SignedInt>>(10)},
            {Token::createString(g_location, "what"), std::make_unique<PrimaryExpression<std::string>>(ExpressionType::PRIMARY_STRING, "what")},
            {Token::createIdentifier(g_location, "ident"), std::make_unique<PrimaryExpression<std::string>>(ExpressionType::PRIMARY_IDENTIFIER, "ident")},

    }};

    for (auto &pair : pairs) {
        std::vector<Token> tokens{
                Token::createIdentifier(g_location, "let"),
                Token::createIdentifier(g_location, "test"),
                Token::createPunctuator(g_location, PunctuatorType::ASSIGNMENT),
                pair.first,
                Token::createPunctuator(g_location, PunctuatorType::SEMICOLON)
        };

        RootNode rootNode;
        rootNode.children().push_back(std::make_unique<DeclarationStatementNode>(
                DeclarationType::LEXICAL_LET,
                createDeclarations(
                        Declaration{"test"sv, std::move(pair.second)}
                )
        ));

        runASTGeneratorTests(tokens, rootNode);
    }
}

