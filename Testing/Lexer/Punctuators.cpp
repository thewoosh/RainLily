/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Testing/Base.hpp"

#include <array>
#include <string_view>
#include <utility> // for std::pair

#include "Include/spdlog.hpp"

#include "Source/Enums/PunctuatorType.hpp"
#include "Source/Lexer.hpp"

TEST(LexerPunctuators, OperatorRecognition) {
    constexpr const std::array<std::pair<std::string_view, JS::PunctuatorType>, 40> values{{
             {"+", JS::PunctuatorType::ADD},
             {"-", JS::PunctuatorType::SUBTRACT},
             {"*", JS::PunctuatorType::MULTIPLY},
             {"/", JS::PunctuatorType::DIVIDE},
             {"%", JS::PunctuatorType::REMAINDER},
             {"**", JS::PunctuatorType::EXPONENT},
             {"<<", JS::PunctuatorType::LEFT_SHIFT},
             {">>", JS::PunctuatorType::RIGHT_SHIFT},
             {">>>", JS::PunctuatorType::ZERO_FILL_RIGHT_SHIFT},
             {"&", JS::PunctuatorType::BITWISE_AND},
            {"^", JS::PunctuatorType::BITWISE_XOR},
            {"|", JS::PunctuatorType::BITWISE_OR},
            {"&&", JS::PunctuatorType::LOGICAL_AND},
            {"||", JS::PunctuatorType::LOGICAL_OR},
            {"??", JS::PunctuatorType::LOGICAL_NULLISH},

            {"+=", JS::PunctuatorType::ASSIGNMENT_ADD},
            {"-=", JS::PunctuatorType::ASSIGNMENT_SUBTRACT},
            {"*=", JS::PunctuatorType::ASSIGNMENT_MULTIPLY},
            {"/=", JS::PunctuatorType::ASSIGNMENT_DIVIDE},
            {"%=", JS::PunctuatorType::ASSIGNMENT_REMAINDER},
            {"**=", JS::PunctuatorType::ASSIGNMENT_EXPONENT},
            {"<<=", JS::PunctuatorType::ASSIGNMENT_LEFT_SHIFT},
            {">>=", JS::PunctuatorType::ASSIGNMENT_RIGHT_SHIFT},
            {">>>=", JS::PunctuatorType::ASSIGNMENT_UNSIGNED_RIGHT_SHIFT},
            {"&=", JS::PunctuatorType::ASSIGNMENT_BITWISE_AND},
            {"^=", JS::PunctuatorType::ASSIGNMENT_BITWISE_XOR},
            {"|=", JS::PunctuatorType::ASSIGNMENT_BITWISE_OR},
            {"&&=", JS::PunctuatorType::ASSIGNMENT_LOGICAL_AND},
            {"||=", JS::PunctuatorType::ASSIGNMENT_LOGICAL_OR},
            {"??=", JS::PunctuatorType::ASSIGNMENT_LOGICAL_NULLISH},

            {"==", JS::PunctuatorType::EQUAL},
            {"!=", JS::PunctuatorType::NOT_EQUAL},
            {"===", JS::PunctuatorType::STRICT_EQUAL},
            {"!==", JS::PunctuatorType::STRICT_NOT_EQUAL},
            {">", JS::PunctuatorType::GREATER_THAN},
            {">=", JS::PunctuatorType::GREATER_THAN_OR_EQUAL},
            {"<", JS::PunctuatorType::LESS_THAN},
            {"<=", JS::PunctuatorType::LESS_THAN_OR_EQUAL},

            {".", JS::PunctuatorType::FULL_STOP},
            {"?." , JS::PunctuatorType::OPTIONAL_CHAINING},
    }};

    for (const auto &element : values) {
        JS::Lexer lexer{element.first};
        auto info = fmt::format("for input=\"{}\" punc={}", element.first, JS::toString(element.second));
        EXPECT_TRUE(lexer.parse()) << info;
        ASSERT_EQ(std::size(lexer.tokens()), 1) << info;
        ASSERT_EQ(lexer.tokens()[0].type(), JS::TokenType::PUNCTUATOR) << info;
        EXPECT_EQ(lexer.tokens()[0].asPunctuator(), element.second) << info;
    }
}

TEST(LexerPunctuators, PunctuatorRecognition) {
    constexpr std::array<std::pair<std::string_view, JS::PunctuatorType>, 11> values{{
        {"(", JS::PunctuatorType::LEFT_PARENTHESIS},
        {")", JS::PunctuatorType::RIGHT_PARENTHESIS},
        {"[", JS::PunctuatorType::LEFT_SQUARE_BRACKET},
        {"]", JS::PunctuatorType::RIGHT_SQUARE_BRACKET},
        {"{", JS::PunctuatorType::LEFT_CURLY_BRACKET},
        {"}", JS::PunctuatorType::RIGHT_CURLY_BRACKET},
        {",", JS::PunctuatorType::COMMA},
        {";", JS::PunctuatorType::SEMICOLON},
        {"...", JS::PunctuatorType::ELLIPSIS},
        {":", JS::PunctuatorType::COLON},
        {"!", JS::PunctuatorType::EXCLAMATION_MARK},
     }};

    for (const auto &element : values) {
        JS::Lexer lexer{element.first};
        auto info = fmt::format("for input=\"{}\" punc={}", element.first, JS::toString(element.second));
        EXPECT_TRUE(lexer.parse()) << info;
        ASSERT_EQ(std::size(lexer.tokens()), 1) << info;
        ASSERT_EQ(lexer.tokens()[0].type(), JS::TokenType::PUNCTUATOR) << info;
        EXPECT_EQ(lexer.tokens()[0].asPunctuator(), element.second) << info;
    }
}
