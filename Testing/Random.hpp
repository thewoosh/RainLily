/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <limits>
#include <random>

#include "Source/Base.hpp"
#include "Testing/Base.hpp"

#define TESTING_RANDOM_ITERATIONS 15

namespace Random {

    std::random_device device;
    std::mt19937 rng(device());
    std::uniform_int_distribution<JS::SignedInt> distribution(std::numeric_limits<JS::SignedInt>::min(),
                                                              std::numeric_limits<JS::SignedInt>::max());

    [[nodiscard]] inline JS::SignedInt
    randomSigned() noexcept {
        return distribution(rng);
    }

} // namespace Random
