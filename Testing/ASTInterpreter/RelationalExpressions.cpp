/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Testing/Base.hpp"

#include <memory>

#include "Source/AST/Node.hpp"
#include "Source/ASTInterpreter.hpp"
#include "Source/Expressions/BiExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"
#include "Source/Settings/ExtensionSettings.hpp"
#include "Source/Settings/InterpreterSettings.hpp"
#include "Testing/Random.hpp"

const JS::AST::RootNode rootNode{};
constexpr JS::ExtensionSettings extensionSettings{};
constexpr JS::InterpreterSettings interpreterSettings{};

#define DO_TEST_APPENDAGE(lhs, rhs, type) << fmt::format("lhs={}, rhs={}, type={}", lhs, rhs, #type);

#define DO_TEST(type, lhs, rhs, expected) { \
    auto expression = std::make_unique<JS::BiExpression>(JS::ExpressionType::type, \
        std::make_unique<JS::PrimaryExpression<JS::SignedInt>>(lhs), \
        std::make_unique<JS::PrimaryExpression<JS::SignedInt>>(rhs) \
    ); \
    JS::AST::Interpreter interpreter{&rootNode, &extensionSettings, &interpreterSettings}; \
    auto result = interpreter.processRelationExpression(expression.get()); \
    ASSERT_TRUE(result.first) DO_TEST_APPENDAGE(lhs, rhs, type); \
    ASSERT_EQ(result.second.index(), JS::ValueIndex::Bool) DO_TEST_APPENDAGE(lhs, rhs, type); \
    EXPECT_EQ(std::get<bool>(result.second), expected) DO_TEST_APPENDAGE(lhs, rhs, type);  \
}

TEST(RelationExpressions, LessThan) {
    DO_TEST(BI_LESS_THAN, 1, 2, true)
    DO_TEST(BI_LESS_THAN, 2, 1, false)

    for (std::size_t i = 0; i < TESTING_RANDOM_ITERATIONS; ++i) {
        const auto lhs = Random::randomSigned();
        const auto rhs = Random::randomSigned();
        DO_TEST(BI_LESS_THAN, lhs, rhs, lhs < rhs)
    }
}

TEST(RelationExpressions, LessThanOrEqual) {
    DO_TEST(BI_LESS_THAN_OR_EQUAL, 1, 2, true)
    DO_TEST(BI_LESS_THAN_OR_EQUAL, 1, 1, true)
    DO_TEST(BI_LESS_THAN_OR_EQUAL, 30, 29, false)

    for (std::size_t i = 0; i < TESTING_RANDOM_ITERATIONS; ++i) {
        const auto lhs = Random::randomSigned();
        const auto rhs = Random::randomSigned();
        DO_TEST(BI_LESS_THAN_OR_EQUAL, lhs, rhs, lhs <= rhs)
    }
}

TEST(RelationExpressions, GreaterThan) {
    DO_TEST(BI_GREATER_THAN, 2, 1, true)
    DO_TEST(BI_GREATER_THAN, 1, 2, false)

    for (std::size_t i = 0; i < TESTING_RANDOM_ITERATIONS; ++i) {
        const auto lhs = Random::randomSigned();
        const auto rhs = Random::randomSigned();
        DO_TEST(BI_GREATER_THAN, lhs, rhs, lhs > rhs)
    }
}

TEST(RelationExpressions, GreaterThanOrEqual) {
    DO_TEST(BI_GREATER_THAN_OR_EQUAL, 1, 2, false)
    DO_TEST(BI_GREATER_THAN_OR_EQUAL, 2, 1, true)
    DO_TEST(BI_GREATER_THAN_OR_EQUAL, 1, 1, true)
    DO_TEST(BI_GREATER_THAN_OR_EQUAL, 30, 29, true)

    for (std::size_t i = 0; i < TESTING_RANDOM_ITERATIONS; ++i) {
        const auto lhs = Random::randomSigned();
        const auto rhs = Random::randomSigned();
        DO_TEST(BI_GREATER_THAN_OR_EQUAL, lhs, rhs, lhs >= rhs)
    }
}
