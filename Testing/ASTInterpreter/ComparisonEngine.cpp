/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Testing/Base.hpp"

#include "Source/Interpretation/ComparisonEngine.hpp"

#define COMPARE_TRUE(lhs, rhs, leftFirst) { \
        const auto result = JS::ComparisonEngine::isLessThan(JS::Value{lhs}, JS::Value{rhs}, leftFirst); \
        ASSERT_EQ(result.index(), JS::ValueIndex::Bool); \
        EXPECT_TRUE(result.toBoolean()); \
    }

#define COMPARE_FALSE(lhs, rhs, leftFirst) { \
        const auto result = JS::ComparisonEngine::isLessThan(JS::Value{lhs}, JS::Value{rhs}, leftFirst); \
        ASSERT_EQ(result.index(), JS::ValueIndex::Bool); \
        EXPECT_FALSE(result.toBoolean()); \
    }

TEST(ComparisonEngine, LessThan) {
    COMPARE_TRUE(1, 2, true);
    COMPARE_FALSE(2, 1, true);
}
