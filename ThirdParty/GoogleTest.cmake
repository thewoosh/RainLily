# Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
# All Rights Reserved
#
# SPDX-License-Identifier: BSD-3-Clause

find_package(GTest QUIET)

if (NOT ${GTest_FOUND})
    configure_file(GoogleTest-in.cmake googletest-download/CMakeLists.txt)

    execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" -DCMAKE_CXX_COMPILER="${CMAKE_CXX_COMPILER}" .
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/googletest-download")
    execute_process(COMMAND ${CMAKE_COMMAND} --build .
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/googletest-download")

    # Prevent GoogleTest from overriding our compiler/linker options
    # when building with Visual Studio
    set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

    # Add googletest directly to our build. This adds the following targets:
    # gtest, gtest_main, gmock and gmock_main
    add_subdirectory("${CMAKE_BINARY_DIR}/googletest-src"
            "${CMAKE_BINARY_DIR}/googletest-build")
endif()
