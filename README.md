# RainLily JavaScript® Engine 
A JavaScript® library & engine.

## Building
RainLily requires [spdlog](https://github.com/gabime/spdlog) for its logging
capabilities. RainLily also requires the usage of the modern C++20. The aim is
to support all UNIX®-like platforms, and Microsoft® Windows®.

After installing spdlog system-wide, the following can be invoked to build
RainLily:
```shell
git clone https://gitlab.com/thewoosh/RainLily.git RainLily
mkdir RainLily/build
cd RainLily/build
cmake ..
cmake --build .
```

## Usage
RainLily is in the development process, which means interfacing with it may be
awkward. **JS** is a simple tool for running a script. Usage is as follows:
```shell
echo "console.log(3 * 15);" > script.js
./JS script.js
```

## Legal
This project is licensed under the terms of a
[BSD-style license](Legal/LICENSE.txt). The contributors of RainLily acknowledge
[the following](Legal/ACKNOWLEDGEMENTS.txt).
