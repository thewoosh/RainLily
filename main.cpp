/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/IO/ReadFile.hpp"

#include <functional>
#include <optional>
#include <map>

#include "Source/Base/Logger.hpp"
#include "Source/MainApp/App.hpp"
#include "Source/MainApp/ExitCodes.hpp"
#include "Source/Utils/StringUtils.hpp"

int main(int argc, const char *argv[]) {
    JS::ExtensionSettings extensionSettings{};
    JS::GeneratorSettings generatorSettings{};
    JS::InterpreterSettings interpreterSettings{};

    std::string_view fileName{};

    // AH stands for Argument Handler.
    #define AH_SET_TRUE(lhs) [&] () -> std::optional<int> { (lhs) = true; return std::nullopt; }

    const static std::map<std::string_view, std::function<std::optional<int> ()>> argumentHandler{
        {"--announce-declarations", AH_SET_TRUE(interpreterSettings.announceDeclarations)},
        {"--announce-node-result", AH_SET_TRUE(interpreterSettings.announceNodeResult)},
        {"--dump-ast", AH_SET_TRUE(generatorSettings.dumpAST)},
        {"--dump-optimized-ast", AH_SET_TRUE(generatorSettings.dumpOptimizedAST)},
        {"--dump-tokens", AH_SET_TRUE(generatorSettings.dumpTokens)},
    };

    for (std::size_t i = 1; i < static_cast<std::size_t>(argc); ++i) {
        const std::string_view arg{argv[i]};
        if (!arg.starts_with("--")) {
            if (!std::empty(fileName)) {
                LOG_ERROR("Sorry, you can only pass one file");
                return JS::ExitCodes::IncorrectUsage;
            }

            fileName = arg;
            break;
        }

        const auto it = std::find_if(std::cbegin(argumentHandler), std::end(argumentHandler), [arg] (const auto &entry) {
            return JS::StringUtils::stringEqualsIgnoreCase(entry.first, arg);
        });

        if (it == std::cend(argumentHandler)) {
            LOG_ERROR("Unknown argument: {}", arg);
            return JS::ExitCodes::IncorrectUsage;
        }

        const auto handlerResult = it->second();
        if (handlerResult)
            return handlerResult.value();
    }

    if (std::empty(fileName)) {
        LOG_ERROR("JS: No file specified");
        return JS::ExitCodes::IncorrectUsage;
    }

    JS::IO::ReadFile file{fileName};
    auto contents = file.read();
    if (!contents.second) {
        LOG_ERROR("JS: Failed to read file: {}", JS::toString(file.error()));
        return JS::ExitCodes::FailedToReadFile;
    }

    std::string_view lexerInput{std::data(contents.first), std::size(contents.first)};
    JS::MainApp app{fileName, lexerInput, extensionSettings, generatorSettings, interpreterSettings};
    return app.start();
}
