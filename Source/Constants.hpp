/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <limits>

#include <cmath>

#include "Source/Base.hpp"

namespace JS::Constants {

    constexpr const FloatType infinity = std::numeric_limits<FloatType>::infinity();
    constexpr const FloatType nan = std::numeric_limits<FloatType>::quiet_NaN();
    constexpr const FloatType pi = M_PI;

} // namespace JS::Constants
