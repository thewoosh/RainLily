/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory>
#include <string_view>

#include "Source/Object.hpp"
#include "Source/Property.hpp"

namespace JS {

    struct PropertyReference {
    private:
        // Keep the property as a reference to prevent the property pointer
        // from becoming dangling.
        std::shared_ptr<Object> m_parent;
        Property *m_property;
        std::string m_nameHint{};

    public:
        [[nodiscard]] PropertyReference() noexcept = default;

        [[nodiscard]] inline
        PropertyReference(std::shared_ptr<Object> &&parent, Property *property, std::string &&nameHint) noexcept
                : m_parent(std::move(parent))
                , m_property(property)
                , m_nameHint(std::move(nameHint)) {
        }

        [[nodiscard]] inline
        PropertyReference(const std::shared_ptr<Object> &parent, Property *property, std::string &&nameHint) noexcept
                : m_parent(parent)
                , m_property(property)
                , m_nameHint(std::move(nameHint)) {
        }

        [[nodiscard]] inline constexpr const std::string &
        nameHint() const noexcept {
            return m_nameHint;
        }

        [[nodiscard]] inline std::shared_ptr<Object> &
        parent() noexcept {
            return m_parent;
        }

        [[nodiscard]] inline const std::shared_ptr<Object> &
        parent() const noexcept {
            return m_parent;
        }

        [[nodiscard]] inline constexpr Property *
        property() noexcept {
            return m_property;
        }

        [[nodiscard]] inline constexpr const Property *
        property() const noexcept {
            return m_property;
        }
    };

} // namespace JS
