/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/MainApp/App.hpp"

#include "Source/AST/Node.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Expressions/AssignmentExpression.hpp"
#include "Source/Expressions/BiExpression.hpp"
#include "Source/Expressions/CallExpression.hpp"
#include "Source/Expressions/MemberExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"
#include "Source/MainApp/Debugger.hpp"
#include "Source/MainApp/ExitCodes.hpp"
#include "Source/Optimization/Optimizer.hpp"
#include "Source/Utils/StringUtils.hpp"
#include "Source/ASTGenerator.hpp"
#include "Source/ASTInterpreter.hpp"
#include "Source/Lexer.hpp"

namespace JS {

    void
    dumpTokenList(const std::vector<JS::Token> &tokens) noexcept {
        LOG_INFO("JS: lexer produced {} tokens", std::size(tokens));

        std::size_t accumulator{};
        for (const auto &token : tokens) {
            LOG_INFO("({}) type={}", ++accumulator, JS::toString(token.type()));
            switch (token.type()) {
                case JS::TokenType::IDENTIFIER:
                case JS::TokenType::STRING:
                    LOG_INFO("    data=\"{}\"", token.asString());
                    break;
                case JS::TokenType::INT:
                    LOG_INFO("    data=\"{}\"", token.asSigned());
                    break;
                case JS::TokenType::PUNCTUATOR:
                    LOG_INFO("    data=\"{}\"", JS::toString(token.asPunctuator()));
                    break;
                case JS::TokenType::KEYWORD:
                    LOG_INFO("    data=\"{}\"", JS::toString(token.asKeyword()));
                    break;
                default:
                    break;
            }
        }
    }

    void
    printExpression(const JS::Expression *expression, std::size_t depth=0) {
        const auto spaces = std::string(depth * 2, ' ');
        const auto prefix = fmt::format("{}Expression({})", spaces, expression == nullptr ? "null" : JS::toString(expression->expressionType()));

        if (expression == nullptr) {
            LOG_INFO(prefix);
            return;
        }

        switch (expression->expressionType()) {
            case JS::ExpressionType::PRIMARY_IDENTIFIER:
                fmt::print("{} data=\"{}\"\n", prefix,
                             static_cast<const JS::PrimaryExpression<std::string> *>(expression)->data());
                break;
            case JS::ExpressionType::PRIMARY_SIGNED_INT:
                fmt::print("{} data={}\n", prefix,
                             static_cast<const JS::PrimaryExpression<JS::SignedInt> *>(expression)->data());
                break;
            case JS::ExpressionType::PRIMARY_STRING:
                fmt::print("{} data=\"{}\"\n", prefix,
                         static_cast<const JS::PrimaryExpression<std::string> *>(expression)->data());
                break;
            case JS::ExpressionType::ASSIGNMENT: {
                const auto *assignment = static_cast<const JS::AssignmentExpression *>(expression);

                fmt::print("{} assignmentType={}\n", prefix,
                             JS::toString(assignment->assignmentType()));
                [[fallthrough]];
            }
            case JS::ExpressionType::BI_ADD:
            case JS::ExpressionType::BI_DIVIDE:
            case JS::ExpressionType::BI_LEFT_SHIFT:
            case JS::ExpressionType::BI_MULTIPLY:
            case JS::ExpressionType::BI_REMAINDER:
            case JS::ExpressionType::BI_RIGHT_SHIFT:
            case JS::ExpressionType::BI_SUBTRACT:
            case JS::ExpressionType::BI_UNSIGNED_RIGHT_SHIFT:
            case JS::ExpressionType::CALL_PROPERTY_ACCESS:
            case JS::ExpressionType::COALESCE:
            case JS::ExpressionType::LOGICAL_AND:
            case JS::ExpressionType::LOGICAL_OR:
            {
                const auto *expr = static_cast<const JS::BiExpression *>(expression);
                fmt::print("{}\n", prefix);
                printExpression(expr->lhs(), depth + 1);
                fmt::print("{}    and\n", spaces);
                printExpression(expr->rhs(), depth + 1);
                break;
            }
            case JS::ExpressionType::CALL_REGULAR: {
                const auto *expr = static_cast<const JS::CallExpression *>(expression);

                fmt::print("{}\n", prefix);
                fmt::print("{}  Callee: \n", spaces);
                printExpression(expr->callee(), depth + 2);

                fmt::print("{}  Parameters({}):\n", spaces, expr->parameters().size());

                for (const auto &parameter : expr->parameters()) {
                    printExpression(parameter.get(), depth + 2);
                }
                break;
            }
            case JS::ExpressionType::MEMBER: {
                const auto &identifiers = static_cast<const JS::MemberExpression *>(expression)->identifiers();
                fmt::print("{} path=\"{}\"\n", prefix,
                         JS::StringUtils::join(std::cbegin(identifiers), std::cend(identifiers), [](const auto &ent, bool first) {
                             if (first)
                                 return ent.identifier();
                             if (ent.optional())
                                 return fmt::format("?.{}", ent.identifier());
                             return fmt::format(".{}", ent.identifier());
                         }, "")
                );
                break;
            }
            default:
                fmt::print("{} <unknown-expression>\n", prefix);
                break;
        }
    }

    void
    printDeclaration(const JS::Declaration &declaration, std::size_t depth=0) noexcept {
        const std::string prefix(depth * 2, ' ');
        fmt::print("{}Identifier: {}\n", prefix, declaration.identifier());
        fmt::print("{}Expression:\n", prefix);
        printExpression(declaration.expression(), depth + 2);
    }

    void
    printAST(const JS::AST::Node *node, std::size_t depth=0) noexcept {
        const auto type = JS::toString(node->nodeType());
        const std::string prefix(depth * 2, ' ');

        switch (node->nodeType()) {
            case JS::AST::NodeType::DECLARATION_STATEMENT: {
                const auto *statement = static_cast<const JS::AST::DeclarationStatementNode *>(node);
                fmt::print("{}{} declarationType={}\n", prefix, type, JS::toString(statement->declarationType()));
                for (const auto &declaration : statement->declarations()) {
                    printDeclaration(declaration, depth + 1);
                }
            } break;
            case JS::AST::NodeType::EXPRESSION_STATEMENT:
                printExpression(static_cast<const JS::AST::ExpressionStatementNode *>(node)->expression(), depth);
                break;
            case JS::AST::NodeType::ROOT:
                for (const auto &child : JS_DYNAMIC_CAST<const JS::AST::ParentNode *>(node)->children()) {
                    printAST(child.get(), depth);
                }
                break;
            case JS::AST::NodeType::FUNCTION_DECLARATION: {
                const auto *declaration = static_cast<const JS::AST::FunctionDeclarationNode *>(node);
                fmt::print("{}  Name: {}\n", prefix, declaration->functionExpression().toString());
                fmt::print("{}  Parameters: {}\n", prefix, std::size(declaration->functionExpression().parameterNames()));

                for (const auto &parameter : declaration->functionExpression().parameterNames())
                    fmt::print("{}    Parameter: {}\n", prefix, parameter);

                fmt::print("{}  Body:\n", prefix);
                for (const auto &child : declaration->children()) {
                    printAST(child.get(), depth + 2);
                }
                break;
            }
            case JS::AST::NodeType::RETURN_STATEMENT:
                printExpression(static_cast<const JS::AST::ReturnStatementNode *>(node)->expression(), depth + 1);
                break;
            default:
                break;
        }
    }

    bool
    MainApp::runASTGenerator(const std::vector<Token> &tokens, AST::RootNode *outRootNode) const noexcept {
        AST::Generator generator{tokens, m_fileName, m_input};

        if (!generator.generate())
            return false;

        if (m_generatorSettings.dumpAST) {
            LOG_INFO("Pre-optimization AST:");
            printAST(&generator.rootNode());
        }

        *outRootNode = std::move(generator.rootNode());
        return true;
    }

    bool
    MainApp::runInterpreter(AST::RootNode &rootNode) noexcept {
        AST::Interpreter interpreter{&rootNode, &m_extensionSettings, &m_interpreterSettings};

        interpreter.registerDebuggerCallback([&](SourceLocation) {
            std::puts("Debugger caught 'debugger' statement");
            Debugger debugger{interpreter};
            debugger.invoke();
        });

        return interpreter.run();
    }

    bool
    MainApp::runLexer(std::vector<Token> *out) noexcept {
        Lexer lexer{m_input};

        if (!lexer.parse())
            return false;

        if (m_generatorSettings.dumpTokens)
            dumpTokenList(lexer.tokens());

        *out = std::move(lexer.tokens());
        return true;
    }

    bool
    MainApp::runOptimizer(AST::RootNode &node) const noexcept {
        AST::Optimizer optimizer{&node};

        if (!optimizer.run()) {
            LOG_ERROR("JS: failed to run optimizer");
            return false;
        }

        if (m_generatorSettings.dumpOptimizedAST)
            printAST(&node);

        return true;
    }

    int
    MainApp::start() noexcept {
        std::vector<Token> tokens;
        if (!runLexer(&tokens))
            return ExitCodes::LexerFailure;

        AST::RootNode rootNode;
        if (!runASTGenerator(tokens, &rootNode))
            return ExitCodes::ASTGeneratorFailure;

        if (!runOptimizer(rootNode))
            return ExitCodes::OptimizerFailure;

        if (!runInterpreter(rootNode))
            return ExitCodes::ASTGeneratorFailure;

        return ExitCodes::Success;
    }

} // namespace JS
