/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/ASTInterpreter.hpp"

namespace JS {

    class Debugger {
    public:
        [[nodiscard]] inline explicit
        Debugger(AST::Interpreter &interpreter) noexcept
                : m_interpreter(interpreter) {
        }

        void
        invoke() noexcept;

    private:
        AST::Interpreter &m_interpreter;

        void
        commandDisable(std::string_view) noexcept;

        void
        commandGlobalObject() noexcept;

        void
        commandQuit() noexcept;
    };

} // namespace JS
