/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/MainApp/Debugger.hpp"

#include <iostream>

#include <Include/fmt.hpp>
#include <Include/text/String.hpp>

#include "Source/Base/Logger.hpp"

namespace JS {

    inline void
    printObject(std::string_view prefix, const Object &object) noexcept {
        for (auto &property : object.properties()) {
            if (property.second.value().index() == ValueIndex::ObjectRef) {
                auto &propertyAsObject = *std::get<std::shared_ptr<Object>>(property.second.value());
                fmt::print(FMTLIB_WRAPPER("{}{} => {{\n"), prefix, property.first);
                if (propertyAsObject.value().index() != ValueIndex::Undefined) {
                    fmt::print(FMTLIB_WRAPPER("{}  <value> => {}\n"), prefix,
                               const_cast<Property &>(property.second).value().toString());
                }
                printObject(std::string(prefix) + "  ", propertyAsObject);
                continue;
            }
            fmt::print(FMTLIB_WRAPPER("{}{} => {}\n"), prefix, property.first,
                       const_cast<Property &>(property.second).value().toString());
        }
    }

    void
    Debugger::commandDisable(std::string_view) noexcept {
        m_interpreter.disableDebugger();
        std::puts("Debugger disabled. Continuing execution...");
    }

    void
    Debugger::commandGlobalObject() noexcept {
        std::puts("GlobalObject => {");
        printObject("  ", m_interpreter.globalObject());
        std::puts("}");
    }

    void
    commandHelp() noexcept {
        std::puts("List of commands:");
        std::puts("exit, q, quit, stop - Stop execution");
        std::puts("global, global-object - Dump Global Object");
        std::puts("help, ? - Show this help message");
    }

    void
    Debugger::commandQuit() noexcept {
        m_interpreter.markStopFlag();
    }

    void
    Debugger::invoke() noexcept {
        while (m_interpreter.debuggerEnabled()) {
            std::fputs("(rldb) ", stdout);

            std::string inputLine;
            std::getline(std::cin, inputLine);

            const auto line = text::trim(inputLine);
            const auto sep = std::min(line.find(' '), line.find('\t'));
            const auto command = line.substr(0, sep);

            if (command.empty())
                continue;

            if (command == "disable") {
                commandDisable(line.substr(sep + 1));
                continue;
            }

            if (text::isAnyOf(command, "q", "quit", "exit", "stop")) {
                commandQuit();
                return;
            }

            if (text::isAnyOf(command, "global", "global-object")) {
                commandGlobalObject();
                continue;
            }

            if (text::isAnyOf(command, "help", "?")) {
                commandHelp();
                continue;
            }

            std::printf("error: '%s' is not a valid command.\n", std::string(command).c_str());
        }
    }

} // namespace JS
