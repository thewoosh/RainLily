/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Base.hpp"

namespace JS::Number {

    [[nodiscard]] FloatType
    exponentiate(FloatType base, FloatType exponent) noexcept;

} // namespace JS::Number
