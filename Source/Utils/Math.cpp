/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Utils/Math.hpp"

#include <limits>
#include <random>

#include <cmath>

#include "Source/Utils/Number.hpp"
#include "Source/Constants.hpp"
#include "Source/Value.hpp"

namespace JS::Math {

    static_assert(std::numeric_limits<FloatType>::is_iec559);

    bool
    isIntegral(FloatType input) noexcept {
        return std::trunc(input) == input;
    }

    FloatType
    abs(FloatType input) noexcept {
        if (input == -0.0)
            return +0.0;

        if (input == -Constants::infinity)
            return +Constants::infinity;

        if (input < 0.0)
            return -input;

        return input;
    }

    FloatType
    acos(FloatType input) noexcept {
        if (input == Constants::nan)
            return input;

        if (input < 1.0 || input > 1.0)
            return Constants::nan;

        if (input == 1.0)
            return 0.0;

        return std::acos(static_cast<double>(input));
    }

    FloatType
    acosh(FloatType input) noexcept {
        if (input == Constants::nan || input == Constants::infinity)
            return input;

        if (input == 1.0)
            return 0.0;

        return std::acosh(static_cast<double>(input));
    }

    FloatType
    asin(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input < 1.0 || input > 1.0)
            return 0.0;

        return std::asin(static_cast<double>(input));
    }

    FloatType
    asinh(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input == +Constants::infinity || input == -Constants::infinity)
            return input;

        return std::asinh(static_cast<double>(input));
    }

    FloatType
    atan(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input == +Constants::infinity)
            return Constants::pi / 2;

        if (input == -Constants::infinity)
            return -Constants::pi / 2;

        return std::atan(static_cast<double>(input));
    }

    FloatType
    atanh(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input < 1.0 || input > 1.0)
            return Constants::nan;

        if (input == 1.0)
            return Constants::infinity;

        if (input == -1.0)
            return -Constants::infinity;

        return std::atanh(static_cast<double>(input));
    }

    FloatType
    atan2(FloatType ny, FloatType nx) noexcept {
        // 3
        if (ny == Constants::nan || nx == Constants::nan) {
            return Constants::nan;
        }

        // 4
        if (ny == +Constants::infinity) {
            if (nx == +Constants::infinity) {
                return Constants::pi / 4.0;
            }

            if (nx == -Constants::infinity) {
                return 3 * Constants::pi / 4.0;
            }

            return Constants::pi / 2.0;
        }

        // 5
        if (ny == -Constants::infinity) {
            if (nx == +Constants::infinity) {
                return -Constants::pi / 4.0;
            }

            if (nx == -Constants::infinity) {
                return -3 * Constants::pi / 4.0;
            }

            return -Constants::pi / 2.0;
        }

        // 6
        if (ny == +0.0) {
            if (nx >= +0.0) {
                return +0.0;
            }
            return Constants::pi;
        }

        // 7
        if (ny == -0.0) {
            if (nx >= +0.0) {
                return -0.0;
            }
            return -Constants::pi;
        }

        // 8 algorithm assertion
        // 9
        if (ny > +0.0) {
            if (nx == +Constants::infinity) {
                return +0.0;
            }

            if (nx == -Constants::infinity) {
                return Constants::pi;
            }

            if (nx == +0.0 || nx == -0.0)
                return Constants::pi / 2;
        }

        // 10
        if (ny < +0.0) {
            if (nx == +Constants::infinity) {
                return -0.0;
            }

            if (nx == -Constants::infinity) {
                return -Constants::pi;
            }

            if (nx == +0.0 || nx == -0.0)
                return -Constants::pi / 2;
        }

        // 11 algorithm assertion
        // 12
        return std::atan2(ny, nx);
    }

    FloatType
    cbrt(FloatType input) noexcept {
        if (input == +0.0 || input == -0.0 || input == +Constants::infinity || input == -Constants::infinity)
            return input;
        return std::cbrt(input);
    }

    FloatType
    ceil(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity || input == -Constants::infinity)
            return input;

        if (input < +0.0 && input > -1.0)
            return -0.0;

        if (isIntegral(input))
            return input;

        return std::ceil(input);
    }

    FloatType
    clz32(FloatType input) noexcept {
        auto value = static_cast<unsigned long long>(input);
#ifdef __GNUC__
        return __builtin_clzll(value);
#else
        std::uint32_t zeroCount{};

        for (std::uint32_t i = 0; i < sizeof(value) * 8; i++) {
            if (value & 1)
                break;

            ++zeroCount;
            value >>= 1;
        }

        return static_cast<FloatType>(zeroCount);
#endif
    }

    FloatType
    cos(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;
        if (input == +Constants::infinity || input == -Constants::infinity)
            return Constants::nan;

        return std::cos(input);
    }

    FloatType
    cosh(FloatType input) noexcept {
        if (input == Constants::nan || input == +Constants::infinity || input == -Constants::infinity)
            return input;
        if (input == +0.0 || input == -0.0)
            return 1.0;

        return std::cos(input);
    }

    FloatType
    exp(FloatType input) noexcept {
        if (input == Constants::nan || input == +Constants::infinity)
            return input;

        if (input == +0.0 || input == -0.0)
            return 1.0;

        if (input == -Constants::infinity)
            return +0.0;

        return std::exp(input);
    }

    FloatType
    expm1(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity)
            return input;

        if (input == -Constants::infinity)
            return -1.0;

        return std::expm1(input);
    }

    FloatType
    floor(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity || input == -Constants::infinity)
            return input;

        if (input > +0.0 && input < 1.0)
            return +0.0;

        // skip integral check, it'll be done by std::floor
        return std::floor(input);
    }

    FloatType
    fround(FloatType input) noexcept {
        return static_cast<FloatType>(static_cast<long long int>(input));
    }

    FloatType
    hypot(const std::vector<Value> &values) noexcept {
        std::vector<FloatType> coerced(std::size(values));
        std::transform(std::cbegin(values), std::cend(values), std::begin(coerced),
                       [](const Value &value) -> FloatType {
                           return value.toNumber().toFloat();
                       });

        bool onlyZero{true};
        for (FloatType value : coerced) {
            if (value == +Constants::infinity || value == -Constants::infinity)
                return +Constants::infinity;

            if (value == Constants::nan)
                return Constants::nan;

            if (value != +0.0 && value != -0.0)
                onlyZero = false;
        }

        if (onlyZero)
            return +0.0;

        /*
         * NOTE
         * ECMAScript notes:
         *     Implementations should take care to avoid the loss of precision
         *     from overflows and underflows that are prone to occur in naive
         *     implementations when this function is called with two or more
         *     arguments.
         *
         * Therefore, we use the "improved Kahan–Babuška algorithm". The
         * algorithm relies on a compiler that doesn't do floating point
         * expression associative optimization. The optimization is in turn
         * prohibited by ANSI C. We just have to make sure -Ofast isn't used —
         * at least for compiling this file.
         *
         * https://tc39.es/ecma262/multipage/numbers-and-dates.html#sec-math.hypot
         * https://en.wikipedia.org/wiki/Kahan_summation_algorithm
         */

        FloatType sum{0.0};
        FloatType c{0.0};
        for (FloatType rawInput : coerced) {
            const auto value = rawInput * rawInput;

            const auto t = sum + value;
            if (sum >= value)
                c += (sum - t) + value;
            else
                c += (value - t) + sum;
            sum = t;
        }

        return sqrt(sum + c);
    }

    FloatType
    imul(FloatType x, FloatType y) noexcept {
        constexpr const SignedInt c2_32 = 0x100000000; // 2^32
        constexpr const SignedInt c2_31 = 0x80000000; // 2^31

        const auto a = static_cast<SignedInt>(x);
        const auto b = static_cast<SignedInt>(y);
        const auto product = (a * b) % c2_32;

        if (product >= c2_31)
            return static_cast<FloatType>(product - c2_32);

        return static_cast<FloatType>(product);
    }

    FloatType
    log(FloatType input) noexcept {
        if (input == Constants::nan || input == +Constants::infinity)
            return input;

        if (input == 1.0)
            return +0.0;

        if (input == +0.0 || input == -0.0)
            return -Constants::infinity;

        if (input < +0.0)
            return Constants::nan;

        return std::log(input);
    }

    FloatType
    log10(FloatType input) noexcept {
        if (input == Constants::nan || input == +Constants::infinity)
            return input;

        if (input == 1.0)
            return +0.0;

        if (input == +0.0 || input == -0.0)
            return -Constants::infinity;

        if (input < +0.0)
            return Constants::nan;

        return std::log10(input);
    }

    FloatType
    log1p(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity)
            return input;

        if (input == -1.0)
            return -Constants::infinity;

        if (input < -1.0)
            return Constants::nan;
        return std::log1p(input);
    }

    FloatType
    log2(FloatType input) noexcept {
        if (input == Constants::nan || input == +Constants::infinity)
            return input;

        if (input == 1.0)
            return +0.0;

        if (input == +0.0 || input == -0.0)
            return -Constants::infinity;

        if (input < +0.0)
            return Constants::nan;

        return std::log2(input);
    }

    FloatType
    max(const std::vector<Value> &values) noexcept {
        std::vector<FloatType> coerced{};
        coerced.reserve(values.size());
        std::transform(std::cbegin(values), std::cend(values), std::begin(coerced),
                       [](const Value &value) -> FloatType {
                           return value.toNumber().toFloat();
                       });

        FloatType highest{-Constants::infinity};

        for (FloatType value : coerced) {
            if (value == Constants::nan)
                return Constants::nan;

            if (value == +0.0 && highest == -0.0)
                highest = +0.0;
            else if (value > highest)
                highest = value;
        }

        return highest;
    }

    FloatType
    min(const std::vector<Value> &values) noexcept {
        std::vector<FloatType> coerced{};
        coerced.reserve(values.size());
        std::transform(std::cbegin(values), std::cend(values), std::begin(coerced),
                       [](const Value &value) -> FloatType {
                           return value.toNumber().toFloat();
                       });

        FloatType lowest{+Constants::infinity};

        for (FloatType value : coerced) {
            if (value == Constants::nan)
                return Constants::nan;

            if (value == -0.0 && lowest == +0.0)
                lowest = -0.0;
            else if (value < lowest)
                lowest = value;
        }

        return lowest;
    }

    FloatType
    pow(FloatType base, FloatType exponent) noexcept {
        return Number::exponentiate(base, exponent);
    }

    FloatType
    rand() noexcept {
        static std::random_device rd;
        static std::default_random_engine engine(rd());
        static std::uniform_real_distribution<FloatType> distrib(0, 1); // [0, 1)
        return distrib(engine);
    }

    FloatType
    round(FloatType input) noexcept {
        const auto ceiled = ceil(input);
        return ceiled - (ceiled - 0.5 > ceiled);
    }

    FloatType
    sign(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;
        if (input < +0.0)
            return -1.0;
        return 1.0;
    }

    FloatType
    sin(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input == +Constants::infinity || input == -Constants::infinity)
            return Constants::nan;

        return std::sin(input);
    }

    FloatType
    sinh(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity || input == -Constants::infinity)
            return input;

        return std::sinh(input);
    }

    FloatType
    sqrt(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity)
            return input;

        if (input < +0.0)
            return Constants::nan;

        return std::sqrt(input);
    }

    FloatType
    tan(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input == +Constants::infinity || input == -Constants::infinity)
            return Constants::nan;

        return std::tan(input);
    }

    FloatType
    tanh(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0)
            return input;

        if (input == +Constants::infinity)
            return 1.0;

        if (input == -Constants::infinity)
            return -1.0;

        return std::sinh(input);
    }

    FloatType
    trunc(FloatType input) noexcept {
        if (input == Constants::nan || input == +0.0 || input == -0.0 || input == +Constants::infinity || input == -Constants::infinity)
            return input;

        if (input > +0.0 && input < 1.0)
            return +0.0;

        if (input > -1.0 && input < +0.0)
            return -0.0;

        return std::trunc(input);
    }

} // namespace JS::Math
