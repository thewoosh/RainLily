/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <sstream>
#include <string>
#include <string_view>

#include "Include/fmt.hpp"

namespace JS::StringUtils {

    template <typename Type>
    [[nodiscard]] inline std::string
    formatOrdinal(Type value) noexcept {
        switch (value) {
            case 1: return "1st";
            case 2: return "2nd";
            case 3: return "3rd";
            default: return fmt::format("{}th", value);
        }
    }

    template <typename InputIt>
    [[nodiscard]] inline std::string
    join(InputIt begin, InputIt end, std::string_view separator = ", ", std::string_view concluder = "") noexcept {
        return join(begin, end, separator, concluder, [](const auto &str) { return str; });
    }

    template <typename InputIt, typename Predicate>
    [[nodiscard]] inline std::string
    join(InputIt begin, InputIt end, Predicate pred, std::string_view separator = ", ", std::string_view concluder = "") noexcept {
        std::stringstream ss;

        if (begin != end) {
            ss << pred(*begin++, true);
        }

        while (begin != end) {
            ss << separator;
            ss << pred(*begin, false);
            ++begin;
        }

        ss << concluder;
        return ss.str();
    }

    /**
     * Makes the character lower cased if it is a ASCII capital letter (basic
     * latin A-Z), otherwise will just return the input character.
     */
    template<typename Type>
    [[nodiscard]] inline constexpr Type
    toASCIILowercase(Type c) {
        if (c >= 'a' && c <= 'z')
            return static_cast<Type>(c - ('a' - 'A'));
        return c;
    }

    [[nodiscard]] constexpr bool
    stringEqualsIgnoreCase(std::string_view a, std::string_view b) {
        if (a.length() != b.length())
            return false;
        for (std::size_t i = 0; i < a.length(); ++i) {
            if (toASCIILowercase(a[i]) != toASCIILowercase(b[i]))
                return false;
        }
        return true;
    }

} // namespace JS::StringUtils
