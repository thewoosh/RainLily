/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <vector>

#include "Source/Base.hpp"

namespace JS::Math {

    // TODO not all functions here should be passed a float, some should be
    //      passed a Real (SignedInt), e.g. imul

    [[nodiscard]] bool
    isIntegral(FloatType input) noexcept;

    [[nodiscard]] FloatType
    abs(FloatType input) noexcept;

    [[nodiscard]] FloatType
    acos(FloatType input) noexcept;

    FloatType
    acosh(FloatType input) noexcept;

    [[nodiscard]] FloatType
    asin(FloatType input) noexcept;

    [[nodiscard]] FloatType
    asinh(FloatType input) noexcept;

    [[nodiscard]] FloatType
    atan(FloatType input) noexcept;

    [[nodiscard]] FloatType
    atanh(FloatType input) noexcept;

    [[nodiscard]] FloatType
    atan2(FloatType y, FloatType x) noexcept;

    [[nodiscard]] FloatType
    cbrt(FloatType input) noexcept;

    [[nodiscard]] FloatType
    ceil(FloatType input) noexcept;

    [[nodiscard]] FloatType
    clz32(FloatType input) noexcept;

    [[nodiscard]] FloatType
    cos(FloatType input) noexcept;

    [[nodiscard]] FloatType
    cosh(FloatType input) noexcept;

    [[nodiscard]] FloatType
    exp(FloatType input) noexcept;

    [[nodiscard]] FloatType
    expm1(FloatType input) noexcept;

    [[nodiscard]] FloatType
    floor(FloatType input) noexcept;

    [[nodiscard]] FloatType
    fround(FloatType input) noexcept;

    [[nodiscard]] FloatType
    hypot(const std::vector<Value> &) noexcept;

    [[nodiscard]] FloatType
    imul(FloatType x, FloatType y) noexcept;

    [[nodiscard]] FloatType
    log(FloatType input) noexcept;

    [[nodiscard]] FloatType
    log1p(FloatType input) noexcept;

    [[nodiscard]] FloatType
    log10(FloatType input) noexcept;

    [[nodiscard]] FloatType
    log2(FloatType input) noexcept;

    [[nodiscard]] FloatType
    max(const std::vector<Value> &) noexcept;

    [[nodiscard]] FloatType
    min(const std::vector<Value> &) noexcept;

    [[nodiscard]] FloatType
    pow(FloatType base, FloatType exponent) noexcept;

    [[nodiscard]] FloatType
    rand() noexcept;

    [[nodiscard]] FloatType
    round(FloatType input) noexcept;

    [[nodiscard]] FloatType
    sign(FloatType input) noexcept;

    [[nodiscard]] FloatType
    sin(FloatType input) noexcept;

    [[nodiscard]] FloatType
    sinh(FloatType input) noexcept;

    [[nodiscard]] FloatType
    sqrt(FloatType input) noexcept;

    [[nodiscard]] FloatType
    tan(FloatType input) noexcept;

    [[nodiscard]] FloatType
    tanh(FloatType input) noexcept;

    [[nodiscard]] FloatType
    trunc(FloatType input) noexcept;

} // namespace JS::Math
