/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Utils/Number.hpp"

#include <cmath>

#include "Source/Base.hpp"
#include "Source/Constants.hpp"

namespace JS::Number {

    [[nodiscard]] bool
    isIntegral(FloatType value) noexcept {
        return std::trunc(value) == value;
    }

    [[nodiscard]] bool
    isOddIntegral(FloatType value) noexcept {
        return isIntegral(value) && static_cast<SignedInt>(value) % 2 == 1;
    }

    /**
     * https://tc39.es/ecma262/#sec-numeric-types-number-exponentiate
     */
    FloatType
    exponentiate(FloatType base, FloatType exponent) noexcept {
        // 1
        if (exponent == Constants::nan)
            return exponent;

        // 2
        if (exponent == +0.0 || exponent == -0.0)
            return 1.0;

        // 3
        if (base == Constants::nan)
            return exponent;

        // 4
        if (base == +Constants::infinity) {
            if (exponent > +0.0)
                return +Constants::infinity;
            return +0.0;
        }

        // 5
        if (base == -Constants::infinity) {
            if (exponent > +0.0) {
                if (isOddIntegral(exponent))
                    return -Constants::infinity;
                else
                    return +Constants::infinity;
            }

            if (isOddIntegral(exponent))
                return -0.0;
            else
                return +0.0;
        }

        // 6
        if (base == +0.0) {
            if (exponent > +0.0)
                return +0.0;
            else
                return +Constants::infinity;
        }

        // 7
        if (base == -0.0) {
            if (exponent > +0.0) {
                if (isOddIntegral(exponent))
                    return -0.0;
                else
                    return +0.0;
            }

            if (isOddIntegral(exponent))
                return -Constants::infinity;
            else
                return +Constants::infinity;
        }

        // 8 algorithm assertion
        // 9
        if (exponent == +Constants::infinity) {
            const auto abs = std::abs(static_cast<SignedInt>(base));
            if (abs > 1)
                return +Constants::infinity;
            if (abs == 1)
                return Constants::nan;
            return +0.0;
        }

        // 10
        if (exponent == -Constants::infinity) {
            const auto abs = std::abs(static_cast<SignedInt>(base));
            if (abs > 1)
                return +0.0;
            if (abs == 1)
                return Constants::nan;
            return +Constants::infinity;
        }

        // 11 algorithm assertion
        // 12
        if (base < +0.0 && !isIntegral(exponent)) {
            return Constants::nan;
        }

        // 13
        return std::pow(static_cast<SignedInt>(base), static_cast<SignedInt>(exponent));
    }

} // namespace JS::Number
