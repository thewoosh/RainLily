/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cstddef> // for std::size_t

#include <iterator> // for std::data, std::size

namespace JS {

    template <typename Type>
    struct ArrayView {
        Type *m_base{};
        std::size_t m_size{};

        ArrayView() noexcept = default;
        ArrayView(ArrayView &&) noexcept = default;
        ArrayView(const ArrayView &) noexcept = default;
        ArrayView &operator=(ArrayView &&) noexcept = default;
        ArrayView &operator=(const ArrayView &) noexcept = default;

        [[nodiscard]] inline constexpr
        ArrayView(Type *base, std::size_t size) noexcept
            : m_base(base)
            , m_size(size) {
        }

        template<typename T>
        [[nodiscard]] inline constexpr
        ArrayView(T &container) noexcept
                : m_base(std::data(container))
                , m_size(std::size(container)) {
        }

        [[nodiscard]] inline constexpr Type *
        data() noexcept {
            return m_base;
        }

        [[nodiscard]] inline constexpr const Type *
        data() const noexcept {
            return m_base;
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const noexcept {
            return m_size;
        }

        [[nodiscard]] inline constexpr Type &
        operator[](std::size_t index) noexcept {
            return m_base[index];
        }

        [[nodiscard]] inline constexpr const Type &
        operator[](std::size_t index) const noexcept {
            return m_base[index];
        }
    };

} // namespace JS
