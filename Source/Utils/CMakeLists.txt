# Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
# All Rights Reserved
#
# SPDX-License-Identifier: BSD-3-Clause

add_library(JSUtilsLibrary OBJECT
        Math.cpp
        Number.cpp
)
target_compile_options(JSUtilsLibrary PRIVATE ${COMPILER_DIAGNOSTICS})
target_link_libraries(JSUtilsLibrary PUBLIC project_options)
