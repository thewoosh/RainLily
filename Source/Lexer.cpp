/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Lexer.hpp"

#include <string>

#include <cstdlib> // for std::strtoll

#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Include/text/Tools.hpp"

#include "Source/Base/Logger.hpp"
#include "Source/LexerRules.hpp"

namespace JS {

    [[nodiscard]] inline constexpr bool
    isIdentifierStart(char32_t character) noexcept {
        return text::isASCIIAlpha(character)
            || character == U'_'
            || character == U'$';
    }

    bool
    Lexer::parse() noexcept {
        while (m_it != std::cend(m_input)) {
            auto character = *m_it;

            if (LexerRules::isLineSeparator(*m_it)) {
                ++m_sourceLocation.line();
                m_sourceLocation.column() = 0;
            }

            if (m_isLineComment) {
                if (character == text::LINE_FEED) {
                    m_isLineComment = false;
                }
                increasePosition(1);
                continue;
            }

            if (m_isBlockComment) {
                if (character == text::ASTERISK) {
                    if (*(m_it + 1) == text::SOLIDUS) {
                        increasePosition(2);
                        m_isBlockComment = false;
                        continue;
                    }
                }
                increasePosition(1);
                continue;
            }

            if (character == text::CARRIAGE_RETURN) {
                increasePosition(1);
                if (*m_it == text::LINE_FEED) {
                    increasePosition(1);
                }
                continue;
            }

            if (LexerRules::isLineSeparator(character)) {
                if (!std::empty(m_tokens)) {
                    m_tokens.back().markLineTerminator();
                }
                increasePosition(1);
                continue;
            }

            if (LexerRules::isWhitespace(character)) {
                increasePosition(1);
                continue;
            }

            if (character == text::SOLIDUS) {
                auto it = m_it + 1;
                if (it != std::end(m_input)) {

                    if (*it == '/') {
                        increasePosition(2);
                        m_isLineComment = true;
                        continue;
                    }

                    if (*it == '*') {
                        increasePosition(2);
                        m_isBlockComment = true;
                        continue;
                    }
                }
            }

            if (character == text::APOSTROPHE
                    || character == text::QUOTATION_MARK) {
                increasePosition(1);
                if (!consumeStringLiteral(static_cast<char32_t>(character))) {
                    return false;
                }
                continue;
            }

            if (text::isDigit(character)) {
                if (!consumeNumber()) {
                    return false;
                }
                continue;
            }

            if (isIdentifierStart(static_cast<char32_t>(character))) {
                consumeIdentifier();
                continue;
            }

            if (!consumePunctuator())
                return false;
        }

        return true;
    }

    bool
    Lexer::consumeCharacter(char32_t character) noexcept {
        if (m_it == std::cend(m_input))
            return false;

        if (static_cast<char32_t>(*m_it) != character)
            return false;

        increasePosition(1);
        return true;
    }

    void
    Lexer::consumeIdentifier() noexcept {
        const auto startLocation = m_sourceLocation;
        std::string buffer{*m_it};

        for (increasePosition(1); m_it != std::cend(m_input); increasePosition(1)) {
            if (!LexerRules::isIdentifierContinue(*m_it)) {
                break;
            }

            buffer += *m_it;
        }

        const auto keyword = convertStringToKeyword(buffer);

        if (keyword != KeywordType::INVALID) {
            m_tokens.push_back(Token::createKeyword(createLocation(startLocation), keyword));
        } else {
            m_tokens.push_back(Token::createIdentifier(createLocation(startLocation), std::move(buffer)));
        }
    }

    bool
    Lexer::consumeNumber() noexcept {
        const auto startLocation = m_sourceLocation;
        std::string buffer{*m_it};

        for (increasePosition(1); m_it != std::cend(m_input); increasePosition(1)) {
            if (!text::isDigit(*m_it)) {
                // TODO check continuing characters here; e.g. alpha chars
                //      aren't allowed.
                break;
            }

            buffer += *m_it;
        }

        char *end{nullptr};
        errno = 0;
        SignedInt integer = std::strtoll(buffer.c_str(), &end, 10);
        if (end == nullptr || *end != text::NULL_CHARACTER || errno != 0) {
            return false;
        }

        m_tokens.push_back(Token::createSignedInt(createLocation(startLocation), integer));
        return true;
    }

    bool
    Lexer::consumePunctuator() noexcept {
        const auto startLocation = m_sourceLocation;
        PunctuatorType type;
        const auto character = *m_it;
        increasePosition(1);

        switch (character) {
            case text::EXCLAMATION_MARK:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::STRICT_NOT_EQUAL;
                    } else {
                        type = PunctuatorType::NOT_EQUAL;
                    }
                } else {
                    type = PunctuatorType::EXCLAMATION_MARK;
                }
                break;
            case text::PERCENTAGE_SIGN:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_REMAINDER;
                } else {
                    type = PunctuatorType::REMAINDER;
                }
                break;
            case text::AMPERSAND:
                if (consumeCharacter(text::AMPERSAND)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::ASSIGNMENT_LOGICAL_AND;
                    } else {
                        type = PunctuatorType::LOGICAL_AND;
                    }
                } else if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_BITWISE_AND;
                } else {
                    type = PunctuatorType::BITWISE_AND;
                }
                break;
            case text::LEFT_PARENTHESIS:
                type = PunctuatorType::LEFT_PARENTHESIS;
                break;
            case text::RIGHT_PARENTHESIS:
                type = PunctuatorType::RIGHT_PARENTHESIS;
                break;
            case text::ASTERISK:
                if (consumeCharacter(text::ASTERISK)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::ASSIGNMENT_EXPONENT;
                    } else {
                        type = PunctuatorType::EXPONENT;
                    }
                } else if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_MULTIPLY;
                } else {
                    type = PunctuatorType::MULTIPLY;
                }
                break;
            case text::PLUS_SIGN:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_ADD;
                } else {
                    if (consumeCharacter(text::PLUS_SIGN)) {
                        type = PunctuatorType::INCREMENT;
                    } else {
                        type = PunctuatorType::ADD;
                    }
                }
                break;
            case text::COMMA:
                type = PunctuatorType::COMMA;
                break;
            case text::HYPHEN_MINUS:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_SUBTRACT;
                } else {
                    if (consumeCharacter(text::PLUS_SIGN)) {
                        type = PunctuatorType::DECREMENT;
                    } else {
                        type = PunctuatorType::SUBTRACT;
                    }
                }
                break;
            case text::FULL_STOP:
                if (consumeCharacter(text::FULL_STOP)) {
                    if (consumeCharacter(text::FULL_STOP)) {
                        type = PunctuatorType::ELLIPSIS;
                    } else {
                        LOG_ERROR("expected '.' or '...', not '..'");
                        return false;
                    }
                } else {
                    type = PunctuatorType::FULL_STOP;
                }
                break;
            case text::SOLIDUS:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_DIVIDE;
                } else {
                    type = PunctuatorType::DIVIDE;
                }
                break;
            case text::COLON:
                type = PunctuatorType::COLON;
                break;
            case text::SEMICOLON:
                type = PunctuatorType::SEMICOLON;
                break;
            case text::LESS_THAN_SIGN:
                if (consumeCharacter(text::LESS_THAN_SIGN)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::ASSIGNMENT_LEFT_SHIFT;
                    } else {
                        type = PunctuatorType::LEFT_SHIFT;
                    }
                } else if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::LESS_THAN_OR_EQUAL;
                } else {
                    type = PunctuatorType::LESS_THAN;
                }
                break;
            case text::EQUALS_SIGN:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::STRICT_EQUAL;
                    } else {
                        type = PunctuatorType::EQUAL;
                    }
                } else {
                    type = PunctuatorType::ASSIGNMENT;
                }
                break;
            case text::GREATER_THAN_SIGN:
                if (consumeCharacter(text::GREATER_THAN_SIGN)) {
                    if (consumeCharacter(text::GREATER_THAN_SIGN)) {
                        if (consumeCharacter(text::EQUALS_SIGN)) {
                            type = PunctuatorType::ASSIGNMENT_UNSIGNED_RIGHT_SHIFT;
                        } else {
                            type = PunctuatorType::ZERO_FILL_RIGHT_SHIFT;
                        }
                    } else {
                        if (consumeCharacter(text::EQUALS_SIGN)) {
                            type = PunctuatorType::ASSIGNMENT_RIGHT_SHIFT;
                        } else {
                            type = PunctuatorType::RIGHT_SHIFT;
                        }
                    }
                } else if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::GREATER_THAN_OR_EQUAL;
                } else {
                    type = PunctuatorType::GREATER_THAN;
                }
                break;
            case text::QUESTION_MARK:
                if (consumeCharacter(text::QUESTION_MARK)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::ASSIGNMENT_LOGICAL_NULLISH;
                    } else {
                        type = PunctuatorType::LOGICAL_NULLISH;
                    }
                } else if (std::distance(m_it, std::cend(m_input)) >= 1
                                && *m_it == text::FULL_STOP
                                && (std::distance(m_it, std::cend(m_input)) < 2 || !text::isDigit(*(m_it + 1)))) {
                    increasePosition(1); // consume the FULL STOP
                    type = PunctuatorType::OPTIONAL_CHAINING;
                } else {
                    goto def;
                }
                break;
            case text::LEFT_SQUARE_BRACKET:
                type = PunctuatorType::LEFT_SQUARE_BRACKET;
                break;
            case text::RIGHT_SQUARE_BRACKET:
                type = PunctuatorType::RIGHT_SQUARE_BRACKET;
                break;
            case text::CIRCUMFLEX_ACCENT:
                if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_BITWISE_XOR;
                } else {
                    type = PunctuatorType::BITWISE_XOR;
                }
                break;
            case text::LEFT_CURLY_BRACKET:
                type = PunctuatorType::LEFT_CURLY_BRACKET;
                break;
            case text::VERTICAL_LINE:
                if (consumeCharacter(text::VERTICAL_LINE)) {
                    if (consumeCharacter(text::EQUALS_SIGN)) {
                        type = PunctuatorType::ASSIGNMENT_LOGICAL_OR;
                    } else {
                        type = PunctuatorType::LOGICAL_OR;
                    }
                } else if (consumeCharacter(text::EQUALS_SIGN)) {
                    type = PunctuatorType::ASSIGNMENT_BITWISE_OR;
                } else {
                    type = PunctuatorType::BITWISE_OR;
                }
                break;
            case text::RIGHT_CURLY_BRACKET:
                type = PunctuatorType::RIGHT_CURLY_BRACKET;
                break;
            case text::TILDE:
                type = PunctuatorType::BITWISE_NOT;
                break;
            def:
            default:
                LOG_ERROR("Unknown character \"{}\" or {}", character, static_cast<std::uint32_t>(character));
                return false;
        }

        m_tokens.push_back(Token::createPunctuator(createLocation(startLocation), type));
        return true;
    }

    bool
    Lexer::consumeStringLiteral(char32_t enclosingCharacter) noexcept {
        const auto startLocation = m_sourceLocation;
        std::string buffer;

        while (m_it != std::end(m_input)) {
            const auto character = static_cast<char32_t>(*m_it);

            if (character == enclosingCharacter) {
                increasePosition(1);
                m_tokens.emplace_back(Token::createString(createLocation(startLocation), std::move(buffer)));
                return true;
            }

            if (character == text::SOLIDUS) {
                increasePosition(1);
                if (*m_it == text::CARRIAGE_RETURN) {
                    increasePosition(1);
                    if (m_it != std::end(m_input) && *m_it == text::LINE_FEED) {
                        increasePosition(1);
                    }
                    continue;
                }
                if (LexerRules::isLineSeparator(*m_it)) {
                    increasePosition(1);
                    continue;
                }

                if (*m_it != text::SOLIDUS) {
                    LOG_ERROR("Invalid string sequence! '{}' '{}'", static_cast<char>(character), *m_it);
                    return false;
                }
            }

            buffer += *m_it;
            increasePosition(1);
        }

        LOG_ERROR("Unexpected end of string literal");
        return false;
    }

} // namespace JS
