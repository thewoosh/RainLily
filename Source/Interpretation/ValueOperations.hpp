/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cmath>

#include "Source/Base.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Value.hpp"

namespace JS {

    namespace ValueOperations {

        constexpr const auto Plus      = [](const auto &a, const auto &b) { return a + b; };
        constexpr const auto Minus     = [](const auto &a, const auto &b) { return a - b; };
        constexpr const auto Multiply  = [](const auto &a, const auto &b) { return a * b; };
        constexpr const auto Divide    = [](const auto &a, const auto &b) { return a / b; };
        constexpr const auto Remainder = [](const auto &a, const auto &b) { return static_cast<decltype(a)>(std::remainder(a, b)); };

        constexpr const auto LeftShift  = [](const auto &a, const auto &b) {
            return static_cast<SignedInt>(a) << static_cast<SignedInt>(b);
        };

        constexpr const auto RightShift = [](const auto &a, const auto &b) {
            return static_cast<SignedInt>(a) >> static_cast<SignedInt>(b);
        };

        constexpr const auto ZeroFillRightShift = [](const auto &a, const auto &b) {
            return static_cast<UnsignedInt>(a) >> static_cast<UnsignedInt>(b);
        };

    } // namespace ValueOperations

    [[nodiscard]] inline constexpr Value &
    resolveValue(Value *value) {
        while (value->index() == ValueIndex::ObjectRef) {
            auto &obj = std::get<std::shared_ptr<Object>>(*value);
            value = &obj->value();
        }

        return *value;
    }

    [[nodiscard]] inline std::pair<bool, Value>
    doValueOperation(Value &inLhs, Value &inRhs, const auto &operation, OperationType type) noexcept {
        auto &lhs = resolveValue(&inLhs);
        auto &rhs = resolveValue(&inRhs);

        if (lhs.index() == ValueIndex::String || rhs.index() == ValueIndex::String) {
            if (type != OperationType::ADD) {
                LOG_DEBUG("ValueOp cannot do operation on std::string {}", JS::toString(type));
                return {false, JS::Undefined{}};
            }
            return {true, lhs.toString() + rhs.toString()};
        }

        auto numericLhs = lhs.toNumeric();
        auto numericRhs = rhs.toNumeric();
        if (!numericLhs.isNumeric() || !numericRhs.isNumeric()) {
            LOG_DEBUG("ValueOp cannot do operation {} on non-convertable non-numeric operands {} and {}", JS::toString(type),
                          numericLhs.toString(), numericRhs.toString());
            return {false, {}};
        }

        if (numericLhs.index() == ValueIndex::Float || numericRhs.index() == ValueIndex::Float)
            return {true, operation(numericLhs.toFloat(), numericRhs.toFloat())};

        return {true, operation(std::get<SignedInt>(numericLhs), std::get<SignedInt>(numericRhs))};
    }

    [[nodiscard]] inline std::pair<bool, Value>
    doValueOperation(Value &&lhs, Value &&rhs, const auto &operation, OperationType type) noexcept {
        return doValueOperation(lhs, rhs, operation, type);
    }

} // namespace JS
