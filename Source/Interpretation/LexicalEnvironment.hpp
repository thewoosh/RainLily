/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cassert>

#include <algorithm>
#include <map>
#include <string_view>
#include <utility> // for std::less
#include <vector>

#include "Source/Enums/LexicalScopeType.hpp"
#include "Source/Base.hpp"
#include "Source/Object.hpp"
#include "Source/Variable.hpp"

namespace JS {

    class LexicalEnvironment {
        struct Scope {
            std::map<std::string_view, Variable, std::less<>> m_data{};
            LexicalScopeType m_type;

            [[nodiscard]] inline explicit
            Scope(LexicalScopeType type) noexcept
                    : m_type(type) {
            }
        };

        std::vector<Scope> m_scopes{1,Scope{LexicalScopeType::ROOT}};

    public:
        inline Variable *
        create(VariableType type, std::string_view name, Value &&value={JS::Undefined{}}) noexcept {
            auto pair = m_scopes.back().m_data.emplace(name, Variable{type, std::move(value)});
            if (!pair.second)
                return nullptr;
            pair.first->second.nameHint(pair.first->first);
            return &pair.first->second;
        }

        /**
         * @returns nullptr if no such object was found, otherwise a non-owning
         *          ptr to the object.
         */
        [[nodiscard]] inline const Variable *
        find(std::string_view name) const noexcept {
            for (auto scopeIt = std::crbegin(m_scopes); scopeIt != std::crend(m_scopes); ++scopeIt) {
                auto objectIt = scopeIt->m_data.find(name);
                if (objectIt != std::cend(scopeIt->m_data)) {
                    return &objectIt->second;
                }
            }

            return nullptr;
        }

        [[nodiscard]] inline bool
        hasScope(LexicalScopeType lexicalScopeType) const noexcept {
            return std::any_of(std::cbegin(m_scopes), std::cend(m_scopes),
                               [lexicalScopeType] (const Scope &scope) {
                return scope.m_type == lexicalScopeType;
            });
        }

        [[nodiscard]] inline Variable *
        find(std::string_view name) noexcept {
            return const_cast<Variable *>(static_cast<const LexicalEnvironment *>(this)->find(name));
        }

        inline void
        pushScope(LexicalScopeType lexicalScopeType) noexcept {
            m_scopes.emplace_back(lexicalScopeType);
        }

        inline void
        popScope() noexcept {
            m_scopes.pop_back();
            JS_ASSERT(!std::empty(m_scopes));
        }
    };

} // namespace JS
