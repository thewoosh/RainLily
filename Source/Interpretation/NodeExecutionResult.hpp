/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace JS::AST {

    struct NodeExecutionResult {
        enum class Behavior {
            REGULAR,
            RETURN,
        };

    private:
        bool m_success;
        Value m_value;
        Behavior m_behavior{Behavior::REGULAR};

    public:
        [[nodiscard]] inline
        NodeExecutionResult(bool success, Value &&value={},
                            Behavior behavior = Behavior::REGULAR) noexcept
                : m_success(success)
                , m_value(std::move(value))
                , m_behavior(behavior) {
        }

        [[nodiscard]] inline
        NodeExecutionResult(bool success, const Value &value,
                            Behavior behavior = Behavior::REGULAR) noexcept
                : m_success(success)
                , m_value(value)
                , m_behavior(behavior) {
        }

        [[nodiscard]] inline constexpr Behavior
        behavior() const noexcept {
            return m_behavior;
        }

        [[nodiscard]] inline constexpr bool
        success() const noexcept {
            return m_success;
        }

        [[nodiscard]] inline Value &
        value() noexcept {
            return m_value;
        }

        [[nodiscard]] inline const Value &
        value() const noexcept {
            return m_value;
        }
    };

} // namespace JS::AST
