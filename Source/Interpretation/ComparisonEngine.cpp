/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Interpretation/ComparisonEngine.hpp"

namespace JS {

    /**
     * Abstract Operation: IsLessThan
     * https://tc39.es/ecma262/#sec-islessthan
     */
    Value
    ComparisonEngine::isLessThan(const Value &lhs,
                                 const Value &rhs,
                                 bool leftFirst) noexcept {
        Value px, py;
        if (leftFirst) {
            px = lhs.toPrimitive();
            py = rhs.toPrimitive();
        } else {
            px = rhs.toPrimitive();
            py = lhs.toPrimitive();
        }

        // TODO String, BigInt, etc.

        return numberIsLessThan(px, py);
    }

    /**
     * Number::isLessThan
     * https://tc39.es/ecma262/#sec-numeric-types-number-lessThan
     */
    Value
    ComparisonEngine::numberIsLessThan(const Value &lhs, const Value &rhs) noexcept {
        if (lhs.index() == rhs.index() && lhs.index() == ValueIndex::SignedInt) {
            return std::get<SignedInt>(lhs) < std::get<SignedInt>(rhs);
        }

        const auto x = lhs.toNumeric().toFloat();
        const auto y = rhs.toNumeric().toFloat();

        if (x == Constants::nan || y == Constants::nan)
            return JS::Undefined{};

        if (x == y)
            return false;

        if ((x == +0.0 && y == -0.0)
                || (x == -0.0 && y == +0.0))
            return false;

        if (x == +Constants::infinity || y == -Constants::infinity)
            return false;

        if (x == -Constants::infinity || y == +Constants::infinity)
            return true;

        return x < y;
    }

} // namespace JS
