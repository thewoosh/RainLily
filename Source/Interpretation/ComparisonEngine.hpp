/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Value.hpp"

namespace JS::ComparisonEngine {

    /**
     * Abstract Operation: IsLessThan
     * https://tc39.es/ecma262/#sec-islessthan
     */
    [[nodiscard]] Value
    isLessThan(const Value &, const Value &, bool leftFirst) noexcept;

    /**
     * Number::isLessThan
     * https://tc39.es/ecma262/#sec-numeric-types-number-lessThan
     */
    [[nodiscard]] Value
    numberIsLessThan(const Value &, const Value &) noexcept;

} // namespace JS::ComparisonEngine
