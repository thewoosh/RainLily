/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/AST/Node.hpp"
#include "Source/SourceLocation.hpp"

namespace JS::AST {

    struct DebuggerStatementNode
            : public Node {
        [[nodiscard]] inline constexpr explicit
        DebuggerStatementNode(SourceLocation sourceLocation) noexcept
                : Node(NodeType::DEBUGGER_STATEMENT)
                , m_sourceLocation(sourceLocation) {
        }

        [[nodiscard]] inline constexpr SourceLocation
        sourceLocation() const noexcept {
            return m_sourceLocation;
        }

    private:
        SourceLocation m_sourceLocation;
    };

} // JS::AST
