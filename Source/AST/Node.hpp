/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory> // for std::unique_ptr
#include <vector>

#include "Source/Enums/ASTNodeType.hpp"

namespace JS::AST {

    struct ParentNode;

    struct Node {
        NodeType m_nodeType;
        ParentNode *m_parent{nullptr};

    protected:
        [[nodiscard]] inline constexpr explicit
        Node(NodeType nodeType) noexcept
                : m_nodeType(nodeType) {
        }

    public:
        virtual ~Node() noexcept = default;

        [[nodiscard]] inline constexpr NodeType
        nodeType() const noexcept {
            return m_nodeType;
        }

        inline void
        parent(ParentNode *parentNode) noexcept {
            m_parent = parentNode;
        }

        [[nodiscard]] inline constexpr ParentNode *
        parent() noexcept {
            return m_parent;
        }

        [[nodiscard]] inline constexpr const ParentNode *
        parent() const noexcept {
            return m_parent;
        }
    };

    struct ParentNode
            : public Node {
    private:
        std::vector<std::unique_ptr<Node>> m_children{};

    public:
        [[nodiscard]] inline explicit
        ParentNode(NodeType nodeType) noexcept
                : Node(nodeType) {
        }

        [[nodiscard]] inline std::vector<std::unique_ptr<Node>> &
        children() noexcept {
            return m_children;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Node>> &
        children() const noexcept {
            return m_children;
        }

        inline Node *
        addChild(std::unique_ptr<Node> &&node) noexcept {
            auto &ref = m_children.emplace_back(std::move(node));
            ref->m_parent = this;
            return ref.get();
        }
    };

    struct RootNode
            : public ParentNode {
        [[nodiscard]] inline explicit
        RootNode() noexcept
                : ParentNode(NodeType::ROOT) {
        }
    };

    struct BlockNode
            : public ParentNode {
        [[nodiscard]] inline explicit
        BlockNode() noexcept
                : ParentNode(NodeType::BLOCK) {
        }
    };

} // namespace JS::AST
