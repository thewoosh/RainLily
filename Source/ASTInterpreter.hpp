/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <functional> // for std::function
#include <optional>
#include <utility> // for std::pair

#include "Source/Enums/DeclarationType.hpp"
#include "Source/Enums/DeclarationErrorType.hpp"
#include "Source/Enums/ExpressionType.hpp"
#include "Source/Interpretation/LexicalEnvironment.hpp"
#include "Source/Interpretation/NodeExecutionResult.hpp"
#include "Source/ExpressionResult.hpp"
#include "Source/GlobalObject.hpp"
#include "Source/PropertyOrVariable.hpp"
#include "Source/ReferenceRecord.hpp"
#include "Source/SourceLocation.hpp"
#include "Source/Value.hpp"

namespace JS::AST {

    class Interpreter {
    JS_PRIVATE_VISIBILITY:
        const ExtensionSettings *m_extensionSettings;
        const InterpreterSettings *m_interpreterSettings;

        bool m_stopFlag{false};

        const RootNode *m_rootNode;
        LexicalEnvironment m_lexicalEnvironment{};
        GlobalObject m_globalObject;

        [[nodiscard]] std::optional<bool>
        doComparison(ExpressionType, const BiExpression *) noexcept;

        [[nodiscard]] static bool
        doStrongEquality(const Value &lhs, const Value &rhs) noexcept;

        [[nodiscard]] static bool
        doLooselyEquality(const Value &lhs, const Value &rhs) noexcept;

        [[nodiscard]] static bool
        doesExpressionCreateProperty(const Expression *parent, const Expression *child) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        executeNativeFunction(Value *thisArgument,
                              const NativeFunctionDescriptor &descriptor,
                              const CallExpression *originatedFromExpression,
                              const std::vector<std::unique_ptr<Expression>> &parameters) noexcept;

        void
        explainRedeclarationError(const Node *node, std::string_view identifier, DeclarationType type,
                                  DeclarationErrorType errorType) const noexcept;

        [[nodiscard]] PropertyOrVariable
        findIdentifier(const std::string &name) noexcept;

        [[nodiscard]] DeclarationErrorType
        isDeclarable(std::string_view identifier, DeclarationType type) const noexcept;

        [[nodiscard]] ReferenceRecord
        processCallMemberExpression(const BiExpression *expression, const Expression *parentExpression) noexcept;

        [[nodiscard]] PropertyReference
        processCallPropertyAccessExpression(const BiExpression *expression, const Expression *parentExpression) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processCallRegularExpression(const CallExpression *expression) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processCoalesceExpression(const BiExpression *) noexcept;

        [[nodiscard]] bool
        processDeclaration(const DeclarationStatementNode *node, DeclarationType type,
                           const Declaration &declaration) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processFunctionExpression(const FunctionExpression *) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processLogicalExpression(const BiExpression *, bool expected) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processObjectLiteral(const ObjectLiteral *) noexcept;

        [[nodiscard]] NodeExecutionResult
        processNode(const Node *) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processRelationExpression(const BiExpression *) noexcept;

        [[nodiscard]] std::pair<bool, Value>
        processUpdateExpression(const UpdateExpression *) noexcept;

        [[nodiscard]] ReferenceRecord
        resolveMemberExpression(const MemberExpression *expression, const Expression *, std::shared_ptr<Object> *) noexcept;

    public:
        using DebuggerCallbackType = std::function<void (SourceLocation)>;

        [[nodiscard]] inline explicit
        Interpreter(const RootNode *rootNode,
                    const ExtensionSettings *extensionSettings,
                    const InterpreterSettings *interpreterSettings) noexcept
                : m_extensionSettings(extensionSettings)
                , m_interpreterSettings(interpreterSettings)
                , m_rootNode(rootNode)
                , m_globalObject(*m_extensionSettings) {
        }

        [[nodiscard]] bool
        run() noexcept;

        [[nodiscard]] inline constexpr bool
        debuggerEnabled() const noexcept {
            return m_debuggerEnabled;
        }

        inline constexpr void
        disableDebugger() noexcept {
            m_debuggerEnabled = false;
        }

        [[nodiscard]] ExpressionResult
        executeExpression(const Expression *, const Expression *) noexcept;

        [[nodiscard]] inline constexpr bool
        hasStopFlag() const noexcept {
            return m_stopFlag;
        }

        [[nodiscard]] inline LexicalEnvironment &
        lexicalEnvironment() noexcept {
            return m_lexicalEnvironment;
        }

        [[nodiscard]] inline const LexicalEnvironment &
        lexicalEnvironment() const noexcept {
            return m_lexicalEnvironment;
        }

        inline void
        markStopFlag() noexcept {
            m_stopFlag = true;
        }

        [[nodiscard]] inline GlobalObject &
        globalObject() noexcept {
            return m_globalObject;
        }

        [[nodiscard]] inline const GlobalObject &
        globalObject() const noexcept {
            return m_globalObject;
        }

        inline void
        registerDebuggerCallback(DebuggerCallbackType &&debuggerCallback) noexcept {
            m_debuggerCallback = std::move(debuggerCallback);
        }

    private:
        DebuggerCallbackType m_debuggerCallback{};

        bool m_debuggerEnabled{true};
    };

} // namespace JS::AST
