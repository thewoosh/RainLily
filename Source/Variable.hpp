/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

#include "Source/Enums/VariableType.hpp"
#include "Source/Value.hpp"

namespace JS {

    struct Variable {
    private:
        VariableType m_type;
        Value m_value;
        std::string_view m_nameHint{"<unresolved-hint>"};

    public:
        [[nodiscard]] Variable(Variable &&) noexcept = default;
        [[nodiscard]] Variable(const Variable &) noexcept = default;
        Variable &operator=(Variable &&) noexcept = default;
        Variable &operator=(const Variable &) noexcept = default;

        [[nodiscard]] inline
        Variable(VariableType type, Value &&value) noexcept
                : m_type(type)
                , m_value(std::move(value)) {
        }

        [[nodiscard]] inline
        Variable(VariableType type, const Value &value) noexcept
                : m_type(type)
                , m_value(value) {
        }

        [[nodiscard]] inline constexpr std::string_view
        nameHint() const noexcept {
            return m_nameHint;
        }

        inline constexpr void
        nameHint(std::string_view nameHint) noexcept {
            m_nameHint = nameHint;
        }

        [[nodiscard]] inline constexpr VariableType
        type() const noexcept {
            return m_type;
        }

        [[nodiscard]] inline Value &
        value() noexcept {
            return m_value;
        }

        [[nodiscard]] inline const Value &
        value() const noexcept {
            return m_value;
        }

        [[nodiscard]] inline bool
        value(Value &&value) noexcept {
            m_value = std::move(value);
            return true;
        }

        [[nodiscard]] inline bool
        value(const Value &value) noexcept {
            m_value = value;
            return true;
        }

    };

} // namespace JS
