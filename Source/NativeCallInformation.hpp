/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Base.hpp"
#include "Source/Value.hpp"

namespace JS {

    struct NativeCallInformation {
    private:
        GlobalObject *m_globalObject;
        Value &m_thisArgument;

    public:
        [[nodiscard]] inline constexpr
        NativeCallInformation(GlobalObject *globalObject, Value &thisArgument) noexcept
                : m_globalObject(globalObject)
                , m_thisArgument(thisArgument) {
        }

        [[nodiscard]] inline constexpr GlobalObject *
        globalObject() noexcept {
            return m_globalObject;
        }

        [[nodiscard]] inline constexpr const GlobalObject *
        globalObject() const noexcept {
            return m_globalObject;
        }

        [[nodiscard]] inline constexpr Value &
        thisArgument() noexcept {
            return m_thisArgument;
        }

        [[nodiscard]] inline constexpr const Value &
        thisArgument() const noexcept {
            return m_thisArgument;
        }
    };

} // namespace JS
