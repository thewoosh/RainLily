/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string>
#include <vector>

#include "Source/Base/Logger.hpp"

namespace JS {

    struct ErrorStack {
    private:
        std::vector<std::vector<std::string>> m_data;

    public:
        inline void
        reset() noexcept {
            m_data.clear();
        }

        inline void
        print() noexcept {
            for (const auto &item : m_data) {
                for (const auto &string : item) {
                    LOG_ERROR(string);
                }
            }

            reset();
        }

        inline void
        push(std::vector<std::string> &&item) noexcept {
            m_data.push_back(std::move(item));
        }
    };

} // namespace JS
