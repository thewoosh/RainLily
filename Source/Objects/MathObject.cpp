/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Objects/MathObject.hpp"

// TODO when C++20s <numbers> gets supported, use those instead.
//      https://en.cppreference.com/w/cpp/header/numbers
#include <cmath>

#include "Source/Objects/CommonHelper.hpp"
#include "Source/Utils/Math.hpp"
#include "Source/NativeCallInformation.hpp"

namespace JS::Objects {

    void
    MathObject::addFunctionProperties() noexcept {
#define ADD_FUNCTIONA(name, suggestedArgumentCount) \
        addNativeFunction(this, #name, NativeFunctionDescriptor{ \
                suggestedArgumentCount, "Math::"#name, [](NativeCallInformation, const std::vector<Value> &parameters) -> Value { \
                    return Math::name(parameters); \
                } \
        });

#define ADD_FUNCTIONW(name) \
        addNativeFunction(this, #name, NativeFunctionDescriptor{ \
                0, "Math::"#name, [](NativeCallInformation, const std::vector<Value> &) -> Value { \
                    return Math::name(); \
                } \
        });

#define ADD_FUNCTION1(name) \
        addNativeFunction(this, #name, NativeFunctionDescriptor{ \
                1, "Math::"#name, [](NativeCallInformation, const std::vector<Value> &parameters) -> Value { \
                    return Math::name(getParameter(parameters, 0).toNumber().toFloat()); \
                } \
        });

#define ADD_FUNCTION2(name) \
        addNativeFunction(this, #name, NativeFunctionDescriptor{ \
                2, "Math::"#name, [](NativeCallInformation, const std::vector<Value> &parameters) -> Value { \
                    return Math::name(getParameter(parameters, 0).toNumber().toFloat(), \
                                      getParameter(parameters, 1).toNumber().toFloat()); \
                } \
        });

        ADD_FUNCTION1(abs)
        ADD_FUNCTION1(acos)
        ADD_FUNCTION1(acosh)
        ADD_FUNCTION1(asin)
        ADD_FUNCTION1(asinh)
        ADD_FUNCTION1(atan)
        ADD_FUNCTION1(atanh)
        ADD_FUNCTION2(atan2)
        ADD_FUNCTION1(cbrt)
        ADD_FUNCTION1(clz32)
        ADD_FUNCTION1(cos)
        ADD_FUNCTION1(cosh)
        ADD_FUNCTION1(exp)
        ADD_FUNCTION1(expm1)
        ADD_FUNCTION1(floor)
        ADD_FUNCTIONA(hypot, 2)
        ADD_FUNCTION2(imul)
        ADD_FUNCTION1(log)
        ADD_FUNCTION1(log1p)
        ADD_FUNCTION1(log10)
        ADD_FUNCTION1(log2)
        ADD_FUNCTIONA(max, 2)
        ADD_FUNCTIONA(min, 2)
        ADD_FUNCTION2(pow)
        ADD_FUNCTIONW(rand)
        ADD_FUNCTION1(round)
        ADD_FUNCTION1(sign)
        ADD_FUNCTION1(sin)
        ADD_FUNCTION1(sinh)
        ADD_FUNCTION1(sqrt)
        ADD_FUNCTION1(tan)
        ADD_FUNCTION1(tanh)
        ADD_FUNCTION1(trunc)
    }

    void
    MathObject::addValueProperties() noexcept {
        addFloatConstant(this, "E", M_E);
        addFloatConstant(this, "LN10", M_LN10);
        addFloatConstant(this, "LN2", M_LN2);
        addFloatConstant(this, "LOG10E", M_LOG10E);
        addFloatConstant(this, "LOG2E", M_LOG2E);
        addFloatConstant(this, "PI", M_PI);
        addFloatConstant(this, "SQRT1_2", M_SQRT1_2);
        addFloatConstant(this, "SQRT2", M_SQRT2);
    }

    MathObject::MathObject() noexcept {
        addFunctionProperties();
        addValueProperties();
    }

} // namespace JS::Objects
