/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * https://console.spec.whatwg.org/
 */

#include "Source/Objects/ConsoleObject.hpp"


#include "Source/Base.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/NativeCallInformation.hpp"
#include "Source/Objects/CommonHelper.hpp"

namespace JS::Objects {

    inline void
    log(std::string_view logLevel, const std::vector<Value> &parameters) noexcept {
        std::string line;

        for (auto parameter : parameters) {
            if (!std::empty(line))
                line += ' ';
            line += parameter.toString();
        }

        LOG_INFO("Console[{}] {}", logLevel, line);
    }

    ConsoleObject::ConsoleObject() noexcept {
        // https://console.spec.whatwg.org/#console-namespace
        // The prototype is from Object, not Prototype
        addBuiltinConstant(this, "prototype", std::make_shared<Object>());

        addNativeFunction(this, "debug", NativeFunctionDescriptor{
            0, "console.debug",
            [](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                log("debug", parameters);
                return JS::Undefined{};
            }
        });

        addNativeFunction(this, "error", NativeFunctionDescriptor{
            0, "console.error",
            [](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                log("error", parameters);
                return JS::Undefined{};
            }
        });

        addNativeFunction(this, "info", NativeFunctionDescriptor{
                0, "console.info",
                [](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                    log("info", parameters);
                    return JS::Undefined{};
                }
        });

        addNativeFunction(this, "log", NativeFunctionDescriptor{
            0, "console.log",
            [](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                log("log", parameters);
                return JS::Undefined{};
            }
        });
    }

} // namespace JS::Objects
