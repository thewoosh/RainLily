/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Object.hpp"

namespace JS::Objects {

    class MathObject : public Object {
        void
        addFunctionProperties() noexcept;

        void
        addValueProperties() noexcept;

    public:
        [[nodiscard]]
        MathObject() noexcept;
    };

} // namespace JS::Objects
