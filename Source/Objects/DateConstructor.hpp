/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once


#include <string>

#include "Source/Base.hpp"
#include "Source/Object.hpp"

namespace JS::Objects {

    class DateConstructor : public Object {
    public:
        [[nodiscard]] static SignedInt
        dateNow() noexcept;

        [[nodiscard]]
        DateConstructor() noexcept;
    };

} // namespace JS::Objects
