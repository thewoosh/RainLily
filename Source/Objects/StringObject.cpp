/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Objects/StringObject.hpp"

#include "Source/Objects/CommonHelper.hpp"
#include "Source/Base.hpp"
#include "Source/GlobalObject.hpp"

namespace JS::Objects {

    void
    StringObject::addFunctionProperties(GlobalObject *globalObject) noexcept {
        const auto &stringClass = std::get<std::shared_ptr<Object>>(globalObject->findProperty("String")->value());
        const auto &stringPrototype = *std::get<std::shared_ptr<Object>>(stringClass->findProperty("prototype")->value());

        for (const auto &property : stringPrototype.properties()) {
            addProperty(property.first, property.second);
        }
    }

    void
    StringObject::addValueProperties(const std::string &data) noexcept {
        addBuiltinConstant(this, "length", static_cast<SignedInt>(data.length()));
    }

    StringObject::StringObject(GlobalObject *globalObject, std::string &&string) noexcept {
        auto result = value(std::move(string));
        JS_ASSERT(result);
        JS_DISCARD(result);

        addFunctionProperties(globalObject);
        addValueProperties(std::get<std::string>(value()));
    }

    StringObject::StringObject(GlobalObject *globalObject, const std::string &string) noexcept {
        JS_ASSERT(value(string));

        addFunctionProperties(globalObject);
        addValueProperties(string);
    }

} // namespace JS::Objects
