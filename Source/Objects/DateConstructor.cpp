/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Objects/DateConstructor.hpp"

#include <chrono>

#include "Source/Objects/CommonHelper.hpp"
#include "Source/Base.hpp"
#include "Source/NativeCallInformation.hpp"
#include "Source/GlobalObject.hpp"

#ifdef __unix__
#   include <sys/time.h>
#elif defined(_WIN32)
#   error "TODO"
#else
#   include <ctime>
#endif

namespace JS::Objects {

    SignedInt
    DateConstructor::dateNow() noexcept {
        // TODO When std::chrono::utc_clock is supported by MSVC, Clang, and
        //      GCC, use that instead.
        // std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::utc_clock::now().time_since_epoch()).count();

#ifdef __unix__
        struct timeval tv{};
        struct timezone tz{};

        const auto result = gettimeofday(&tv, &tz);
        JS_ASSERT(result == 0);
        JS_DISCARD(result);

        return (static_cast<SignedInt>(tv.tv_sec) * 1000)
            + (static_cast<SignedInt>(tv.tv_usec) / 1000);
#else
#   error "TODO"
#endif
    }

    DateConstructor::DateConstructor() noexcept {
        addNativeFunction(this, "now", NativeFunctionDescriptor{
            0,
            "Date.now",
            [] (NativeCallInformation, const std::vector<Value> &) -> Value {
                return dateNow();
            }
        });
    }

} // namespace JS::Objects
