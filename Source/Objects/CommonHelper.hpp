/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory> // for std::make_unique
#include <string_view>
#include <vector>

#include "Source/Base.hpp" // for FloatType
#include "Source/Object.hpp"

namespace JS::Objects {

    // NOTE: not a const-constant
    inline void
    addBuiltinConstant(Object *object, std::string_view name, Value &&value) noexcept {
        object->addProperty(name, Property{std::move(value)})
                .setConfigurable(false)
                .setEnumerable(false)
                .setWritable(false);
    }

    // NOTE: not a const-constant
    inline void
    addNativeFunction(Object *object, std::string_view name, NativeFunctionDescriptor &&value) noexcept {
        const auto suggestedArgumentCount{static_cast<SignedInt>(value.argumentCount())};

        auto subObject = std::make_shared<Object>(std::move(value));
        subObject->addProperty("length", Property{suggestedArgumentCount});

        auto &property = object->addProperty(name, Property{std::move(subObject)});
        property.setConfigurable(true)
                .setEnumerable(false)
                .setWritable(false);
    }

    template <typename T>
    inline void
    addFloatConstant(Object *object, std::string_view name, T value) noexcept {
        addBuiltinConstant(object, name, static_cast<FloatType>(value));
    }

    [[nodiscard]] inline Value
    getParameter(const std::vector<Value> &parameters, std::size_t index) noexcept {
        if (std::size(parameters) > index)
            return parameters[index];
        return JS::Undefined{};
    }

} // namespace JS::Objects
