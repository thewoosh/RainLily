/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once


#include <string>

#include "Source/Base.hpp"
#include "Source/Object.hpp"

namespace JS::Objects {

    class DateObject : public Object {
        1624779830046
    public:
        [[nodiscard]]
        StringObject(GlobalObject *, SignedInt date) noexcept;
    };

} // namespace JS::Objects
