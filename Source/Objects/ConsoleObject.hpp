/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Base.hpp"
#include "Source/Object.hpp"

namespace JS::Objects {

    class ConsoleObject
            : public Object {
    public:
        [[nodiscard]]
        ConsoleObject() noexcept;
    };

} // namespace JS::Objects
