/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string>
#include <variant>

#include "Include/fmt.hpp"

#include "Source/Enums/KeywordType.hpp"
#include "Source/Enums/PunctuatorType.hpp"
#include "Source/Enums/TokenType.hpp"
#include "Source/Base.hpp"
#include "Source/SourceLocation.hpp"

namespace JS {

    struct Token {
    private:
        SourceLocation m_sourceLocation;
        TokenType m_type;
        std::variant<std::string, PunctuatorType, SignedInt, KeywordType> m_data;
        bool m_isLineTerminator{false};

    public:
        template <typename...T>
        [[nodiscard]] inline explicit
        Token(SourceLocation location, TokenType type, T &&...data) noexcept
                : m_sourceLocation(location)
                , m_type(type)
                , m_data(std::forward<T>(data)...) {
            static_assert(sizeof...(T) > 0);
        }

        [[nodiscard]] inline constexpr KeywordType
        asKeyword() const noexcept {
            return std::get<KeywordType>(m_data);
        }

        [[nodiscard]] inline constexpr const std::string &
        asString() const noexcept {
            return std::get<std::string>(m_data);
        }

        [[nodiscard]] inline constexpr PunctuatorType
        asPunctuator() const noexcept {
            return std::get<PunctuatorType>(m_data);
        }

        [[nodiscard]] inline constexpr SignedInt
        asSigned() const noexcept {
            return std::get<SignedInt>(m_data);
        }

        [[nodiscard]] inline static Token
        createKeyword(SourceLocation sourceLocation, KeywordType data) noexcept {
            return Token{sourceLocation, TokenType::KEYWORD, data};
        }

        [[nodiscard]] inline static Token
        createIdentifier(SourceLocation sourceLocation, std::string &&data) noexcept {
            return Token{sourceLocation, TokenType::IDENTIFIER, std::move(data)};
        }

        [[nodiscard]] inline static Token
        createPunctuator(SourceLocation sourceLocation, PunctuatorType data) noexcept {
            return Token{sourceLocation, TokenType::PUNCTUATOR, data};
        }

        [[nodiscard]] inline static Token
        createSignedInt(SourceLocation sourceLocation, SignedInt data) noexcept {
            return Token{sourceLocation, TokenType::INT, data};
        }

        [[nodiscard]] inline static Token
        createString(SourceLocation sourceLocation, std::string &&data) noexcept {
            return Token{sourceLocation, TokenType::STRING, std::move(data)};
        }

        [[nodiscard]] inline constexpr bool
        isLineTerminator() const noexcept {
            return m_isLineTerminator;
        }

        inline void
        markLineTerminator() noexcept {
            m_isLineTerminator = true;
        }

        [[nodiscard]] inline constexpr SourceLocation
        sourceLocation() const noexcept {
            return m_sourceLocation;
        }

        [[nodiscard]] inline std::string
        toString() const noexcept {
            switch (m_type) {
                case TokenType::KEYWORD:
                    return fmt::format("keyword({})", JS::toString(asKeyword()));
                case TokenType::IDENTIFIER:
                    return fmt::format("identifier({})", asString());
                case TokenType::INT:
                    return fmt::format("signed-int({})", asSigned());
                case TokenType::PUNCTUATOR:
                    return fmt::format("punctuator({})", JS::toString(asPunctuator()));
                case TokenType::STRING:
                    return fmt::format("string-literal({})", asString());
                default:
                    return fmt::format("unknown(t={})", JS::toString(m_type));
            }
        }

        [[nodiscard]] inline constexpr TokenType
        type() const noexcept {
            return m_type;
        }
    };

} // namespace JS
