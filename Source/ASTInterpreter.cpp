/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "ASTInterpreter.hpp"

#include "Source/AST/DebuggerStatementNode.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Enums/OperationType.hpp"
#include "Source/Expressions/AssignmentExpression.hpp"
#include "Source/Expressions/BiExpression.hpp"
#include "Source/Expressions/CallExpression.hpp"
#include "Source/Expressions/FunctionExpression.hpp"
#include "Source/Expressions/MemberExpression.hpp"
#include "Source/Expressions/ObjectLiteralExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"
#include "Source/Expressions/UniExpression.hpp"
#include "Source/Expressions/UpdateExpression.hpp"
#include "Source/Interpretation/ComparisonEngine.hpp"
#include "Source/Interpretation/ValueOperations.hpp"
#include "Source/Objects/StringObject.hpp"
#include "Source/Utils/StringUtils.hpp"
#include "Source/ASTNode.hpp"
#include "Source/Base.hpp"
#include "Source/NativeCallInformation.hpp"
#include "Source/Object.hpp"
#include "Source/Settings/InterpreterSettings.hpp"

namespace JS::AST {

    std::pair<bool, Value>
    assign(AssignmentType type, PropertyOrVariable &&lhs, Value &&rhs, std::string_view nameHint, bool ignoreConstness=false) noexcept {
        if (lhs.isNull()) {
            LOG_ERROR("Interpreter: assign to null/invalid");
            return {false, JS::Undefined{}};
        }

        if (!ignoreConstness
                && lhs.isVariable()
                && std::get<Variable *>(lhs)->type() == VariableType::LEXICAL_CONST) {
            LOG_ERROR("Interpreter: cannot assign to const variable \"{}\"", nameHint);
            return {false, {}};
        }

        if (!ignoreConstness
                && lhs.isProperty()
                && !std::get<PropertyReference>(lhs).property()->isWritable()) {
            LOG_ERROR("Interpreter: cannot assign to non-writable variable \"{}\"", nameHint);
            return {false, {}};
        }

        if (type == AssignmentType::REGULAR) {
            return {lhs.visit([&] (auto *ptr) {
                return ptr->value(std::move(rhs));
            }), lhs.toValue()};
        }

        auto value{lhs.toValue()};
        std::pair<bool, Value> ret;

        const auto operatorType = translateAssignmentTypeToOperator(type);

        switch (type) {
#define JS_ADD_OPERATION(type, operation) \
            case AssignmentType::type:           \
                ret = doValueOperation(value, rhs, ValueOperations::operation, operatorType); \
                break;
            JS_ADD_OPERATION(ADD, Plus)
            JS_ADD_OPERATION(SUBTRACT, Minus)
            JS_ADD_OPERATION(MULTIPLY, Multiply)
            JS_ADD_OPERATION(DIVIDE, Divide)
            JS_ADD_OPERATION(REMAINDER, Remainder)
#undef JS_ADD_OPERATION
            default:
                LOG_ERROR("ASSIGN: unknown operation {}", type);
                return {false, JS::Undefined{}};
        }

        if (!ret.first) {
            LOG_ERROR("Interpreter: ValueOp failed");
            return ret;
        }

        return {lhs.visit([&] (auto *ptr) {
            return ptr->value(std::move(ret.second));
        }), lhs.toValue()};
    }

    std::optional<bool>
    Interpreter::doComparison(ExpressionType type, const BiExpression *biExpression) noexcept {
        auto lhsResult = executeExpression(biExpression->lhs(), biExpression);
        if (lhsResult.isError()) {
            return std::nullopt;
        }

        auto rhsResult = executeExpression(biExpression->rhs(), biExpression);
        if (rhsResult.isError()) {
            return std::nullopt;
        }

        JS_DISCARD(type);
        auto lhs = lhsResult.toValue();
        auto rhs = rhsResult.toValue();

        switch (type) {
            case ExpressionType::BI_EQUAL:
                return doLooselyEquality(lhs, rhs);
            case ExpressionType::BI_NOT_EQUAL:
                return !doLooselyEquality(lhs, rhs);
            case ExpressionType::BI_STRICT_EQUAL:
                if (lhs.index() != rhs.index())
                    return false;

                return doStrongEquality(lhs, rhs);
            case ExpressionType::BI_NOT_STRICT_EQUAL:
                if (lhs.index() != rhs.index())
                    return true;

                return !doStrongEquality(lhs, rhs);
            default:
                JS_ASSERT(false);
        }
    }

    bool
    Interpreter::doStrongEquality(const Value &lhs, const Value &rhs) noexcept {
        switch (lhs.index()) {
            case ValueIndex::Undefined:
            case ValueIndex::Null:
                return true;
            case ValueIndex::Bool:
                return std::get<bool>(lhs) == std::get<bool>(rhs);
            case ValueIndex::SignedInt:
                return std::get<SignedInt>(lhs) == std::get<SignedInt>(rhs);
            case ValueIndex::Float:
                // TODO epsilon comparison?
                return std::get<FloatType>(lhs) == std::get<FloatType>(rhs);
            case ValueIndex::String:
                return std::get<std::string>(lhs) == std::get<std::string>(rhs);
            case ValueIndex::ObjectRef:
                return std::get<std::shared_ptr<Object>>(lhs).get() == std::get<std::shared_ptr<Object>>(rhs).get();
            case ValueIndex::FunctionReference:
                return std::get<const FunctionDeclarationNode *>(lhs) == std::get<const FunctionDeclarationNode *>(rhs);
            case ValueIndex::FunctionExpr:
                return std::get<const FunctionExpression *>(lhs) == std::get<const FunctionExpression *>(rhs);
            case ValueIndex::NativeFunction:
                // TODO
                return std::get<NativeFunctionDescriptor>(lhs).debugName() == std::get<NativeFunctionDescriptor>(rhs).debugName();
            default:
                JS_ASSERT(false);
                return false;
        }
    }

    bool
    Interpreter::doLooselyEquality(const Value &lhs, const Value &rhs) noexcept {
        if (lhs.index() == rhs.index())
            return doStrongEquality(lhs, rhs);

        if (lhs.index() == ValueIndex::SignedInt && rhs.index() == ValueIndex::Float)
            return doStrongEquality(lhs.asFloat(), rhs);

        if (lhs.index() == ValueIndex::Float && rhs.index() == ValueIndex::SignedInt)
            return doStrongEquality(lhs, rhs.asFloat());

        if (lhs.index() == ValueIndex::Undefined && rhs.index() == ValueIndex::Null)
            return true;

        if (lhs.index() == ValueIndex::Null && rhs.index() == ValueIndex::Undefined)
            return true;

        if (lhs.isNumeric() && rhs.index() == ValueIndex::String)
            return doLooselyEquality(lhs, rhs.toNumber());

        if (lhs.index() == ValueIndex::String && rhs.isNumeric())
            return doLooselyEquality(lhs.toNumber(), rhs);

        // TODO BigInt and Symbol

        if (lhs.index() == ValueIndex::Bool)
            return doLooselyEquality(lhs.toNumber(), rhs);

        if (rhs.index() == ValueIndex::Bool)
            return doLooselyEquality(lhs, rhs.toNumber());

        // TODO Symbol
        if (rhs.index() == ValueIndex::ObjectRef && (lhs.index() == ValueIndex::String || lhs.isNumeric())) {
            return doLooselyEquality(lhs, rhs.toPrimitive());
        }

        // TODO Symbol
        if (lhs.index() == ValueIndex::ObjectRef && (rhs.index() == ValueIndex::String || rhs.isNumeric())) {
            return doLooselyEquality(lhs.toPrimitive(), rhs);
        }

        // TODO BigInt

        return false;
    }

    ExpressionResult
    Interpreter::executeExpression(const Expression *expression, const Expression *parentExpression) noexcept {
        const auto operatorType = translateExpressionTypeToOperator(expression->expressionType());

        const auto expressionType = expression->expressionType();
        switch (expressionType) {
            case ExpressionType::ASSIGNMENT: {
                const auto expr = static_cast<const AssignmentExpression *>(expression);

                auto lhs = executeExpression(expr->lhs(), expression);
                if (lhs.isError()) {
                    LOG_ERROR("Interpreter: invalid lhs for assignment-expr");
                    return {};
                }

                auto rhs = executeExpression(expr->rhs(), expression);
                if (rhs.isError()) {
                    LOG_ERROR("Interpreter: invalid rhs for assignment-expr");
                    return {};
                }

                if (lhs.isProperty()) {
                    auto &reference = std::get<PropertyReference>(lhs);
                    return assign(expr->assignmentType(), {reference}, rhs.toValue(), reference.nameHint());
                }

                if (lhs.isVariable()) {
                    auto *variable = std::get<Variable *>(lhs);
                    return assign(expr->assignmentType(), {variable}, rhs.toValue(), variable->nameHint());
                }

                LOG_ERROR("Interpreter: invalid type of lhs ({}) for assigment-expr", lhs.index());
                return {};
            }
            case ExpressionType::CALL_MEMBER:
                return processCallMemberExpression(static_cast<const BiExpression *>(expression), parentExpression);
            case ExpressionType::CALL_PROPERTY_ACCESS:
                return processCallPropertyAccessExpression(static_cast<const BiExpression *>(expression), parentExpression);
            case ExpressionType::CALL_REGULAR:
                return processCallRegularExpression(static_cast<const CallExpression *>(expression));
#define JS_EXPR_OP(type, op) \
case ExpressionType::type: { \
    const auto *expr = static_cast<const BiExpression *>(expression); \
    auto lhs = executeExpression(expr->lhs(), expression); \
    auto rhs = executeExpression(expr->rhs(), expression); \
    if (lhs.isError() || rhs.isError()) \
        return {}; \
    return doValueOperation(lhs.toValue(), rhs.toValue(), ValueOperations::op, operatorType); \
}
            JS_EXPR_OP(BI_ADD, Plus)
            JS_EXPR_OP(BI_DIVIDE, Divide)
            JS_EXPR_OP(BI_LEFT_SHIFT, LeftShift)
            JS_EXPR_OP(BI_MULTIPLY, Multiply)
            JS_EXPR_OP(BI_REMAINDER, Remainder)
            JS_EXPR_OP(BI_RIGHT_SHIFT, RightShift)
            JS_EXPR_OP(BI_SUBTRACT, Minus)
            JS_EXPR_OP(BI_UNSIGNED_RIGHT_SHIFT, ZeroFillRightShift)

            case ExpressionType::BI_EQUAL:
            case ExpressionType::BI_NOT_EQUAL:
            case ExpressionType::BI_NOT_STRICT_EQUAL:
            case ExpressionType::BI_STRICT_EQUAL: {
                auto ret = doComparison(expressionType, static_cast<const BiExpression *>(expression));
                if (ret.has_value())
                    return Value{ret.value()};
                return {};
            }
            case ExpressionType::BI_GREATER_THAN:
            case ExpressionType::BI_GREATER_THAN_OR_EQUAL:
            case ExpressionType::BI_LESS_THAN:
            case ExpressionType::BI_LESS_THAN_OR_EQUAL:
            case ExpressionType::COALESCE:
                return processCoalesceExpression(static_cast<const BiExpression *>(expression));
            case ExpressionType::RELATIONAL_IN:
            case ExpressionType::RELATIONAL_INSTANCEOF:
                return processRelationExpression(static_cast<const BiExpression *>(expression));
            case ExpressionType::FUNCTION:
                return processFunctionExpression(static_cast<const FunctionExpression *>(expression));
            case ExpressionType::LITERAL_NULL:
                return {Null{}};
            case ExpressionType::LITERAL_FALSE:
                return {false};
            case ExpressionType::LITERAL_TRUE:
                return {true};
            case ExpressionType::LOGICAL_AND:
                return processLogicalExpression(static_cast<const BiExpression *>(expression), false);
            case ExpressionType::LOGICAL_OR:
                return processLogicalExpression(static_cast<const BiExpression *>(expression), true);
            case ExpressionType::MEMBER: {
                auto result = resolveMemberExpression(static_cast<const MemberExpression *>(expression), parentExpression, nullptr);
                if (result.isNull())
                    return {};
                if (result.isUndefined())
                    return {JS::Undefined{}};
                return {std::forward<ReferenceRecord>(result)};
            } case ExpressionType::OBJECT_LITERAL:
                return processObjectLiteral(static_cast<const ObjectLiteral *>(expression));
            case ExpressionType::PRIMARY_IDENTIFIER: {
                const auto &identifier = static_cast<const PrimaryExpression<std::string> *>(expression)->data();
                auto ident = findIdentifier(identifier);
                if (ident.isNull()) {
                    LOG_ERROR("Interpreter: undefined identifier \"{}\"", identifier);
                    return {};
                }

                return {std::forward<PropertyOrVariable>(ident)};
            }
            case ExpressionType::PRIMARY_SIGNED_INT:
                return {static_cast<const PrimaryExpression<SignedInt> *>(expression)->data()};
            case ExpressionType::PRIMARY_STRING:
                return {static_cast<const PrimaryExpression<std::string> *>(expression)->data()};
            case ExpressionType::UNI_PLUS: {
                auto result = executeExpression(static_cast<const UniExpression *>(expression)->data(), expression);
                if (result.isError())
                    return result;
                return {+result.toValue().toFloat()};
            };
            case ExpressionType::UNI_MINUS: {
                auto result = executeExpression(static_cast<const UniExpression *>(expression)->data(), expression);
                if (result.isError())
                    return result;
                return {-result.toValue().toFloat()};
            }
            case ExpressionType::UNI_LOGICAL_NOT: {
                auto result = executeExpression(static_cast<const UniExpression *>(expression)->data(), expression);
                if (result.isError())
                    return result;
                return {!result.toValue().toSignedInt()};
            }
            case ExpressionType::UNI_BITWISE_NOT: {
                auto result = executeExpression(static_cast<const UniExpression *>(expression)->data(), expression);
                if (result.isError())
                    return result;
                return {~result.toValue().toSignedInt()};
            }
            case ExpressionType::UPDATE:
                return processUpdateExpression(static_cast<const UpdateExpression *>(expression));
            default:
                LOG_ERROR("Interpreter: Unknown expression type: {}", JS::toString(expression->expressionType()));
                break;
        }

        return {};
    }

    bool
    Interpreter::doesExpressionCreateProperty(const Expression *parent, const Expression *child) noexcept {
        if (parent == nullptr || parent->expressionType() != ExpressionType::ASSIGNMENT)
            return false;

        const auto *assignment = static_cast<const AssignmentExpression *>(parent);
        return assignment->lhs() == child;
    }

    std::pair<bool, Value>
    Interpreter::executeNativeFunction(Value *inThisArgument,
                                       const NativeFunctionDescriptor &descriptor,
                                       const CallExpression *originatedFromExpression,
                                       const std::vector<std::unique_ptr<Expression>> &parameterExpressions) noexcept {
        std::vector<Value> parameters{};
        parameters.reserve(std::size(parameterExpressions));

        auto thisArgument = inThisArgument == nullptr
                          ? Value{JS::Undefined{}}
                          : *inThisArgument;

        for (const auto &expression : parameterExpressions) {
            auto result = executeExpression(expression.get(), originatedFromExpression);
            if (result.isError()) {
                LOG_ERROR("ASTInterpreter: failed to resolve parameter expression for native function \"%s\"", descriptor.debugName());
                return {false, {}};
            }
            parameters.push_back(result.toValue());
        }

        auto ret = descriptor.data()(NativeCallInformation{&m_globalObject, thisArgument}, parameters);
        return {!m_stopFlag, std::move(ret)};
    }

    void
    Interpreter::explainRedeclarationError(const Node *node, std::string_view name, DeclarationType type,
                                           DeclarationErrorType errorType) const noexcept {
        JS_DISCARD(node);
        LOG_ERROR(R"(Interpreter: redeclaration of identifier "{}" as {} in record "{}")",
                      name, JS::toString(type), JS::toString(errorType));
    }

    PropertyOrVariable
    Interpreter::findIdentifier(const std::string &name) noexcept {
        if (auto *variable = m_lexicalEnvironment.find(name))
            return {variable};

        if (auto *property = m_globalObject.findProperty(name))
            return {PropertyReference{nullptr, property, std::string(name)}};

        return {nullptr};
    }

    DeclarationErrorType
    Interpreter::isDeclarable(std::string_view identifier, DeclarationType type) const noexcept {
        if (m_lexicalEnvironment.find(identifier) != nullptr) {
            return DeclarationErrorType::LEXICAL_DEFINITION;
        }

        if (type == DeclarationType::LEXICAL_CONST || type == DeclarationType::LEXICAL_LET) {
            if (m_globalObject.findProperty(identifier) != nullptr) {
                return DeclarationErrorType::GLOBAL_DEFINITION;
            }
        }

        return DeclarationErrorType::NONE;
    }

    ReferenceRecord
    Interpreter::processCallMemberExpression(const BiExpression *expression, const Expression *parentExpression) noexcept {
        auto lhs = executeExpression(expression->lhs(), expression);
        if (lhs.isError()) {
            LOG_ERROR("ASTInterpreter: processCallMemberExpression: failed to executed lhs");
            return ReferenceRecord{nullptr};
        }

        auto lhsValue = lhs.toValue();
        std::shared_ptr<Object> object;
        if (lhsValue.index() == ValueIndex::String) {
            object = std::make_shared<Objects::StringObject>(&m_globalObject, std::get<std::string>(lhsValue));
        } else if (lhsValue.index() == ValueIndex::ObjectRef) {
            object = std::get<std::shared_ptr<Object>>(lhsValue);
        } else {
            LOG_ERROR("ASTInterpreter: processCallMemberExpression: cannot work with Value: {} (index={})", lhsValue.toString(), lhsValue.index());
            return ReferenceRecord{nullptr};
        }

        LOG_TRACE("rhs type is {}", JS::toString(expression->rhs()->expressionType()));

        if (expression->rhs()->expressionType() == ExpressionType::PRIMARY_IDENTIFIER) {
            const auto &identifier = static_cast<const PrimaryExpression<std::string> *>(expression->rhs())->data();

            if (auto *property = object->findProperty(identifier)) {
                return ReferenceRecord{object, PropertyReference{object, property, std::string(identifier)}};
            }

            if (doesExpressionCreateProperty(parentExpression, expression)) {
                return ReferenceRecord{object, PropertyReference{object, &(object->addProperty(identifier, Property{JS::Undefined{}})), std::string(identifier)}};
            }

            return ReferenceRecord{nullptr};
        }

        JS_ASSERT(expression->rhs()->expressionType() == ExpressionType::MEMBER);

        auto ret = resolveMemberExpression(static_cast<const MemberExpression *>(expression->rhs()), parentExpression, &object);
        if (ret.isNull()) {
            LOG_ERROR("ASTInterpreter: processCallMemberExpression: resolveMemberExpression failed!");
            return ReferenceRecord{nullptr};
        }

        return ret;
    }

    PropertyReference
    Interpreter::processCallPropertyAccessExpression(const BiExpression *expression, const Expression *parentExpression) noexcept {
        auto parent = executeExpression(expression->lhs(), expression);
        if (parent.isError()) {
            LOG_ERROR("processCallPropertyAccessExpression: failed to execute parent");
            return {};
        }

        auto parentValue = parent.toValue();
        if (parentValue.index() != ValueIndex::ObjectRef) {
            LOG_ERROR("processCallPropertyAccessExpression: parent is not of type object: {}", parentValue.toString());
            return {};
        }

        auto child = executeExpression(expression->rhs(), expression);
        if (child.isError()) {
            LOG_ERROR("processCallPropertyAccessExpression: failed to execute child");
            return {};
        }

        auto name = child.toValue().toString();
        auto &object = std::get<std::shared_ptr<Object>>(parentValue);
        auto *property = object->findProperty(name);
        if (property == nullptr) {
            if (doesExpressionCreateProperty(parentExpression, expression)) {
                return {object, &(object->addProperty(name, Property{JS::Undefined{}})), std::move(name)};
            }

            LOG_ERROR("processCallPropertyAccessExpression: object doesn't contain property \"{}\"!", name);
            return {};
        }

        return {object, property, std::move(name)};
    }

    std::pair<bool, Value>
    Interpreter::processCallRegularExpression(const CallExpression *expression) noexcept {
        bool hasThisArgument{false};
        Value thisArgument{JS::Undefined{}};

        ExpressionResult callee;

        switch (expression->callee()->expressionType()) {
            case ExpressionType::CALL_MEMBER: {
                auto record = processCallMemberExpression(static_cast<BiExpression *>(expression->callee()), expression);
                if (record.isNull()) {
                    LOG_ERROR("{} todo handle error", __FUNCTION__);
                    return {false, {}};
                }

                if (record.isUndefined()) {
                    return {true, {}};
                }

                hasThisArgument = true;
                thisArgument = record.parent();
                callee = std::move(record);
            } break;
            default:
                callee = executeExpression(expression->callee(), expression);
                break;
        }

        if (callee.isError()) {
            LOG_ERROR("ASTInterpreter: call-regular: failed to resolve callee.");
            return {false, {}};
        }

        auto calleeValue = callee.toValue();
        const Value &functorValue
            = calleeValue.index() == ValueIndex::ObjectRef
            ? std::get<std::shared_ptr<Object>>(calleeValue)->value()
            : calleeValue;

        const FunctionDeclaration *declaration;
        const std::vector<std::unique_ptr<Node>> *children;

        if (functorValue.index() == ValueIndex::NativeFunction) {
            return executeNativeFunction(
                    hasThisArgument ? &thisArgument : nullptr,
                    std::get<NativeFunctionDescriptor>(functorValue),
                    expression,
                    expression->parameters()
            );
        }

        if (functorValue.index() == ValueIndex::FunctionReference) {
            const auto *node = std::get<const FunctionDeclarationNode *>(functorValue);
            children = &node->children();
            declaration = &node->functionExpression();
        } else if (functorValue.index() == ValueIndex::FunctionExpr) {
            auto *functionExpression = std::get<const FunctionExpression *>(functorValue);
            children = &functionExpression->node().children();
            declaration = &functionExpression->declaration();
        } else {
            LOG_ERROR("ASTInterpreter: call-regular: callee is of non-function type {} ({})!", calleeValue.toString(), calleeValue.index());
            return {false, {}};
        }

        m_lexicalEnvironment.pushScope(LexicalScopeType::FUNCTION);

        for (std::size_t i = 0; i < std::size(declaration->parameterNames()); ++i) {
            if (expression->parameters().size() > i) {
                auto result = executeExpression(expression->parameters()[i].get(), expression);
                if (result.isError()) {
                    LOG_ERROR("ASTInterpreter: call-regular: failed to resolve {}th parameter", i + 1);
                    return {false, {}};
                }

                m_lexicalEnvironment.create(VariableType::FUNCTION_ARGUMENT, declaration->parameterNames()[i], result.toValue());
            } else {
                m_lexicalEnvironment.create(VariableType::FUNCTION_ARGUMENT, declaration->parameterNames()[i], {JS::Undefined{}});
            }
        }

        Value returnValue{};
        for (const auto &child : *children) {
            auto result = processNode(child.get());
            if (!result.success()) {
                LOG_TRACE("ASTInterpreter: call-regular: stmt inside failed :(");
                m_lexicalEnvironment.popScope();
                return {false, {}};
            }

            if (result.behavior() == NodeExecutionResult::Behavior::RETURN) {
                returnValue = std::move(result.value());
                break;
            }
        }

        m_lexicalEnvironment.popScope();
        return {true, returnValue};
    }

    std::pair<bool, Value>
    Interpreter::processCoalesceExpression(const BiExpression *expression) noexcept {
        auto lref = executeExpression(expression->lhs(), expression);
        if (lref.isError())
            return {false, {}};
        auto lval = lref.toValue();
        if (lval.index() != ValueIndex::Undefined && lval.index() != ValueIndex::Null)
            return {true, std::move(lval)};

        auto rref = executeExpression(expression->rhs(), expression);
        if (rref.isError())
            return {false, {}};
        return {true, rref.toValue()};
    }

    bool
    Interpreter::processDeclaration(const DeclarationStatementNode *node, DeclarationType type, const Declaration &declaration) noexcept {
        switch (type) {
            case DeclarationType::LEXICAL_CONST:
            case DeclarationType::LEXICAL_LET: {
                const auto errorType = isDeclarable(declaration.identifier(), type);
                if (errorType != DeclarationErrorType::NONE) {
                    explainRedeclarationError(node, declaration.identifier(), type, errorType);
                    return false;
                }

                auto *variable = m_lexicalEnvironment.create(
                        type == DeclarationType::LEXICAL_CONST
                              ? VariableType::LEXICAL_CONST
                              : VariableType::LEXICAL_LET,
                        declaration.identifier());
                if (variable == nullptr) {
                    LOG_DEBUG("failed to declare lexical record of \"{}\"",
                                  declaration.identifier());
                    return false;
                }

                if (declaration.expression() == nullptr)
                    return true;

                auto value = executeExpression(declaration.expression(), nullptr);
                if (value.isError()) {
                    LOG_DEBUG("failed to execute expression of lexical declaration of \"{}\"",
                                  declaration.identifier());
                    return false;
                }

                auto pair = assign(AssignmentType::REGULAR, {variable}, value.toValue(), declaration.identifier(), true);

                if (m_interpreterSettings->announceDeclarations) {
                    LOG_INFO("Interpreter: declared \"{}\" (decltype={}) with value: {}", declaration.identifier(),
                             JS::toString(type), pair.second.toString());
                }

                return pair.first;
            }
            default:
                LOG_TRACE("Unknown declaration type {}", JS::toString(type));
                JS_ASSERT(false);
                return false;
        }
    }

    std::pair<bool, Value>
    Interpreter::processFunctionExpression(const FunctionExpression *expression) noexcept {
        return {true, expression};
    }

    NodeExecutionResult
    Interpreter::processNode(const Node *node) noexcept {
        if (m_stopFlag)
            return NodeExecutionResult{true, {}};

        if (node->nodeType() == NodeType::DECLARATION_STATEMENT) {
            const auto *statement = static_cast<const DeclarationStatementNode *>(node);

            for (const auto &decl : statement->declarations()) {
                if (!processDeclaration(statement, statement->declarationType(), decl)) {
                    LOG_ERROR("Interpreter: failed to process declaration statement for id \"{}\"",
                                  decl.identifier());
                    return {false, {}};
                }
            }

            return {true, {}};
        }

        if (node->nodeType() == NodeType::EXPRESSION_STATEMENT) {
            auto pair = executeExpression(static_cast<const ExpressionStatementNode *>(node)->expression(), nullptr);
            if (pair.isError()) {
                LOG_ERROR("Interpreter: failed to execute expression statement.");
                return {false, {}};
            }
            return {true, pair.toValue()};
        }

        if (node->nodeType() == NodeType::FUNCTION_DECLARATION) {
            const auto *declaration = static_cast<const FunctionDeclarationNode *>(node);
            if (declaration->functionExpression().isAnonymous()) {
                LOG_WARNING("Interpreter: ignoring anonymous function");
                return {true, {}};
            }

            const auto &identifier = declaration->functionExpression().name().value();
            const auto declarationErrorType = isDeclarable(identifier, DeclarationType::FUNCTION);
            if (declarationErrorType != DeclarationErrorType::NONE) {
                explainRedeclarationError(node, identifier, DeclarationType::FUNCTION, declarationErrorType);
                return {false, {}};
            }

            auto *object = m_lexicalEnvironment.create(VariableType::FUNCTION_ARGUMENT, identifier, declaration);
            if (!object) {
                return {false, {}};
            }

            return {true, {}};
        }

        if (node->nodeType() == NodeType::RETURN_STATEMENT) {
            if (!m_lexicalEnvironment.hasScope(LexicalScopeType::FUNCTION)) {
                LOG_ERROR("Interpreter: 'return' outside function definition");
                return {false, {}};
            }

            const auto *statement = static_cast<const ReturnStatementNode *>(node);
            if (statement->expression()) {
                auto result = executeExpression(statement->expression(), nullptr);
                return {!result.isError(), result.toValue(), NodeExecutionResult::Behavior::RETURN};
            }

            return {true, {}, NodeExecutionResult::Behavior::RETURN};
        }

        if (node->nodeType() == NodeType::IF_STATEMENT) {
            const auto *statement = static_cast<const IfStatementNode *>(node);
            auto result = executeExpression(statement->condition(), nullptr);
            if (result.isError()) {
                LOG_ERROR("Interpreter: failed to execute condition expression for if statement");
                return {false, {}};
            }

            if (result.toValue().toBoolean()) {
                return processNode(statement->firstStatement());
            }

            if (statement->secondStatement() != nullptr) {
                return processNode(statement->secondStatement());
            }

            return {true, {}};
        }

        if (node->nodeType() == NodeType::WHILE_STATEMENT) {
            const auto *statement = static_cast<const WhileStatementNode *>(node);

            while (true) {
                auto result = executeExpression(statement->condition(), nullptr);
                if (result.isError()) {
                    LOG_ERROR("Interpreter: failed to execute condition expression for if statement");
                    return {false, {}};
                }

                if (!result.toValue().toBoolean()) {
                    break;
                }

                const auto bodyResult = processNode(statement->body());
                if (!bodyResult.success()) {
                    return {false, {}};
                }

                if (bodyResult.behavior() == NodeExecutionResult::Behavior::RETURN) {
                    break;
                }
            }

            return {true, {}};
        }

        if (node->nodeType() == NodeType::BLOCK) {
            m_lexicalEnvironment.pushScope(LexicalScopeType::GENERIC);
            for (const auto &child : static_cast<const BlockNode *>(node)->children()) {
                auto result = processNode(child.get());
                if (!result.success()) {
                    return {false, {}};
                }

                if (result.behavior() == NodeExecutionResult::Behavior::RETURN)
                    break;
            }
            m_lexicalEnvironment.popScope();
            return {true, {}};
        }

        if (node->nodeType() == NodeType::FOR_STATEMENT){
            const auto *statement = static_cast<const ForStatementNode *>(node);

            m_lexicalEnvironment.pushScope(LexicalScopeType::FOR_STATEMENT);

            if (statement->firstExpression() != nullptr) {
                if (executeExpression(statement->firstExpression(), nullptr).isError())
                    return {false, {}};
            } else {
                JS_ASSERT(statement->declaration() != nullptr);
                if (!processNode(statement->declaration()).success())
                    return {false, {}};
            }

            while (true) {
                const auto secondResult = executeExpression(statement->secondExpression(), nullptr);
                if (secondResult.isError()) {
                    LOG_ERROR("Interpreter: failed to execute second expression for for statement");
                    return {false, {}};
                }

                if (!secondResult.toValue().toBoolean()) {
                    break;
                }

                const auto childResult = processNode(statement->body());
                if (!childResult.success()) {
                    return {false, {}};
                }

                if (childResult.behavior() == NodeExecutionResult::Behavior::RETURN) {
                    break;
                }

                const auto thirdResult = executeExpression(statement->thirdExpression(), nullptr);
                if (thirdResult.isError()) {
                    LOG_ERROR("Interpreter: failed to execute third expression for for statement");
                    return {false, {}};
                }
            }

            m_lexicalEnvironment.popScope();
            return {true, {}};
        }

        if (node->nodeType() == NodeType::DEBUGGER_STATEMENT) {
            if (m_debuggerEnabled) {
                if (m_debuggerCallback)
                    m_debuggerCallback(static_cast<const DebuggerStatementNode *>(node)->sourceLocation());
                else
                    markStopFlag();
            }
            return {true, {}};
        }

        LOG_ERROR("Interpreter: unhandled node of type {}", JS::toString(node->nodeType()));
        return {false, {}};
    }

    std::pair<bool, Value>
    Interpreter::processLogicalExpression(const BiExpression *expression, bool expected) noexcept {
        auto lref = executeExpression(expression->lhs(), expression);
        if (lref.isError())
            return {false, {}};
        auto lval = lref.toValue();
        if (lval.toBoolean() == expected)
            return {true, std::move(lval)};

        auto rref = executeExpression(expression->rhs(), expression);
        if (rref.isError())
            return {false, {}};
        return {true, rref.toValue()};
    }

    std::pair<bool, Value>
    Interpreter::processObjectLiteral(const ObjectLiteral *literal) noexcept {
        auto object = std::make_shared<Object>();
        for (const auto &propertyDefinition : literal->definitions()) {
            if (propertyDefinition.initializer() == nullptr) {
                object->addProperty(propertyDefinition.identifier(), Property{JS::Undefined{}});
            } else {
                auto status = executeExpression(propertyDefinition.initializer(), literal);
                if (status.isError()) {
                    return {false, {}};
                }

                object->addProperty(propertyDefinition.identifier(), Property{status.toValue()});
            }
        }

        return {true, object};
    }

    std::pair<bool, Value>
    Interpreter::processRelationExpression(const BiExpression *expression) noexcept {
        const auto lhsResult = executeExpression(expression->lhs(), expression);
        if (lhsResult.isError())
            return {false, {}};

        const auto rhsResult = executeExpression(expression->rhs(), expression);
        if (rhsResult.isError())
            return {false, {}};

        const auto lhs = lhsResult.toValue();
        const auto rhs = rhsResult.toValue();
        Value r;

        switch (expression->expressionType()) {
            case ExpressionType::BI_LESS_THAN:
                r = ComparisonEngine::isLessThan(lhs, rhs, true);
                if (r.index() == ValueIndex::Undefined)
                    return {true, false};
                return {true, r};
            case ExpressionType::BI_GREATER_THAN:
                r = ComparisonEngine::isLessThan(rhs, lhs, true);
                if (r.index() == ValueIndex::Undefined)
                    return {true, false};
                return {true, r};
            case ExpressionType::BI_LESS_THAN_OR_EQUAL:
                r = ComparisonEngine::isLessThan(rhs, lhs, true);
                if (r.index() == ValueIndex::Undefined || (r.index() == ValueIndex::Bool && std::get<bool>(r)))
                    return {true, false};
                return {true, true};
            case ExpressionType::BI_GREATER_THAN_OR_EQUAL:
                r = ComparisonEngine::isLessThan(lhs, rhs, true);
                if (r.index() == ValueIndex::Undefined || (r.index() == ValueIndex::Bool && std::get<bool>(r)))
                    return {true, false};
                return {true, true};
                // TODO instanceof
            case ExpressionType::RELATIONAL_IN:
                // <property> in <object>
                // TODO throw TypeError when lhs isn't object
                if (rhs.index() != ValueIndex::ObjectRef)
                    return {false, {}};
                return {true, std::get<std::shared_ptr<Object>>(rhs)->findProperty(Value(lhs).toString()) != nullptr};
            default:
                JS_ASSERT(false);
                break;
        }

        return {};
    }

    std::pair<bool, Value>
    Interpreter::processUpdateExpression(const UpdateExpression *updateExpression) noexcept {
        auto result = executeExpression(updateExpression->expression(), updateExpression);

        if (!result.isProperty() && !result.isVariable()) {
            LOG_ERROR("Interpreter: cannot do update on {}", result.index());
            return {false, {}};
        }

        auto &value = result.isProperty()
                    ? std::get<PropertyReference>(result).property()->value()
                    : std::get<Variable *>(result)->value();

        return {true, value.doUpdate(updateExpression->updateType())};
    }

    ReferenceRecord
    Interpreter::resolveMemberExpression(const MemberExpression *expression,
                                         const Expression *parentExpression,
                                         std::shared_ptr<Object> *object) noexcept {
        const bool expressionCreatesProperty = doesExpressionCreateProperty(parentExpression, expression);

        for (std::size_t i = 0; i < std::size(expression->identifiers()); ++i) {
            const auto &entry = expression->identifiers()[i];
            const auto &identifier = entry.identifier();

            PropertyOrVariable inside;
            if (object == nullptr) {
                inside = findIdentifier(identifier);
            } else if (auto *property = (*object)->findProperty(identifier)) {
                inside = {PropertyReference{*object, property, std::string(identifier)}};
            }

            if (inside.isNull()) {
                if (entry.optional()) {
                    return ReferenceRecord{JS::Undefined{}};
                }

                // for the last item, we can create a new identifier
                if (i == std::size(expression->identifiers()) - 1) {
                    if (object != nullptr && expressionCreatesProperty) {
                        return ReferenceRecord{PropertyReference{*object, &((*object)->addProperty(identifier, Property{
                                JS::Undefined{}})), std::string(identifier)}};
                    }

                    return ReferenceRecord{JS::Undefined{}};
                };

                LOG_ERROR("ASTInterpreter: resolveMemberExpression: unknown identifier \"{}\"!", identifier);
                LOG_ERROR("ASTInterpreter: resolveMemberExpression: #{} in \"{}\"",
                    StringUtils::formatOrdinal(i + 1),
                    StringUtils::join(std::cbegin(expression->identifiers()), std::cend(expression->identifiers()),
                                      [](const auto &ent, bool first) {
                                          if (first)
                                              return ent.identifier();
                                          if (ent.optional())
                                              return fmt::format("?.{}", ent.identifier());
                                          return fmt::format(".{}", ent.identifier());
                                      }, ""));
                return ReferenceRecord{nullptr};
            }

            if (i == std::size(expression->identifiers()) - 1) {
                if (inside.isProperty())
                    return ReferenceRecord{std::get<PropertyReference>(inside)};
                JS_ASSERT(false);
                return ReferenceRecord{nullptr};
            }

            auto &value = inside.toValue();
            if (value.index() != ValueIndex::ObjectRef) {
                LOG_ERROR("ASTInterpreter: resolveMemberExpression: invalid property/variable type: {}", value.index());
                return ReferenceRecord{nullptr};
            }
            object = &std::get<std::shared_ptr<Object>>(value);
        }

        JS_ASSERT(false);
        return ReferenceRecord{nullptr};
    }

    bool
    Interpreter::run() noexcept {
        for (const auto &node : m_rootNode->children()) {
            auto ret = processNode(node.get());
            if (!ret.success()) {
                LOG_ERROR("Interpreter: failed to process node {}", JS::toString(node->nodeType()));
                return false;
            }

            JS_ASSERT(ret.behavior() == NodeExecutionResult::Behavior::REGULAR);

            if (m_interpreterSettings->announceNodeResult) {
                LOG_DEBUG("Interpreter: {} node returned: {}", JS::toString(node->nodeType()),
                              ret.value().toString());
            }
        }

        LOG_INFO("Stopped successfully");
        return true;
    }

} // namespace JS::AST
