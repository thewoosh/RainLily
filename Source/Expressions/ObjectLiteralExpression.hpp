/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Source/Expressions/Expression.hpp"

namespace JS {

    struct PropertyDefinition {
    private:
        std::string m_identifier;
        std::unique_ptr<Expression> m_initializer;

    public:
        [[nodiscard]] inline explicit
        PropertyDefinition(std::string &&identifier, std::unique_ptr<Expression> &&initializer=nullptr) noexcept
                : m_identifier(std::move(identifier))
                , m_initializer(std::move(initializer)) {
        }

        [[nodiscard]] inline explicit
        PropertyDefinition(const std::string &identifier, std::unique_ptr<Expression> &&initializer=nullptr) noexcept
                : m_identifier(identifier)
                , m_initializer(std::move(initializer)) {
        }

        [[nodiscard]] inline std::string &
        identifier() noexcept {
            return m_identifier;
        }

        [[nodiscard]] inline const std::string &
        identifier() const noexcept {
            return m_identifier;
        }

        [[nodiscard]] inline Expression *
        initializer() noexcept {
            return m_initializer.get();
        }

        [[nodiscard]] inline const Expression *
        initializer() const noexcept {
            return m_initializer.get();
        }
    };

    struct ObjectLiteral
            : public Expression {
    private:
        std::vector<PropertyDefinition> m_definitions;

    public:
        [[nodiscard]] inline
        ObjectLiteral() noexcept
                : Expression(ExpressionType::OBJECT_LITERAL) {
        }

        [[nodiscard]] inline std::vector<PropertyDefinition> &
        definitions() noexcept {
            return m_definitions;
        }

        [[nodiscard]] inline const std::vector<PropertyDefinition> &
        definitions() const noexcept {
            return m_definitions;
        }

    };

} // namespace JS
