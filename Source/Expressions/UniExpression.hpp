/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/Expression.hpp"

namespace JS {

    struct UniExpression
            : public Expression {
    private:
        std::unique_ptr<Expression> m_data;

    public:
        [[nodiscard]] inline
        UniExpression(ExpressionType expressionType,
                      std::unique_ptr<Expression> &&data) noexcept
                : Expression(expressionType)
                , m_data(std::move(data)) {
        }

        [[nodiscard]] inline Expression *
        data() noexcept {
            return m_data.get();
        }

        [[nodiscard]] inline const Expression *
        data() const noexcept {
            return m_data.get();
        }
    };

} // namespace JS
