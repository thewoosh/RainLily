/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/Expression.hpp"
#include "Source/Base.hpp"

#include <string>

namespace JS {

    template <typename Contained>
    struct PrimaryExpression
            : public Expression {
    private:
        Contained m_contained;

    public:
        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(SignedInt value) noexcept
                : Expression(ExpressionType::PRIMARY_SIGNED_INT)
                , m_contained(value) {
        }

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(ExpressionType type, std::string &&value) noexcept
                : Expression(type)
                , m_contained(std::move(value)) {
        }

        [[nodiscard]] inline constexpr explicit
        PrimaryExpression(ExpressionType type, const std::string &value) noexcept
                : Expression(type)
                , m_contained(value) {
        }

        [[nodiscard]] inline constexpr Contained &
        data() noexcept {
            return m_contained;
        }

        [[nodiscard]] inline constexpr const Contained &
        data() const noexcept {
            return m_contained;
        }
    };

} // namespace JS
