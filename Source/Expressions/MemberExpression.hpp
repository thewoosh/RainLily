/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string>
#include <vector>

#include "Source/Expressions/Expression.hpp"

namespace JS {

    struct MemberExpressionElement {
        std::string m_identifier{};
        bool m_optional{};

        [[nodiscard]] inline
        MemberExpressionElement(std::string &&identifier, bool optional)
                : m_identifier(std::move(identifier))
                , m_optional(optional) {
        }

        [[nodiscard]] inline
        MemberExpressionElement(const std::string &identifier, bool optional)
                : m_identifier(identifier)
                , m_optional(optional) {
        }

        [[nodiscard]] MemberExpressionElement() = default;
        [[nodiscard]] MemberExpressionElement(MemberExpressionElement &&) = default;
        [[nodiscard]] MemberExpressionElement(const MemberExpressionElement &) = default;
        MemberExpressionElement &operator=(MemberExpressionElement &&) = default;
        MemberExpressionElement &operator=(const MemberExpressionElement &) = default;

        [[nodiscard]] inline std::string &
        identifier() noexcept {
            return m_identifier;
        }

        [[nodiscard]] inline const std::string &
        identifier() const noexcept {
            return m_identifier;
        }

        [[nodiscard]] inline constexpr bool
        optional() const noexcept {
            return m_optional;
        }
    };

    struct MemberExpression : public Expression {
    private:
        std::vector<MemberExpressionElement> m_identifiers{};

    public:
        [[nodiscard]] inline
        MemberExpression(std::vector<MemberExpressionElement> &&identifiers) noexcept
                : Expression(ExpressionType::MEMBER)
                , m_identifiers(std::move(identifiers)) {
        }

        [[nodiscard]] inline const std::vector<MemberExpressionElement> &
        identifiers() const noexcept {
            return m_identifiers;
        }
    };

} // namespace JS
