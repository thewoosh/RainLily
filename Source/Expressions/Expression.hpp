/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Enums/AssignmentType.hpp"
#include "Source/Enums/ExpressionType.hpp"
#include "Source/Enums/UpdateExpressionType.hpp"
#include "Source/AST/Node.hpp"
#include "Source/Base.hpp"
#include "Source/Declaration.hpp"

#ifndef JS_EXPRESS_ASSERT
#   include <cassert>
#   define JS_EXPRESS_ASSERT assert
#endif

namespace JS {

    struct Expression {
    private:
        ExpressionType m_expressionType;

    public:
        [[nodiscard]] inline constexpr explicit
        Expression(ExpressionType expressionType) noexcept
                : m_expressionType(expressionType) {
        }

        virtual ~Expression() = default;

        [[nodiscard]] inline constexpr ExpressionType
        expressionType() const noexcept {
            return m_expressionType;
        }
    };

} // namespace JS
