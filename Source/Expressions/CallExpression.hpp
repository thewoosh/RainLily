/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/BiExpression.hpp"

namespace JS {

    struct CallExpression
            : public Expression {
    private:
        std::unique_ptr<Expression> m_callee;
        std::vector<std::unique_ptr<Expression>> m_parameters;

        friend class AST::Optimizer;

    public:
        [[nodiscard]] inline
        CallExpression(ExpressionType expressionType,
                       std::unique_ptr<Expression> &&callee,
                       std::vector<std::unique_ptr<Expression>> &&parameters) noexcept
                : Expression(expressionType)
                , m_callee(std::move(callee))
                , m_parameters(std::move(parameters)) {
        }

        [[nodiscard]] inline Expression *
        callee() const noexcept {
            return m_callee.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        calleeData() noexcept {
            return m_callee;
        }

        [[nodiscard]] inline std::vector<std::unique_ptr<Expression>> &
        parameters() noexcept {
            return m_parameters;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Expression>> &
        parameters() const noexcept {
            return m_parameters;
        }
    };

} // namespace JS
