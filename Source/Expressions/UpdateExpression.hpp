/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/Expression.hpp"

namespace JS {

    struct UpdateExpression
            : public Expression {
    private:
        UpdateExpressionType m_updateExpressionType;
        std::unique_ptr<Expression> m_expression;

    public:
        [[nodiscard]] inline
        UpdateExpression(UpdateExpressionType type, std::unique_ptr<Expression> &&expression) noexcept
                : Expression(ExpressionType::UPDATE)
                , m_updateExpressionType(type)
                , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline Expression *
        expression() noexcept {
            return m_expression.get();
        }

        [[nodiscard]] inline const Expression *
        expression() const noexcept {
            return m_expression.get();
        }

        [[nodiscard]] inline constexpr UpdateExpressionType
        updateType() const noexcept {
            return m_updateExpressionType;
        }
    };

} // namespace JS
