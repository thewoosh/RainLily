/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/Expression.hpp"

namespace JS {

    struct BiExpression
            : public Expression {
    private:
        std::unique_ptr<Expression> m_lhs;
        std::unique_ptr<Expression> m_rhs;

        friend class AST::Optimizer;

    public:
        [[nodiscard]] inline
        BiExpression(ExpressionType expressionType,
                     std::unique_ptr<Expression> &&lhs,
                     std::unique_ptr<Expression> &&rhs) noexcept
                : Expression(expressionType)
                , m_lhs(std::move(lhs))
                , m_rhs(std::move(rhs)) {
        }

        [[nodiscard]] inline Expression *
        lhs() noexcept {
            return m_lhs.get();
        }

        [[nodiscard]] inline const Expression *
        lhs() const noexcept {
            return m_lhs.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        lhsData() noexcept {
            return m_lhs;
        }

        [[nodiscard]] inline Expression *
        rhs() noexcept {
            return m_rhs.get();
        }

        [[nodiscard]] inline const Expression *
        rhs() const noexcept {
            return m_rhs.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        rhsData() noexcept {
            return m_rhs;
        }
    };

} // namespace JS
