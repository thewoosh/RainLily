/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/Expression.hpp"
#include "Source/Declaration.hpp"

namespace JS {

    struct FunctionExpression : public Expression {
    private:
        FunctionDeclaration m_declaration;
        AST::ParentNode m_node{AST::NodeType::FUNCTION_EXPRESSION};

    public:
        [[nodiscard]] inline explicit
        FunctionExpression(FunctionDeclaration &&declaration) noexcept
                : Expression(ExpressionType::FUNCTION)
                , m_declaration(std::move(declaration)) {
        }

        [[nodiscard]] inline const FunctionDeclaration &
        declaration() const noexcept {
            return m_declaration;
        }

        [[nodiscard]] inline AST::ParentNode &
        node() noexcept {
            return m_node;
        }

        [[nodiscard]] inline const AST::ParentNode &
        node() const noexcept {
            return m_node;
        }
    };

} // namespace JS
