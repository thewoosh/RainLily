/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Expressions/BiExpression.hpp"

namespace JS {

    struct AssignmentExpression
            : public BiExpression {
    private:
        AssignmentType m_assignmentType;

    public:
        [[nodiscard]] inline
        AssignmentExpression(AssignmentType assignmentType,
                             std::unique_ptr<Expression> &&lhs,
                             std::unique_ptr<Expression> &&rhs) noexcept
                : BiExpression(ExpressionType::ASSIGNMENT, std::move(lhs), std::move(rhs))
                , m_assignmentType(assignmentType) {
        }

        [[nodiscard]] inline constexpr AssignmentType
        assignmentType() const noexcept {
            return m_assignmentType;
        }
    };

} // namespace JS
