/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Value.hpp"

#include <fmt/format.h>

#include "Source/ASTNode.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Object.hpp"

namespace JS {

    /**
     * https://tc39.es/ecma262/multipage/abstract-operations.html#sec-toboolean
     */
    bool
    Value::toBoolean() const noexcept {
        const auto i = index();

        if (i == ValueIndex::Undefined || i == ValueIndex::Null)
            return false;

        if (i == ValueIndex::Bool)
            return std::get<bool>(*this);

        if (i == ValueIndex::SignedInt)
            return std::get<SignedInt>(*this) != 0;

        if (i == ValueIndex::Float) {
            const auto val = std::get<FloatType>(*this) != 0;
            return val != +0.0 && val != -0.0 && val != Constants::nan;
        }

        if (i == ValueIndex::String) {
            return !std::empty(std::get<std::string>(*this));
        }

        if (i == ValueIndex::ObjectRef)
            return true;

        // TODO
        return true;
    }

    Value
    Value::doUpdate(UpdateExpressionType type) noexcept {
#define VALUE_DO_UPDATE(vt, express) \
        vt result = express; \
        switch (type) { \
            case UpdateExpressionType::PRE_INCREMENT: \
                operator=(result += vt(1)); \
                break; \
            case UpdateExpressionType::PRE_DECREMENT: \
                operator=(result -= vt(1)); \
                break; \
            case UpdateExpressionType::POST_INCREMENT:\
                operator=(result + vt(1)); \
                break; \
            case UpdateExpressionType::POST_DECREMENT: \
                operator=(result - vt(1)); \
                break; \
            default: \
                JS_ASSERT(false);    \
        } \
        return result;

        if (index() == ValueIndex::SignedInt) {
            VALUE_DO_UPDATE(SignedInt, std::get<SignedInt>(*this))
        }

        VALUE_DO_UPDATE(FloatType, std::get<FloatType>(toNumeric()))
    }

    Value
    Value::toPrimitive() const noexcept  {
        if (index() != ValueIndex::ObjectRef) {
            return *this;
        }

        return std::get<std::shared_ptr<Object>>(*this)->value().toPrimitive();

//        // TODO @@toPrimitive (?)
//        JS_ASSERT(false);
//        return {};
    }

    std::string
    Value::toString() noexcept {
        switch (index()) {
            case ValueIndex::Undefined:
                return "undefined";
            case ValueIndex::Null:
                return "null";
            case ValueIndex::Bool:
                return std::get<bool>(*this) ? "true" : "false";
            case ValueIndex::SignedInt:
                return fmt::format_int(std::get<SignedInt>(*this)).str();
            case ValueIndex::Float:
                return fmt::format("{}", std::get<FloatType>(*this));
            case ValueIndex::ObjectRef: {
                auto &obj = std::get<std::shared_ptr<Object>>(*this);
                if (obj == nullptr) {
                    return "null";
                }

                return obj->value().toString();
            }
            case ValueIndex::String:
                return fmt::format("\"{}\"", std::get<std::string>(*this));
            case ValueIndex::FunctionReference: {
                auto &func = std::get<const AST::FunctionDeclarationNode *>(*this);
                return fmt::format("function{{name={}}}", func->functionExpression().toString());
            }
            case ValueIndex::FunctionExpr:
                return "function-reference";
            case ValueIndex::NativeFunction:
                return "native-function-descriptor";
            default:
                LOG_ERROR("Unknown value index: {}", index());
                JS_ASSERT(false);
                return "(invalid)";
        }
    }

} // namespace JS