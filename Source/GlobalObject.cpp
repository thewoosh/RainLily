/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/GlobalObject.hpp"

#include "Source/Objects/CommonHelper.hpp"
#include "Source/Objects/ConsoleObject.hpp"
#include "Source/Objects/DateConstructor.hpp"
#include "Source/Objects/MathObject.hpp"
#include "Source/Prototypes/StringPrototype.hpp"
#include "Source/NativeCallInformation.hpp"

namespace JS {

    void
    GlobalObject::addBasicProperties() noexcept {
        addProperty("Infinity", Property{std::numeric_limits<FloatType>::infinity()})
                .setConfigurable(false)
                .setEnumerable(false)
                .setWritable(false);

        addProperty("NaN", Property{std::numeric_limits<FloatType>::quiet_NaN()})
                .setConfigurable(false)
                .setEnumerable(false)
                .setWritable(false);

        addProperty("undefined", Property{JS::Undefined{}})
                .setConfigurable(false)
                .setEnumerable(false)
                .setWritable(false);

        Objects::addNativeFunction(this, "isFinite", NativeFunctionDescriptor{
                1,
                "GlobalObject::isFinite",
                [](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                    if (std::size(parameters) == 0)
                        return false;
                    return parameters.front().toFloat() != Constants::nan
                        && parameters.front().toFloat() != +Constants::infinity
                        && parameters.front().toFloat() != -Constants::infinity;
                }
        });

        Objects::addNativeFunction(this, "isNan", NativeFunctionDescriptor{
            1,
            "GlobalObject::isNan",
            [](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                if (std::size(parameters) == 0)
                    return false;
                return parameters.front().toFloat() == Constants::nan;
            }
        });
    }

    void
    GlobalObject::addStringPrototype() noexcept {
        auto stringObject = std::make_shared<Object>();
        auto prototype = std::make_shared<Object>();

        Prototypes::fillStringPrototype(prototype.get());

        stringObject->addProperty("prototype", Property{std::move(prototype)});
        addProperty("String", Property{std::move(stringObject)});
    }

    GlobalObject::GlobalObject(const ExtensionSettings &extensionSettings) noexcept
            : m_extensionSettings(extensionSettings) {
        addBasicProperties();
        addProperty("Date", Property{std::make_shared<Objects::DateConstructor>()});
        addProperty("Math", Property{std::make_shared<Objects::MathObject>()});
        addStringPrototype();

        if (m_extensionSettings.enableConsole) {
            addProperty("console", Property{std::make_shared<Objects::ConsoleObject>()});
        }
    }

} // namespace JS