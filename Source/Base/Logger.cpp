/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Base/Logger.hpp"

#include <array>
#include <chrono>

#include <fmt/chrono.h>

std::string_view
logger::currentDateTime() noexcept {
    static thread_local std::string buffer;

    buffer.clear();
    fmt::format_to(std::back_inserter(buffer), "{:%H:%M:%S}", std::chrono::system_clock::now().time_since_epoch());

    return {std::data(buffer)};
}
