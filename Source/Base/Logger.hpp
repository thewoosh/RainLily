/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>
#include <utility> // for std::forward

#include <cstdio> // for stdout

#include "Include/fmt.hpp"

namespace logger {

    [[nodiscard]] std::string_view
    currentDateTime() noexcept;

    template <typename...Args>
    inline void
    log(const char *tag, const char *fileName, std::size_t line, std::string_view format, Args &&...args) noexcept {
        fmt::print(stdout,
                   FMTLIB_WRAPPER(fmt::format("[{}] [{}] [{}:{}] {}\n", currentDateTime(), tag, fileName, line, format)),
                   std::forward<Args>(args)...);
    }

} // namespace logger

#define LOG_DEBUG(...) ::logger::log("DEBUG", __FILE__, __LINE__, __VA_ARGS__)
#define LOG_ERROR(...) ::logger::log("ERROR", __FILE__, __LINE__, __VA_ARGS__)
#define LOG_INFO(...) ::logger::log("INFO", __FILE__, __LINE__, __VA_ARGS__)
#define LOG_TRACE(...) ::logger::log("TRACE", __FILE__, __LINE__, __VA_ARGS__)
#define LOG_WARNING(...) ::logger::log("WARNING", __FILE__, __LINE__, __VA_ARGS__)
