/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <variant>

#include <cstddef>

#include "Source/PropertyReference.hpp"
#include "Source/Variable.hpp"

namespace JS {

    struct PropertyOrVariable
            : public std::variant<std::nullptr_t, PropertyReference, Variable *> {
        [[nodiscard]] inline bool
        isNull() const noexcept {
            return index() == 0;
        }

        [[nodiscard]] inline bool
        isProperty() const noexcept {
            return index() == 1;
        }

        [[nodiscard]] inline bool
        isVariable() const noexcept {
            return index() == 2;
        }

        [[nodiscard]] inline Value &
        toValue() noexcept {
            if (isProperty())
                return std::get<PropertyReference>(*this).property()->value();
            return std::get<Variable *>(*this)->value();
        }

        template <typename Visitor>
        [[nodiscard]] inline bool
        visit(Visitor &&visitor) noexcept {
            if (isProperty())
                return visitor(std::get<PropertyReference>(*this).property());
            if (isVariable())
                return visitor(std::get<Variable *>(*this));
            return false;
        }
    };

} // namespace JS
