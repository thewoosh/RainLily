/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>
#include <vector>

#include <cstddef> // for std::size_t

#include "Source/IO/Base.hpp"
#include "Source/IO/ErrorType.hpp"
#include "Source/Utils/ArrayView.hpp"

#ifdef JS_IO_USE_CSTDIO
#   include <cstdio>
#endif

namespace JS::IO {

    class ReadFile {
#ifdef JS_IO_USE_POSIX
        int m_fd{-1};
#else
        std::FILE *m_file{nullptr};
#endif

        std::size_t m_size{};
        ErrorType m_error{ErrorType::NONE};

    public:
        [[nodiscard]] explicit
        ReadFile(std::string_view name) noexcept;

        ~ReadFile() noexcept;

        [[nodiscard]] inline constexpr ErrorType
        error() const noexcept {
            return m_error;
        }

        [[nodiscard]] inline constexpr std::size_t
        size() const noexcept {
            return m_size;
        }

        [[nodiscard]] bool
        read(ArrayView<char> destination);

        [[nodiscard]] inline std::pair<std::vector<char>, bool>
        read() noexcept {
            std::vector<char> vec(m_size);
            bool status = read(vec);
            return {vec, status};
        }
    };

} // namespace JS::IO
