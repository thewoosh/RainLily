/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/IO/ReadFile.hpp"

#include <cassert>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "Source/Base.hpp"
#include "Source/Base/Logger.hpp"

namespace POSIX {

    using ::read;

} // namespace POSIX

namespace JS::IO {

    ReadFile::ReadFile(std::string_view name) noexcept
            : m_fd(open(name.data(), O_RDONLY)){
        if (m_fd == -1) {
            switch (errno) {
                case ENOTDIR:
                case ENAMETOOLONG:
                case ENOENT:
                    m_error = ErrorType::FILE_NOT_FOUND;
                    break;
                case EACCES:
                    m_error = ErrorType::ACCESS_DENIED;
                    break;
                case EISDIR:
                    m_error = ErrorType::READFILE_ON_DIRECTORY;
                    break;
                default:
                    LOG_WARNING("Unknown errno of open(2): {}", errno);
                    m_error = ErrorType::UNKNOWN_ERROR;
            }

            return;
        }

        struct stat status;

        if (fstat(m_fd, &status) == -1) {
            LOG_WARNING("Unknown errno of fstat(2): {}", errno);
            m_error = ErrorType::UNKNOWN_ERROR;
            return;
        }

        if (S_ISDIR(status.st_mode)) {
            m_error = ErrorType::READFILE_ON_DIRECTORY;
            return;
        }

        JS_ASSERT(status.st_size >= 0);
        m_size = static_cast<std::size_t>(status.st_size);
    }

    ReadFile::~ReadFile() noexcept {
        if (m_fd != -1) {
            static_cast<void>(close(m_fd));
        }
    }

    bool
    ReadFile::read(ArrayView<char> destination) {
        char *ptr = std::data(destination);
        std::size_t left = std::size(destination);

        if (m_error != ErrorType::NONE) {
            return false;
        }

        do {
            const auto ret = POSIX::read(m_fd, ptr, left);
            if (ret == 0) {
                m_error = ErrorType::READ_PAST_EOF;
                return false;
            }

            if (ret == -1) {
                LOG_WARNING("Unknown errno of read(2): {}", errno);
                m_error = ErrorType::UNKNOWN_ERROR;
                return false;
            }

            left -= static_cast<std::size_t>(ret);
            ptr += ret;
        } while (left != 0);

        return true;
    }

} // namespace JS::IO
