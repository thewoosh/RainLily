/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/IO/ReadFile.hpp"

#include <cassert>

#include "Include/spdlog.hpp"

#include "Source/Base.hpp"

namespace JS::IO {

    ReadFile::ReadFile(std::string_view name) noexcept
            : m_file(std::fopen(name.data(), "rb")) {
        if (m_file == nullptr) {
            // TODO
            m_error = ErrorType::FILE_NOT_FOUND;
            return;
        }

        std::fseek(m_file, 0L, SEEK_END);
        m_size = static_cast<std::size_t>(std::ftell(m_file));
        std::rewind(m_file);
    }

    ReadFile::~ReadFile() noexcept {
        if (m_file != nullptr) {
            std::fclose(m_file);
        }
    }

    bool
    ReadFile::read(ArrayView<char> destination) {

        char *ptr = std::data(destination);
        std::size_t left = std::size(destination);

        if (m_error != ErrorType::NONE) {
            return false;
        }

        do {
            const auto ret = std::fread(static_cast<void *>(ptr), sizeof(char), left, m_file);
            if (std::feof(m_file)) {
                m_error = ErrorType::READ_PAST_EOF;
                return false;
            }
 
            if (std::ferror(m_file)) {
                m_error = ErrorType::UNKNOWN_ERROR;
                return false;
            }

            left -= ret;
            ptr += ret;
        } while (left != 0);

        return true;
    }

} // namespace JS::IO
