/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#if !defined(__unix__) && !defined(unix)
#   define JS_IO_USE_CSTDIO
#else
#   define JS_IO_USE_POSIX
#endif
