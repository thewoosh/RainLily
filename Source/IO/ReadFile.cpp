/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/IO/ReadFile.hpp"

#ifdef JS_IO_USE_POSIX
#   include "ReadFile_POSIX.cpp"
#endif

#ifdef JS_IO_USE_CSTDIO
#   include "ReadFile_CSTDIO.cpp"
#endif
