/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    namespace IO {

        enum class ErrorType {
            NONE,

            ACCESS_DENIED,
            FILE_NOT_FOUND,
            READFILE_ON_DIRECTORY,
            READ_PAST_EOF,
            UNKNOWN_ERROR,
        };

    } // namespace IO

    [[nodiscard]] inline constexpr std::string_view
    toString(IO::ErrorType type) noexcept {
        switch (type) {
            case IO::ErrorType::NONE:                   return "none";
            case IO::ErrorType::ACCESS_DENIED:          return "access-denied";
            case IO::ErrorType::FILE_NOT_FOUND:         return "file-not-found";
            case IO::ErrorType::READFILE_ON_DIRECTORY:  return "readfile-on-directory";
            case IO::ErrorType::READ_PAST_EOF:          return "read-past-eof";
            case IO::ErrorType::UNKNOWN_ERROR:          return "unknown-error";
            default:                                return "(invalid)";
        }
    }

} // namespace JS
