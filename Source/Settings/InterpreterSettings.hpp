/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace JS {

    struct InterpreterSettings {
        bool announceDeclarations{false};
        bool announceNodeResult{false};
    };

} // namespace JS
