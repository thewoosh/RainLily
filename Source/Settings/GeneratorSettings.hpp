/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace JS {

    struct GeneratorSettings {
        bool dumpTokens{false};
        bool dumpAST{false};
        bool dumpOptimizedAST{false};
    };

} // namespace JS
