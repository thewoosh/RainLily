/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace JS {

    struct ExtensionSettings {

        /**
         * Enable the 'console' object in the global object. This provides
         * functions such as console.log("Hello World"). For more information,
         * see the Console Living Standard: https://console.spec.whatwg.org/.
         */
        bool enableConsole{true};

    };

} // namespace JS
