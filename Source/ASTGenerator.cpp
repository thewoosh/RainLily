/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/ASTGenerator.hpp"

#include "Source/AST/DebuggerStatementNode.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Expressions/AssignmentExpression.hpp"
#include "Source/Expressions/BiExpression.hpp"
#include "Source/Expressions/CallExpression.hpp"
#include "Source/Expressions/FunctionExpression.hpp"
#include "Source/Expressions/MemberExpression.hpp"
#include "Source/Expressions/ObjectLiteralExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"
#include "Source/Expressions/UniExpression.hpp"
#include "Source/Expressions/UpdateExpression.hpp"

#define JS_SUBROUTINE_ERROR_TRACE LOG_TRACE
#define JS_CHECK_EOF_RESULT
#define JS_CHECK_EOF() \
        if (m_it == std::cend(m_tokens)) { \
            reportError(GeneratorError::UNEXPECTED_EOF); \
            return JS_CHECK_EOF_RESULT; \
        }


namespace JS::AST {

    std::unique_ptr<Expression>
    Generator::consumeAdditiveExpression() noexcept {
        auto expr = consumeMultiplicativeExpression();
        if (expr == nullptr || m_it->type() != TokenType::PUNCTUATOR)
            return expr;

        const auto punc = m_it->asPunctuator();
        if (punc != PunctuatorType::ADD && punc != PunctuatorType::SUBTRACT)
            return expr;

        ++m_it;
        auto rhs = consumeAdditiveExpression();
        if (!rhs) {
            --m_it;
            return expr;
        }

        return std::make_unique<BiExpression>(
                punc == PunctuatorType::ADD ? ExpressionType::BI_ADD : ExpressionType::BI_SUBTRACT,
                std::move(expr), std::move(rhs)
        );
    }

    std::unique_ptr<Expression>
    Generator::consumeAssignmentExpression() noexcept {
        // TODO fixme update to other stuff
        const auto capturedIt{m_it};

        m_errorStack.print();
        {
            auto lhsExpr = consumeLHSExpression();
            if (lhsExpr != nullptr
                    && m_it->type() == TokenType::PUNCTUATOR
                    && isAssignmentPunctuator(m_it->asPunctuator())) {
                const auto punctuator = m_it->asPunctuator();
                ++m_it;

                auto rhs = consumeAssignmentExpression();
                if (rhs) {
                    return std::make_unique<AssignmentExpression>(translatePunctuatorToAssignmentType(punctuator),
                                                                  std::move(lhsExpr), std::move(rhs));
                }
            }

            m_it = capturedIt;
        }
        m_errorStack.reset();

        if (auto expr = consumeShortCircuitExpression()) {
            return expr;
        }

        return nullptr;
    }

    bool
    Generator::consumeBlock() noexcept {
        m_currentNode = static_cast<ParentNode *>(m_currentNode->addChild(std::make_unique<BlockNode>()));

        while (true) {
            if (m_it == std::cend(m_tokens)) {
                reportError(GeneratorError::UNEXPECTED_EOF);
                return false;
            }

            auto status = processToken(m_currentNode);
            if (status == TokenProcessError::END_OF_INTENDED_SCOPE_REACHED) {
                break;
            }

            if (status != TokenProcessError::NONE) {
                JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeBlock: inner token error: {}", JS::toString(status));
                return false;
            }
        }

        m_currentNode = m_currentNode->parent();

        return true;
    }

    std::unique_ptr<Expression>
    Generator::consumeCallArguments(std::unique_ptr<Expression> &&lhs) noexcept {
        auto expr = std::move(lhs);

        while (m_it != std::cend(m_tokens)) {
            if (consumePunctuator(PunctuatorType::LEFT_PARENTHESIS)) {
                std::vector<std::unique_ptr<Expression>> parameters{};

                while (true) {
                    if (consumePunctuator(PunctuatorType::RIGHT_PARENTHESIS)) {
                        expr = std::make_unique<CallExpression>(ExpressionType::CALL_REGULAR,
                                                                std::move(expr), std::move(parameters));
                        break;
                    }

                    if (!std::empty(parameters) && !consumePunctuator(PunctuatorType::COMMA)) {
                        reportError(GeneratorError::GENERIC_EXPECTED_GOT, ',', m_it->toString());
                        return nullptr;
                    }

                    auto param = consumeExpression();
                    if (!param) {
                        JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeCallExpression: failed to produce expression for parameter");
                        return nullptr;
                    }

                    parameters.push_back(std::move(param));
                }

                continue;
            }

            if (consumePunctuator(PunctuatorType::FULL_STOP)) {
                auto member = consumeMemberExpression();
                if (member == nullptr) {
                    return nullptr;
                }

                expr = std::make_unique<BiExpression>(ExpressionType::CALL_MEMBER, std::move(expr), std::move(member));
                continue;
            }

            if (m_it->type() == TokenType::PUNCTUATOR && m_it->asPunctuator() == PunctuatorType::OPTIONAL_CHAINING) {
                expr = consumeOptionalChain(std::move(expr));
                if (!expr) {
                    JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to invoke consumeOptionalChain");
                    break;
                }

                continue;
            }

            break;
        }

        return expr;
    }

    std::unique_ptr<Expression>
    Generator::consumeCallExpression() noexcept {
        auto expr = consumeMemberExpression();
        if (expr)
            return consumeCallArguments(std::move(expr));
        return expr;
    }

    std::unique_ptr<Expression>
    Generator::consumeEqualityExpression() noexcept {
        auto expr = consumeRelationalExpression();
        if (!expr || m_it == std::end(m_tokens) || m_it->type() != TokenType::PUNCTUATOR)
            return expr;

        const auto expressionType = convertPunctuatorToEqualityExpressionType(m_it->asPunctuator());
        if (expressionType == ExpressionType::INVALID) {
            return expr;
        }

        ++m_it;
        auto rhs = consumeEqualityExpression();
        if (!rhs)
            return nullptr;

        return std::make_unique<BiExpression>(expressionType, std::move(expr), std::move(rhs));
    }

    std::unique_ptr<Expression>
    Generator::consumeExponentiationExpression() noexcept {
        auto expr = consumeUnaryExpression();

        if (expr == nullptr
                || m_it->type() != TokenType::PUNCTUATOR
                || m_it->asPunctuator() != PunctuatorType::EXPONENT)
            return expr;

        ++m_it;
        auto rhs = consumeExponentiationExpression();
        if (!rhs)
            return nullptr;
        return std::make_unique<BiExpression>(ExpressionType::EXPONENTIATION, std::move(expr), std::move(rhs));
    }

    std::unique_ptr<Expression>
    Generator::consumeExpression() noexcept {
        // TODO https://tc39.es/ecma262/multipage/ecmascript-language-expressions.html#prod-Expression
        return consumeAssignmentExpression();
    }

    bool
    Generator::consumeForStatement() noexcept {
        if (!consumePunctuator(PunctuatorType::LEFT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, '(', m_it->toString());
            return false;
        }

        std::unique_ptr<Expression> firstExpression{};
        std::unique_ptr<DeclarationStatementNode> declarationStatement{};

        const auto type = findLexicalDeclarationType();
        if (type != LexicalDeclarationType::NONE) {
            auto declaration = consumeLexicalDeclaration(type);
            if (!declaration) {
                return false;
            }

            auto node = std::move(m_currentNode->children().back());
            m_currentNode->children().pop_back();
            declarationStatement = std::unique_ptr<DeclarationStatementNode>(static_cast<DeclarationStatementNode *>(node.release()));
        } else {
            firstExpression = consumeExpression();
            if (firstExpression == nullptr)
                return false;
        }

        std::array<std::unique_ptr<Expression>, 2> expressions{};
        for (std::size_t i = 0; i < 2; ++i) {
            if (!consumeSemicolon()) {
                reportError(GeneratorError::GENERIC_EXPECTED_GOT, ';', m_it->toString());
                return false;
            }

            expressions[i] = consumeExpression();
            if (!expressions[i])
                return false;
        }

        if (!consumePunctuator(PunctuatorType::RIGHT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, ')', m_it->toString());
            return false;
        }

        auto *const savedCurrentNode = m_currentNode;

        ParentNode tempNode{NodeType::WHILE_STATEMENT};
        tempNode.parent(m_currentNode);
        m_currentNode = &tempNode;

        if (auto status = processToken(); status != TokenProcessError::NONE) {
            m_currentNode = savedCurrentNode;
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeWhileStatement: processToken => {}", JS::toString(status));
            return false;
        }

        JS_ASSERT(m_currentNode == &tempNode);
        JS_ASSERT(std::size(tempNode.children()) == 1);
        m_currentNode = tempNode.parent();

        if (declarationStatement == nullptr) {
            JS_ASSERT(firstExpression != nullptr);
            m_currentNode->children().push_back(
                    std::make_unique<ForStatementNode>(
                            std::move(firstExpression),
                            std::move(expressions[0]),
                            std::move(expressions[1]),
                            std::move(tempNode.children()[0])
                    )
            );
        } else {
            JS_ASSERT(firstExpression == nullptr);
            m_currentNode->children().push_back(
                    std::make_unique<ForStatementNode>(
                            std::move(declarationStatement),
                            std::move(expressions[0]),
                            std::move(expressions[1]),
                            std::move(tempNode.children()[0])
                    )
            );
        }

        return true;
    }

    std::optional<FunctionDeclaration>
    Generator::consumeFunction(bool canBeDefault) noexcept {
        // TODO allow conditionally default keyword prefix:
        JS_DISCARD(canBeDefault);

        // https://tc39.es/ecma262/multipage/ecmascript-language-functions-and-classes.html#sec-function-definitions
        ++m_it; // consume 'FUNCTION' keyword

        std::optional<std::string> name{};
        std::vector<std::string> parameterNames{};
        if (m_it->type() == TokenType::IDENTIFIER) {
            name = m_it->asString();
            ++m_it;
        }

        if (!consumePunctuator(PunctuatorType::LEFT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, '(', m_it->toString());
            return std::nullopt;
        }

        while (true) {
            if (m_it == std::end(m_tokens)) {
                reportError(GeneratorError::UNEXPECTED_EOF);
                return std::nullopt;
            }

            if (consumePunctuator(PunctuatorType::RIGHT_PARENTHESIS)) {
                break;
            }

            if (!std::empty(parameterNames) && !consumePunctuator(PunctuatorType::COMMA)) {

                reportError(GeneratorError::NO_COMMA_BETWEEN_FUNCTION_DECLARATION_ARGUMENTS, m_it->toString());
                return std::nullopt;
            }

            if (m_it->type() != TokenType::IDENTIFIER) {
                reportError(GeneratorError::FUNCTION_DECLARATION_EXPECT_ARGUMENT_NAME, m_it->toString());
                return std::nullopt;
            }

            parameterNames.emplace_back(m_it->asString());
            ++m_it;
        }

        if (!consumePunctuator(PunctuatorType::LEFT_CURLY_BRACKET)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, '{', m_it->toString());
            return std::nullopt;
        }

        return FunctionDeclaration{std::move(name), std::move(parameterNames)};
    }

    bool
    Generator::consumeFunctionDeclaration() noexcept {
        auto function = consumeFunction(true);
        if (!function.has_value()) {
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeFunctionDeclaration: failed to consume a function");
            return false;
        }

        auto *node = m_currentNode->addChild(std::make_unique<FunctionDeclarationNode>(std::move(function.value())));
        m_currentNode = JS_DYNAMIC_CAST<ParentNode *>(node);
        return true;
    }

    std::unique_ptr<Expression>
    Generator::consumeFunctionExpression() noexcept {
        auto function = consumeFunction(true);
        if (!function.has_value()) {
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeFunctionExpression: failed to consume a function");
            return nullptr;
        }

        auto expr = std::make_unique<FunctionExpression>(std::move(function.value()));

        auto *const savedCurrentNode = m_currentNode;

        auto *const functionNode = &expr->node();
        functionNode->parent(m_currentNode);
        m_currentNode = functionNode;

        while (true) {
            if (m_it == std::cend(m_tokens)) {
                reportError(GeneratorError::UNEXPECTED_EOF);
                return nullptr;
            }

            if (m_currentNode == functionNode && consumePunctuator(PunctuatorType::RIGHT_CURLY_BRACKET)) {
                break;
            }

            if (auto status = processToken(); status != TokenProcessError::NONE) {
                JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeFunctionExpression: failed to process inner token, error: {}", JS::toString(status));
                return nullptr;
            }
        }

        m_currentNode = savedCurrentNode;
        return expr;
    }

    bool
    Generator::consumeIfStatement() noexcept {
        if (!consumePunctuator(PunctuatorType::LEFT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, '(', m_it->toString());
            return false;
        }

        auto condition = consumeExpression();
        if (condition == nullptr) {
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to generate if condition expression");
            return false;
        }

        if (!consumePunctuator(PunctuatorType::RIGHT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, ')', m_it->toString());
            return false;
        }

        auto *const savedCurrentNode = m_currentNode;

        ParentNode node{NodeType::IF_STATEMENT};
        node.parent(m_currentNode);
        m_currentNode = &node;

        if (auto status = processToken(); status != TokenProcessError::NONE) {
            m_currentNode = savedCurrentNode;
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeIfStatement: processToken => {}", JS::toString(status));
            return false;
        }

        if (consumeKeyword(KeywordType::ELSE)) {
            if (auto status = processToken(); status != TokenProcessError::NONE) {
                m_currentNode = savedCurrentNode;
                JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeIfStatement: processToken => {}", JS::toString(status));
                return false;
            }
        }

        JS_ASSERT(std::size(node.children()) >= 1);

        // TODO assert types of node.children()
        m_currentNode = savedCurrentNode;
        m_currentNode->addChild(std::make_unique<IfStatementNode>(
                std::move(condition),
                std::move(node.children()[0]),
                node.children().size() == 1 ? nullptr : std::move(node.children()[1])
        ));
        return true;
    }

    bool
    Generator::consumeKeyword(KeywordType type) noexcept {
        if (m_it->type() != TokenType::KEYWORD)
            return false;

        if (m_it->asKeyword() != type)
            return false;

        ++m_it;
        return true;
    }

    std::unique_ptr<Expression>
    Generator::consumeLHSExpression() noexcept {
        return consumeCallExpression();
    }

    bool
    Generator::consumePunctuator(PunctuatorType type) noexcept {
        if (m_it->type() != TokenType::PUNCTUATOR)
            return false;

        if (m_it->asPunctuator() != type)
            return false;

        ++m_it;
        return true;
    }

    [[nodiscard]] std::unique_ptr<Expression>
    Generator::consumeMemberExpression() noexcept {
        auto expr = consumePrimaryExpression();
        if (expr == nullptr)
            return expr;

        bool isOptional;
        if (expr->expressionType() == ExpressionType::PRIMARY_IDENTIFIER
                && ((isOptional = consumePunctuator(PunctuatorType::OPTIONAL_CHAINING))
                   || consumePunctuator(PunctuatorType::FULL_STOP))) {
            std::vector<MemberExpressionElement> identifiers{{std::move(static_cast<PrimaryExpression<std::string> *>(expr.get())->data()), isOptional}};

            --m_it; // undo consumption
            while (true) {
                if (m_it == std::end(m_tokens)) {
                    reportError(GeneratorError::UNEXPECTED_EOF);
                    return nullptr;
                }

                if (!(isOptional = consumePunctuator(PunctuatorType::OPTIONAL_CHAINING))
                        && !consumePunctuator(PunctuatorType::FULL_STOP)) {
                    break;
                }

                if (m_it->type() != TokenType::IDENTIFIER) {
                    reportError(GeneratorError::NO_IDENTIFIER_AFTER_MEMBER_OR_OPTIONAL_EXPRESSION, m_it->toString());
                    return nullptr;
                }

                identifiers.emplace_back(m_it->asString(), isOptional);
                ++m_it;
            }

            return std::make_unique<MemberExpression>(std::move(identifiers));
        }

        if (consumePunctuator(PunctuatorType::LEFT_SQUARE_BRACKET)) {
            auto inside = consumeExpression();
            if (!inside) {
                JS_SUBROUTINE_ERROR_TRACE("consumeMemberExpression: failed to parse inside of bracket property accessor expression");
                return nullptr;
            }
            if (!consumePunctuator(PunctuatorType::RIGHT_SQUARE_BRACKET)) {
                reportError(GeneratorError::GENERIC_EXPECTED_GOT, ']', m_it->toString());
                return nullptr;
            }
            return std::make_unique<BiExpression>(ExpressionType::CALL_PROPERTY_ACCESS, std::move(expr), std::move(inside));
        }

        return expr;
    }

    std::unique_ptr<Expression>
    Generator::consumeMultiplicativeExpression() noexcept {
        auto expr = consumeExponentiationExpression();

        if (expr == nullptr || m_it->type() != TokenType::PUNCTUATOR)
            return expr;

        const auto punc = m_it->asPunctuator();
        if (punc != PunctuatorType::MULTIPLY && punc != PunctuatorType::DIVIDE && punc != PunctuatorType::REMAINDER)
            return expr;

        ++m_it;
        // TODO should this be consumeExponentiationExpression()
        //      https://tc39.es/ecma262/multipage/ecmascript-language-expressions.html#prod-MultiplicativeExpression
        auto rhs = consumeMultiplicativeExpression();
        if (!rhs) {
            --m_it;
            return expr;
        }

        const auto expressionType = punc == PunctuatorType::MULTIPLY
                ? ExpressionType::BI_MULTIPLY
                : punc == PunctuatorType::DIVIDE
                        ? ExpressionType::BI_DIVIDE
                        : ExpressionType::BI_REMAINDER;
        return std::make_unique<BiExpression>(expressionType, std::move(expr), std::move(rhs));
    }

    std::unique_ptr<Expression>
    Generator::consumeObjectLiteralExpression() noexcept {
        auto expression = std::make_unique<ObjectLiteral>();

        bool end{false};
        while (true) {
            if (m_it == std::cend(m_tokens)) {
                reportError(GeneratorError::UNEXPECTED_EOF);
                return nullptr;
            }

            if (consumePunctuator(PunctuatorType::RIGHT_CURLY_BRACKET)) {
                return expression;
            }

            if (end) {
                if (!consumePunctuator(PunctuatorType::COMMA)) {
                    reportError(GeneratorError::GENERIC_EXPECTED_GOT_2, '}', ',', m_it->toString());
                    return nullptr;
                }
                end = false;
                continue;
            }

            if (m_it->type() == TokenType::IDENTIFIER) {
                const auto &identifier = m_it->asString();
                ++m_it;

                if (consumePunctuator(PunctuatorType::COLON)) {
                    auto assignment = consumeAssignmentExpression();
                    if (assignment == nullptr) {
                        return nullptr;
                    }

                    expression->definitions().emplace_back(identifier, std::move(assignment));
                } else {
                    expression->definitions().emplace_back(identifier);
                }

                end = true;
                continue;
            }

            reportError(GeneratorError::OBJECT_EXPRESSION_UNEXPECTED_TOKEN, m_it->toString());
            return nullptr;
        }
    }

    std::unique_ptr<Expression>
    Generator::consumeOptionalChain(std::unique_ptr<Expression> &&lhs) noexcept {
        auto expr = std::move(lhs);

        while (m_it != std::cend(m_tokens)) {
            if (!consumePunctuator(PunctuatorType::OPTIONAL_CHAINING))
                break;

            // TODO
            break;
        }

        return expr;
    }

    std::unique_ptr<Expression>
    Generator::consumePrimaryExpression() noexcept {
        if (consumePunctuator(PunctuatorType::LEFT_PARENTHESIS)) {
            auto expr = consumeExpression();
            if (!consumePunctuator(PunctuatorType::RIGHT_PARENTHESIS)) {
                reportError(GeneratorError::GENERIC_EXPECTED_GOT, ')', m_it->toString());
                return nullptr;
            }

            return expr;
        }

        if (consumePunctuator(PunctuatorType::LEFT_CURLY_BRACKET)) {
            return consumeObjectLiteralExpression();
        }

        switch (m_it->type()) {
            case TokenType::INT:
                return std::make_unique<PrimaryExpression<SignedInt>>((m_it++)->asSigned());
            case TokenType::IDENTIFIER:
                return std::make_unique<PrimaryExpression<std::string>>(ExpressionType::PRIMARY_IDENTIFIER, (m_it++)->asString());
            case TokenType::STRING:
                return std::make_unique<PrimaryExpression<std::string>>(ExpressionType::PRIMARY_STRING, (m_it++)->asString());
            case TokenType::KEYWORD:
                if (auto expressionType = convertKeywordToExpressionType(m_it->asKeyword());
                            expressionType != ExpressionType::INVALID) {
                    ++m_it;
                    return std::make_unique<Expression>(expressionType);
                }
                if (m_it->asKeyword() == KeywordType::FUNCTION) {
                    return consumeFunctionExpression();
                }
                [[fallthrough]];
            default:
                reportError(GeneratorError::UNKNOWN_START_OF_EXPRESSION, m_it->toString());
                return nullptr;
        }
    }

    std::unique_ptr<Expression>
    Generator::consumeRelationalExpression() noexcept {
        auto expr = consumeShiftExpression();
        if (!expr || m_it == std::end(m_tokens))
            return expr;

        const auto expressionType = convertTokenToRelationExpressionType(*m_it);
        if (expressionType == ExpressionType::INVALID) {
            return expr;
        }

        ++m_it;
        auto rhs = consumeRelationalExpression();
        if (!rhs)
            return nullptr;

        return std::make_unique<BiExpression>(expressionType, std::move(expr), std::move(rhs));
    }

    bool
    Generator::consumeReturnStatement() noexcept {
        ++m_it; // consume 'RETURN' keyword

        if (consumeSemicolon()) {
            m_currentNode->addChild(std::make_unique<ReturnStatementNode>(nullptr));
            return true;
        }

        auto expr = consumeExpression();
        if (!expr) {
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeReturnStatement -> consumeExpression(failed)");
            return false;
        }

        if (!consumeSemicolon()) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, ';', m_it->toString());
            return false;
        }

        m_currentNode->addChild(std::make_unique<ReturnStatementNode>(std::move(expr)));
        return true;
    }

    bool
    Generator::consumeSemicolon() noexcept {
        if (consumePunctuator(PunctuatorType::SEMICOLON))
            return true;

        // 12.9.1.  1.1.
        if (m_it != std::cbegin(m_tokens)
                && (m_it - 1)->isLineTerminator()) {
            return true;
        }

        // 12.9.1.  1.2.
        if (m_it != std::cend(m_tokens)
               && (m_it - 1)->type() == TokenType::PUNCTUATOR
               && (m_it - 1)->asPunctuator() == PunctuatorType::RIGHT_CURLY_BRACKET) {
            return true;
        }

        // 12.9.1.  2.
        if (m_it == std::cend(m_tokens))
            return true;

        return false;
    }

    std::unique_ptr<Expression>
    Generator::consumeShiftExpression() noexcept {
        auto expr = consumeAdditiveExpression();

        if (expr == nullptr || m_it->type() != TokenType::PUNCTUATOR)
            return expr;

        const auto punctuator = m_it->asPunctuator();
        ExpressionType type;
        if (punctuator == PunctuatorType::LEFT_SHIFT) {
            type = ExpressionType::BI_LEFT_SHIFT;
        } else if (punctuator == PunctuatorType::RIGHT_SHIFT) {
            type = ExpressionType::BI_RIGHT_SHIFT;
        } else if (punctuator == PunctuatorType::ZERO_FILL_RIGHT_SHIFT) {
            type = ExpressionType::BI_UNSIGNED_RIGHT_SHIFT;
        } else {
            return expr;
        }

        ++m_it;
        auto rhs = consumeShiftExpression();
        if (!rhs) {
            --m_it;
            return expr;
        }

        return std::make_unique<BiExpression>(type, std::move(expr), std::move(rhs));
    }

    std::unique_ptr<Expression>
    Generator::consumeShortCircuitExpression() noexcept {
        auto expr = consumeEqualityExpression();

        if (expr == nullptr || m_it->type() != TokenType::PUNCTUATOR)
            return expr;

        const auto punctuator = m_it->asPunctuator();
        ExpressionType type;
        if (punctuator == PunctuatorType::LOGICAL_OR) {
            type = ExpressionType::LOGICAL_OR;
        } else if (punctuator == PunctuatorType::LOGICAL_AND) {
            type = ExpressionType::LOGICAL_AND;
        } else if (punctuator == PunctuatorType::LOGICAL_NULLISH) {
            type = ExpressionType::COALESCE;
            // TODO this should use a different rhs (CoalesceExpressionHead)
        } else {
            return expr;
        }

        ++m_it;
        auto rhs = consumeShortCircuitExpression();
        if (!rhs) {
            --m_it;
            return expr;
        }

        return std::make_unique<BiExpression>(type, std::move(expr), std::move(rhs));
    }

    std::unique_ptr<Expression>
    Generator::consumeUnaryExpression() noexcept {
        if (consumePunctuator(PunctuatorType::ADD)) {
            auto expr = consumeUnaryExpression();
            if (expr == nullptr)
                return expr;
            return std::make_unique<UniExpression>(ExpressionType::UNI_PLUS, std::move(expr));
        }

        if (consumePunctuator(PunctuatorType::SUBTRACT)) {
            auto expr = consumeUnaryExpression();
            if (expr == nullptr)
                return expr;
            return std::make_unique<UniExpression>(ExpressionType::UNI_MINUS, std::move(expr));
        }

        if (consumePunctuator(PunctuatorType::BITWISE_NOT)) {
            auto expr = consumeUnaryExpression();
            if (expr == nullptr)
                return expr;
            return std::make_unique<UniExpression>(ExpressionType::UNI_BITWISE_NOT, std::move(expr));
        }

        if (consumePunctuator(PunctuatorType::EXCLAMATION_MARK)) {
            auto expr = consumeUnaryExpression();
            if (expr == nullptr)
                return expr;
            return std::make_unique<UniExpression>(ExpressionType::UNI_LOGICAL_NOT, std::move(expr));
        }

        return consumeUpdateExpression();
    }

    std::unique_ptr<Expression>
    Generator::consumeUpdateExpression() noexcept {
        if (consumePunctuator(PunctuatorType::INCREMENT)) {
            auto expr = consumeUnaryExpression();
            if (!expr)
                return nullptr;
            return std::make_unique<UpdateExpression>(UpdateExpressionType::PRE_INCREMENT, std::move(expr));
        }

        if (consumePunctuator(PunctuatorType::DECREMENT)) {
            auto expr = consumeUnaryExpression();
            if (!expr)
                return nullptr;
            return std::make_unique<UpdateExpression>(UpdateExpressionType::PRE_INCREMENT, std::move(expr));
        }

        auto expr = consumeLHSExpression();
        if (!expr)
            return nullptr;

        if (consumePunctuator(PunctuatorType::INCREMENT))
            return std::make_unique<UpdateExpression>(UpdateExpressionType::POST_INCREMENT, std::move(expr));

        if (consumePunctuator(PunctuatorType::DECREMENT))
            return std::make_unique<UpdateExpression>(UpdateExpressionType::POST_DECREMENT, std::move(expr));

        return expr;
    }

    bool
    Generator::consumeLexicalDeclaration(LexicalDeclarationType type) noexcept {
        auto statement = std::make_unique<DeclarationStatementNode>(
                type == LexicalDeclarationType::LET
                ? DeclarationType::LEXICAL_LET
                : DeclarationType::LEXICAL_CONST
        );

        // Consume 'let' or 'const'
        ++m_it;



        while (m_it != std::cend(m_tokens)) {
            if (m_it->type() != TokenType::IDENTIFIER) {
                reportError(GeneratorError::NO_IDENTIFIER_AFTER_LEXICAL_OR_VARIABLE_DECLARATION,
                            JS::toString(type), m_it->toString());
                return false;
            }

            const auto &identifier = m_it->asString();

            ++m_it;
            #undef JS_CHECK_EOF_RESULT
            #define JS_CHECK_EOF_RESULT false

            JS_CHECK_EOF()

            if (m_it->type() != TokenType::PUNCTUATOR) {
                reportError(GeneratorError::LEXICAL_DECLARATION_NO_EQUALS_COMMA_OR_SEMICOLON,
                            JS::toString(type), identifier, m_it->toString());
                return false;
            }

            if (m_it->asPunctuator() == PunctuatorType::ASSIGNMENT) {
                ++m_it;
                JS_CHECK_EOF()

                auto expr = consumeAssignmentExpression();
                statement->declarations().emplace_back(identifier, std::move(expr));

                JS_CHECK_EOF()
                if (m_it->type() != TokenType::PUNCTUATOR) {
                    reportError(GeneratorError::GENERIC_EXPECTED_GOT, ';', m_it->toString());
                    return false;
                }
            } else {
                statement->declarations().emplace_back(identifier);
            }

            JS_CHECK_EOF()
            const auto punc = m_it->asPunctuator();

            if (peekSemicolon()) {
                m_currentNode->addChild(std::move(statement));
                return true;
            }

            if (punc == PunctuatorType::COMMA) {
                ++m_it;
                continue;
            }

            reportError(GeneratorError::LEXICAL_DECLARATION_NO_EQUALS_COMMA_OR_SEMICOLON,
                        JS::toString(type), identifier, m_it->toString());
            return false;
        }

        reportError(GeneratorError::UNEXPECTED_EOF);
        return false;
    }

    bool
    Generator::consumeWhileStatement() noexcept {
        if (!consumePunctuator(PunctuatorType::LEFT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, '(', m_it->toString());
            return false;
        }

        auto condition = consumeExpression();
        if (condition == nullptr) {
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to generate while condition expression");
            return false;
        }

        if (!consumePunctuator(PunctuatorType::RIGHT_PARENTHESIS)) {
            reportError(GeneratorError::GENERIC_EXPECTED_GOT, ')', m_it->toString());
            return false;
        }

        auto *const savedCurrentNode = m_currentNode;

        ParentNode node{NodeType::WHILE_STATEMENT};
        node.parent(m_currentNode);
        m_currentNode = &node;

        if (auto status = processToken(); status != TokenProcessError::NONE) {
            m_errorStack.print();
            m_currentNode = savedCurrentNode;
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: consumeWhileStatement: processToken => {}", JS::toString(status));
            return false;
        }

        JS_ASSERT(std::size(node.children()) == 1);

        m_currentNode = savedCurrentNode;
        m_currentNode->addChild(std::make_unique<WhileStatementNode>(
                std::move(condition),
                std::move(node.children()[0])
        ));

        return true;
    }

    ExpressionType
    Generator::convertKeywordToExpressionType(KeywordType keyword) noexcept {
        switch (keyword) {
            case KeywordType::KW_NULL: return ExpressionType::LITERAL_NULL;
            case KeywordType::TRUE: return ExpressionType::LITERAL_TRUE;
            case KeywordType::FALSE: return ExpressionType::LITERAL_FALSE;
            default: return ExpressionType::INVALID;
        }
    }

    ExpressionType
    Generator::convertPunctuatorToEqualityExpressionType(PunctuatorType punctuator) noexcept {
        switch (punctuator) {
            case PunctuatorType::EQUAL: return ExpressionType::BI_EQUAL;
            case PunctuatorType::NOT_EQUAL: return ExpressionType::BI_NOT_EQUAL;
            case PunctuatorType::STRICT_EQUAL: return ExpressionType::BI_STRICT_EQUAL;
            case PunctuatorType::STRICT_NOT_EQUAL: return ExpressionType::BI_NOT_STRICT_EQUAL;
            default: return ExpressionType::INVALID;
        }
    }

    ExpressionType
    Generator::convertTokenToRelationExpressionType(const Token &token) noexcept {
        if (token.type() == TokenType::KEYWORD) {
            if (token.asKeyword() == KeywordType::IN)
                return ExpressionType::RELATIONAL_IN;
            if (token.asKeyword() == KeywordType::INSTANCEOF)
                return ExpressionType::RELATIONAL_INSTANCEOF;
            return ExpressionType::INVALID;
        }

        if (token.type() != TokenType::PUNCTUATOR) {
            return ExpressionType::INVALID;
        }

        switch (token.asPunctuator()) {
            case PunctuatorType::LESS_THAN: return ExpressionType::BI_LESS_THAN;
            case PunctuatorType::LESS_THAN_OR_EQUAL: return ExpressionType::BI_LESS_THAN_OR_EQUAL;
            case PunctuatorType::GREATER_THAN: return ExpressionType::BI_GREATER_THAN;
            case PunctuatorType::GREATER_THAN_OR_EQUAL: return ExpressionType::BI_GREATER_THAN_OR_EQUAL;
            default: return ExpressionType::INVALID;
        }
    }

    LexicalDeclarationType
    Generator::findLexicalDeclarationType() const noexcept {
        if (m_it->type() == TokenType::IDENTIFIER
                && m_it->asString() == "let")
            return LexicalDeclarationType::LET;

        if (m_it->type() == TokenType::KEYWORD
                && m_it->asKeyword() == KeywordType::CONST)
            return LexicalDeclarationType::CONST;

        return LexicalDeclarationType::NONE;
    }

    bool Generator::peekSemicolon() const noexcept {
        if (m_it->type() == TokenType::PUNCTUATOR
                && m_it->asPunctuator() == PunctuatorType::SEMICOLON)
            return true;

        // 12.9.1.  1.1.
        if (m_it != std::cbegin(m_tokens)
            && (m_it - 1)->isLineTerminator()) {
            return true;
        }

        // 12.9.1.  1.2.
        if (m_it != std::cend(m_tokens)
            && (m_it - 1)->type() == TokenType::PUNCTUATOR
            && (m_it - 1)->asPunctuator() == PunctuatorType::RIGHT_CURLY_BRACKET) {
            return true;
        }

        // 12.9.1.  2.
        if (m_it == std::cend(m_tokens))
            return true;

        return false;
    }


    template <typename...Args>
    void
    Generator::reportError(const Token *token, GeneratorError error, Args &&...args) noexcept {
        std::vector<std::string> result{};

        auto string = getFormatString(error);
        JS_ASSERT(!string.empty());
        result.push_back(fmt::format(fmt::runtime(string), std::forward<Args>(args)...));

        if (error == GeneratorError::UNEXPECTED_EOF) {
            m_errorStack.push(std::move(result));
            return;
        }

        const auto targetLine{token->sourceLocation().line()};

        result.push_back(fmt::format("Error occurred here: {}:{}:{}:", m_fileName,
                         targetLine, token->sourceLocation().column()));

        if (targetLine == 0) {
            m_errorStack.push(std::move(result));
            return;
        }

        std::size_t line{1};
        auto fromThisLine = std::cend(m_inputFile);
        auto endOfLine = std::cend(m_inputFile);
        for (auto it = std::cbegin(m_inputFile); it != std::cend(m_inputFile); ++it) {
            if (*it == '\n') {
                ++line;

                if (fromThisLine != std::cend(m_inputFile)) {
                    endOfLine = it;
                    break;
                }

                continue;
            }

            if (line == targetLine && fromThisLine == std::cend(m_inputFile)) {
                fromThisLine = it;
            }
        }

        if (fromThisLine != std::cend(m_inputFile)) {
            const auto lineLength = static_cast<std::size_t>(std::distance(fromThisLine, endOfLine));

            result.emplace_back(m_inputFile.substr(
                    static_cast<std::size_t>(std::distance(std::cbegin(m_inputFile), fromThisLine)),
                    lineLength
            ));

            JS_ASSERT(token->sourceLocation().length() < 4096);

            if (token->sourceLocation().column() == 0) {
                result.emplace_back(token->sourceLocation().length(), '^');
            } else {
                result.push_back(std::string(token->sourceLocation().column() - 1, ' ')
                                 + std::string(token->sourceLocation().length(), '^'));
            }
        }

        m_errorStack.push(std::move(result));
    }

    bool
    Generator::generate() noexcept {
        while (m_it != std::cend(m_tokens)) {
            if (processToken() != TokenProcessError::NONE) {
                m_errorStack.print();
                return false;
            }
        }

        return true;
    }

    TokenProcessError
    Generator::processToken(ParentNode *intendedScope) noexcept {
        // consumeSemicolon shouldn't be used here (i think)
        // TODO we should generate an empty expression.
        if (consumePunctuator(PunctuatorType::SEMICOLON)) {
            return TokenProcessError::NONE;
        }

        if (consumePunctuator(PunctuatorType::RIGHT_CURLY_BRACKET)) {
            if (m_currentNode == intendedScope) {
                return TokenProcessError::END_OF_INTENDED_SCOPE_REACHED;
            }

            if (m_currentNode->parent() == nullptr) {
                reportError(m_it - 1, GeneratorError::UNEXPECTED_RIGHT_CURLY_BRACKET);
                return TokenProcessError::MISPLACED_RIGHT_CURLY_BRACKET;
            }

            m_currentNode = m_currentNode->parent();
            return TokenProcessError::NONE;
        }

        const auto lexicalDeclarationType = findLexicalDeclarationType();
        if (lexicalDeclarationType != LexicalDeclarationType::NONE) {
            if (!consumeLexicalDeclaration(lexicalDeclarationType)) {
                JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to produce a lexical declaration!");
                return TokenProcessError::GENERIC;
            }

            return TokenProcessError::NONE;
        }

        if (m_it->type() == TokenType::KEYWORD) {
            if (m_it->asKeyword() == KeywordType::FUNCTION) {
                if (!consumeFunctionDeclaration()) {
                    JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to produce a function declaration!");
                    return TokenProcessError::GENERIC;
                }
                return TokenProcessError::NONE;
            }

            if (m_it->asKeyword() == KeywordType::RETURN) {
                if (!consumeReturnStatement()) {
                    JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to produce a return statement node!");
                    return TokenProcessError::GENERIC;
                }
                return TokenProcessError::NONE;
            }

            if (consumeKeyword(KeywordType::IF)) {
                if (!consumeIfStatement()) {
                    JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to produce an if statement node!");
                    return TokenProcessError::GENERIC;
                }
                return TokenProcessError::NONE;
            }

            if (consumeKeyword(KeywordType::WHILE)) {
                if (!consumeWhileStatement()) {
                    JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to produce a while statement node!");
                    return TokenProcessError::GENERIC;
                }
                return TokenProcessError::NONE;
            }

            if (consumeKeyword(KeywordType::FOR)) {
                if (!consumeForStatement()) {
                    JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: failed to produce a for statement node!");
                    return TokenProcessError::GENERIC;
                }
                return TokenProcessError::NONE;
            }

            if (consumeKeyword(KeywordType::DEBUGGER)) {
                if (!consumeSemicolon()) {
                    reportError(GeneratorError::NO_SEMICOLON_AFTER_EXPRESSION_STATEMENT, m_it->toString());
                    return TokenProcessError::GENERIC;
                }

                m_currentNode->addChild(std::make_unique<DebuggerStatementNode>(
                        (m_it - 2)->sourceLocation()
                ));
                return TokenProcessError::NONE;
            }
        }

        if (consumePunctuator(PunctuatorType::LEFT_CURLY_BRACKET)) {
            return consumeBlock() ? TokenProcessError::NONE : TokenProcessError::GENERIC;
        }

        // TODO use https://tc39.es/ecma262/multipage/ecmascript-language-statements-and-declarations.html#sec-expression-statement
        auto expr = consumeAssignmentExpression();
        if (!expr) {
            JS_SUBROUTINE_ERROR_TRACE("ASTGenerator: generate -> consumeAssignmentExpression(failed)");
            return TokenProcessError::GENERIC;
        }

        if (!consumeSemicolon()) {
            reportError(GeneratorError::NO_SEMICOLON_AFTER_EXPRESSION_STATEMENT, m_it->toString());
        }

        m_currentNode->addChild(std::make_unique<ExpressionStatementNode>(std::move(expr)));
        return TokenProcessError::NONE;
    }

} // namespace JS::AST
