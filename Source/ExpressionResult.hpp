/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <variant>
#include <utility>

#include <cstddef> // for std::nullptr_t

#include "Source/PropertyOrVariable.hpp"
#include "Source/PropertyReference.hpp"
#include "Source/ReferenceRecord.hpp"
#include "Source/Value.hpp"
#include "Source/Variable.hpp"

namespace JS {

    struct ExpressionResult
            // nullptr(error)
            : public std::variant<std::nullptr_t, Value, PropertyReference, Variable *> {

        [[nodiscard]] ExpressionResult() noexcept
                : variant(nullptr) {
        }

        [[nodiscard]] inline
        ExpressionResult(std::nullptr_t) noexcept
                : variant(nullptr) {
        }

        [[nodiscard]] inline
        ExpressionResult(Value &&value) noexcept
                : variant(std::move(value)) {
        }

        [[nodiscard]] inline
        ExpressionResult(PropertyReference &&propertyReference) noexcept
                : variant(std::move(propertyReference)) {
            JS_ASSERT(std::get<PropertyReference>(*this).property());
        }

        [[nodiscard]] inline
        ExpressionResult(Variable *variable) noexcept
                : variant(variable) {
            JS_ASSERT(variable);
        }

        [[nodiscard]] inline
        ExpressionResult(ReferenceRecord &&record) noexcept {
            if (record.isProperty()) {
                variant::operator=(std::get<PropertyReference>(record));
            } else if (record.isObject()) {
                variant::operator=(std::move(std::get<std::shared_ptr<Object>>(record)));
            }
        }

        [[nodiscard]] inline
        ExpressionResult(PropertyOrVariable &&var) noexcept {
            if (var.isProperty()) {
                variant::operator=(std::get<PropertyReference>(var));
            } else if (var.isVariable()) {
                variant::operator=(std::get<Variable *>(var));
            }
        }

        [[nodiscard]] inline
        ExpressionResult(std::pair<bool, Value> &&pair) noexcept {
            if (pair.first)
                variant::operator=(std::move(pair.second));
        }

        [[nodiscard]] inline bool
        isError() const noexcept {
            return index() == 0;
        }

        [[nodiscard]] inline bool
        isValue() const noexcept {
            return index() == 1;
        }

        [[nodiscard]] inline bool
        isProperty() const noexcept {
            return index() == 2;
        }

        [[nodiscard]] inline bool
        isVariable() const noexcept {
            return index() == 3;
        }

        [[nodiscard]] inline Value
        toValue() const noexcept {
            switch (index()) {
                case 0: return {JS::Undefined{}};
                case 1: return std::get<Value>(*this);
                case 2: {
                    const auto &reference = std::get<PropertyReference>(*this);
                    if (reference.property())
                        return reference.property()->value();
                    return {JS::Undefined{}};
                }
                case 3: {
                    auto *variable = std::get<Variable *>(*this);
                    if (variable)
                        return variable->value();
                    return {JS::Undefined{}};
                }
                default:
                    JS_ASSERT(false);
                    return {};
            }
        }
    };

} // namespace JS
