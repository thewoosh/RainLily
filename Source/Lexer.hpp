/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>
#include <vector>

#include "Source/Base.hpp"
#include "Source/Token.hpp"

namespace JS {

    class Lexer {
        std::string_view m_input;
        std::vector<Token> m_tokens{};
        SourceLocation m_sourceLocation{1, 1};

        using Iter = std::string_view::const_iterator;
        Iter m_it;

        bool m_isLineComment{false};
        bool m_isBlockComment{false};

        [[nodiscard]] inline constexpr SourceLocation
        createLocation(SourceLocation startLocation) noexcept {
            if (m_sourceLocation.line() != startLocation.line())
                return startLocation;

            return SourceLocation{
                startLocation.line(),
                startLocation.column(),
                m_sourceLocation.column() - startLocation.column()
            };
        }

        inline void
        increasePosition(std::size_t delta) noexcept {
            m_it += delta;
            m_sourceLocation.column() += delta;
        }

        [[nodiscard]] bool
        consumeCharacter(char32_t character) noexcept;

        [[nodiscard]] bool
        consumeNumber() noexcept;

        void
        consumeIdentifier() noexcept;

        [[nodiscard]] bool
        consumePunctuator() noexcept;

        [[nodiscard]] bool
        consumeStringLiteral(char32_t enclosingCharacter) noexcept;

    public:
        [[nodiscard]] inline explicit
        Lexer(std::string_view input) noexcept
            : m_input(input)
            , m_it(std::cbegin(m_input)) {
        }

        [[nodiscard]] bool
        parse() noexcept;

        [[nodiscard]] inline std::vector<Token> &
        tokens() noexcept {
            return m_tokens;
        }

        [[nodiscard]] inline const std::vector<Token> &
        tokens() const noexcept {
            return m_tokens;
        }
    };

} // namespace JS
