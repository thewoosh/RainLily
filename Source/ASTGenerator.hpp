/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <vector>

#include "Source/Enums/GeneratorError.hpp"
#include "Source/Enums/LexicalDeclarationType.hpp"
#include "Source/Enums/TokenProcessError.hpp"
#include "Source/ASTNode.hpp"
#include "Source/ErrorStack.hpp"
#include "Source/Token.hpp"

namespace JS::AST {

    class Generator {
        const std::vector<Token> &m_tokens;

        using Iter = std::vector<Token>::const_iterator;
        Iter m_it;

        const std::string_view m_fileName{};
        const std::string_view m_inputFile{};

        RootNode m_rootNode{};
        ParentNode *m_currentNode{&m_rootNode};
        ErrorStack m_errorStack{};

        [[nodiscard]] std::unique_ptr<Expression>
        consumeAdditiveExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeAssignmentExpression() noexcept;

        [[nodiscard]] bool
        consumeBlock() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeCallArguments(std::unique_ptr<Expression> &&lhs) noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeCallExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeEqualityExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeExponentiationExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeExpression() noexcept;

        [[nodiscard]] bool
        consumeForStatement() noexcept;

        [[nodiscard]] std::optional<FunctionDeclaration>
        consumeFunction(bool canBeDefault) noexcept;

        [[nodiscard]] bool
        consumeFunctionDeclaration() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeFunctionExpression() noexcept;

        [[nodiscard]] bool
        consumeIfStatement() noexcept;

        [[nodiscard]] bool
        consumeKeyword(KeywordType type) noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeLHSExpression() noexcept;

        [[nodiscard]] bool
        consumeLexicalDeclaration(LexicalDeclarationType type) noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeMemberExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeMultiplicativeExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeObjectLiteralExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeOptionalChain(std::unique_ptr<Expression> &&lhs) noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumePrimaryExpression() noexcept;

        [[nodiscard]] bool
        consumePunctuator(PunctuatorType type) noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeRelationalExpression() noexcept;

        [[nodiscard]] bool
        consumeReturnStatement() noexcept;

        /**
         * Consumes a semicolon. Also accounts for the rules of automatic
         * semicolon insertion.
         *
         * https://tc39.es/ecma262/multipage/ecmascript-language-lexical-grammar.html#sec-automatic-semicolon-insertion
         */
        [[nodiscard]] bool
        consumeSemicolon() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeShiftExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeShortCircuitExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeUnaryExpression() noexcept;

        [[nodiscard]] std::unique_ptr<Expression>
        consumeUpdateExpression() noexcept;

        [[nodiscard]] bool
        consumeWhileStatement() noexcept;

        [[nodiscard]] static ExpressionType
        convertKeywordToExpressionType(KeywordType) noexcept;

        [[nodiscard]] static ExpressionType
        convertPunctuatorToEqualityExpressionType(PunctuatorType) noexcept;

        [[nodiscard]] static ExpressionType
        convertTokenToRelationExpressionType(const Token &) noexcept;


        /**
         * Consumes a semicolon. Also accounts for the rules of automatic
         * semicolon insertion.
         *
         * https://tc39.es/ecma262/multipage/ecmascript-language-lexical-grammar.html#sec-automatic-semicolon-insertion
         */
        [[nodiscard]] bool
        peekSemicolon() const noexcept;

        [[nodiscard]] LexicalDeclarationType
        findLexicalDeclarationType() const noexcept;

        template <typename...Args>
        void
        reportError(const Token *, GeneratorError error, Args &&...) noexcept;

        template <typename...Args>
        inline void
        reportError(GeneratorError error, Args &&...args) noexcept {
            reportError(&(*m_it), error, std::forward<Args>(args)...);
        }

        template <typename...Args>
        inline void
        reportError(const std::vector<Token>::const_iterator &iter, GeneratorError error, Args &&...args) noexcept {
            reportError(&(*iter), error, std::forward<Args>(args)...);
        }

        [[nodiscard]] TokenProcessError
        processToken(ParentNode *intendedScope = nullptr) noexcept;

    public:
        [[nodiscard]] inline explicit
        Generator(const std::vector<Token> &tokens, std::string_view fileName={}, std::string_view inputFile={}) noexcept
            : m_tokens(tokens)
            , m_it(std::cbegin(tokens))
            , m_fileName(fileName)
            , m_inputFile(inputFile) {
        }

        [[nodiscard]] bool
        generate() noexcept;

        [[nodiscard]] inline constexpr RootNode &
        rootNode() noexcept {
            return m_rootNode;
        }

        [[nodiscard]] inline constexpr const RootNode &
        rootNode() const noexcept {
            return m_rootNode;
        }
    };

} // namespace JS::AST
