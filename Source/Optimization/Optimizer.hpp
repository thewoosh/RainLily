/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory>

#include "Source/Optimization/OptimizationResult.hpp"
#include "Source/Base.hpp"

namespace JS::AST {

    class Optimizer {
        AST::RootNode *m_rootNode;

        [[nodiscard]] OptimizationResult
        optimizeLogicalAndExpression(std::unique_ptr<Expression> &) noexcept;

        template <typename Function>
        [[nodiscard]] static OptimizationResult
        optimizeSimpleBiExpression(std::unique_ptr<Expression> &, Function) noexcept;

        [[nodiscard]] OptimizationResult
        visitExpression(std::unique_ptr<Expression> &) noexcept;

        [[nodiscard]] static OptimizationResult
        visitNode(std::unique_ptr<Node> &) noexcept;

    public:
        [[nodiscard]] inline constexpr
        Optimizer(AST::RootNode *rootNode) noexcept
                : m_rootNode(rootNode) {
        }

        [[nodiscard]] bool
        run() noexcept;
    };

} // namespace JS::AST
