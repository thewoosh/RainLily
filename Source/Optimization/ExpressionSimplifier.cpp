/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Optimization/ExpressionSimplifier.hpp"

#include <optional>

#include <cassert>

#include "Source/ASTNode.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Expressions/BiExpression.hpp"
#include "Source/Expressions/CallExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"

namespace JS::AST {

    [[nodiscard]] inline static std::optional<bool>
    simpleToBoolean(const Expression *expression) noexcept {
        switch (expression->expressionType()) {
            case ExpressionType::LITERAL_FALSE:
            case ExpressionType::LITERAL_NULL:
                return false;
            case ExpressionType::LITERAL_TRUE:
                return true;
            case ExpressionType::PRIMARY_IDENTIFIER:
                return static_cast<const PrimaryExpression<std::string> *>(expression)->data() == "undefined";
            case ExpressionType::PRIMARY_SIGNED_INT: {
                const auto value = std::data(*static_cast<const PrimaryExpression<SignedInt> *>(expression));
                return value != 0;
            }
            default:
                return std::nullopt;
        }
    }

    void
    ExpressionSimplifier::run() noexcept {
        visitNode(rootNode());
    }

    void
    ExpressionSimplifier::visitExpression(std::unique_ptr<Expression> &expression) noexcept {
        while (true) {
            switch (expression->expressionType()) {
                case ExpressionType::CALL_REGULAR: {
                    auto *expr = static_cast<CallExpression *>(expression.get());
                    visitExpression(expr->calleeData());
                    for (auto &param : expr->parameters())
                        visitExpression(param);
                    return;
                }
                case ExpressionType::LOGICAL_AND: {
                    auto *expr = static_cast<BiExpression *>(expression.get());
                    visitExpression(expr->lhsData());
                    visitExpression(expr->rhsData());

                    const auto boolean = simpleToBoolean(expr->lhs());
                    if (boolean.has_value()) {
                        if (boolean.value()) {
                            expression = std::move(expr->rhsData());
                            markSomethingChanged();
                            continue;
                        }

                        expression = std::move(expr->lhsData());
                        markSomethingChanged();
                    }

                    return;
                }
                case ExpressionType::LOGICAL_OR: {
                    auto *expr = static_cast<BiExpression *>(expression.get());
                    visitExpression(expr->lhsData());
                    visitExpression(expr->rhsData());

                    const auto boolean = simpleToBoolean(expr->lhs());
                    if (boolean.has_value()) {
                        if (!boolean.value()) {
                            expression = std::move(expr->rhsData());
                            markSomethingChanged();
                            continue;
                        }

                        expression = std::move(expr->lhsData());
                        markSomethingChanged();
                    }

                    return;
                }
                default:
                    return;
            }
        }
    }

    void
    ExpressionSimplifier::visitNode(Node *node) noexcept {
        switch (node->nodeType()) {
            case NodeType::FUNCTION_DECLARATION:
            case NodeType::ROOT:
            case NodeType::BLOCK:
                for (auto &child : static_cast<ParentNode *>(node)->children())
                    visitNode(child.get());
                return;
            case NodeType::DECLARATION_STATEMENT:
                for (auto &decl : static_cast<DeclarationStatementNode *>(node)->declarations())
                    if (decl.expression() != nullptr)
                        visitExpression(decl.data());
                return;
            case NodeType::EXPRESSION_STATEMENT:
                return visitExpression(static_cast<ExpressionStatementNode *>(node)->data());
            case NodeType::DEBUGGER_STATEMENT:
            case NodeType::FUNCTION_EXPRESSION:
                return;
            case NodeType::FOR_STATEMENT:
                visitExpression(static_cast<ForStatementNode *>(node)->firstExpressionData());
                visitExpression(static_cast<ForStatementNode *>(node)->secondExpressionData());
                visitExpression(static_cast<ForStatementNode *>(node)->thirdExpressionData());
                return visitNode(static_cast<ForStatementNode *>(node)->body());
            case NodeType::IF_STATEMENT:
                return visitExpression(static_cast<IfStatementNode *>(node)->conditionData());
            case NodeType::RETURN_STATEMENT:
                return visitExpression(static_cast<ReturnStatementNode *>(node)->data());
            case NodeType::WHILE_STATEMENT:
                visitExpression(static_cast<WhileStatementNode *>(node)->conditionData());
                return visitNode(static_cast<WhileStatementNode *>(node)->body());
#ifndef __clang__
            default:
                assert(false);
#endif
        }
    }

} // namespace JS::AST
