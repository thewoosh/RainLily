/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Optimization/Optimizer.hpp"

#include "Source/AST/Node.hpp"
#include "Source/ASTNode.hpp"
#include "Source/Optimization/ExpressionSimplifier.hpp"

namespace JS::AST {

    bool
    Optimizer::run() noexcept {
        bool somethingChanged{true};

        while (somethingChanged) {
            somethingChanged = false;

#define INVOKE_SUB_OPTIMIZER(name) { \
                name subOptimizer(m_rootNode); \
                subOptimizer.run(); \
                if (subOptimizer.result() == OptimizationResult::ERROR_FOUND) { \
                    return false; \
                } \
                if (subOptimizer.result() == OptimizationResult::SOMETHING_DID_CHANGE) { \
                    somethingChanged = true; \
                } \
            }

            INVOKE_SUB_OPTIMIZER(ExpressionSimplifier)

        }

        return true;
    }

} // namespace JS::AST
