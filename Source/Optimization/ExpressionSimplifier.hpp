/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory>

#include "Source/Optimization/SubOptimizer.hpp"

namespace JS {
    struct Expression;
} // namespace JS

namespace JS::AST {

    struct Node;

    class ExpressionSimplifier
            : public SubOptimizer {
    public:
        [[nodiscard]] inline constexpr explicit
        ExpressionSimplifier(RootNode *rootNode) noexcept
                : SubOptimizer(rootNode) {
        }

        void
        run() noexcept override;

    private:
        void
        visitExpression(std::unique_ptr<Expression> &) noexcept;

        void
        visitNode(Node *) noexcept;
    };

} // namespace JS::AST
