/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Optimization/OptimizationResult.hpp"

namespace JS::AST {

    struct RootNode;

    class SubOptimizer {
    public:
        [[nodiscard]] inline constexpr explicit
        SubOptimizer(RootNode *rootNode) noexcept
                : m_rootNode(rootNode) {
        }

        virtual ~SubOptimizer() noexcept = default;

        [[nodiscard]] inline constexpr OptimizationResult
        result() const noexcept {
            return m_optimizationResult;
        }

        [[nodiscard]] inline constexpr RootNode *
        rootNode() noexcept {
            return m_rootNode;
        }

        [[nodiscard]] inline constexpr const RootNode *
        rootNode() const noexcept {
            return m_rootNode;
        }

        virtual void
        run() noexcept = 0;

    protected:
        inline constexpr void
        markErrorFound() noexcept {
            m_optimizationResult = OptimizationResult::ERROR_FOUND;
        }

        inline constexpr void
        markSomethingChanged() noexcept {
            if (m_optimizationResult == OptimizationResult::NOTHING_CHANGED)
                m_optimizationResult = OptimizationResult::SOMETHING_DID_CHANGE;
        }

    private:
        RootNode *m_rootNode;
        OptimizationResult m_optimizationResult{OptimizationResult::NOTHING_CHANGED};
    };

} // namespace JS::AST
