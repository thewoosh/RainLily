/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

#include "Source/Base.hpp"

namespace JS {

    enum class OptimizationResult {
        ERROR_FOUND,
        NOTHING_CHANGED,
        SOMETHING_DID_CHANGE,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(OptimizationResult optimizationResult) noexcept {
        switch (optimizationResult) {
            case OptimizationResult::ERROR_FOUND: return "error-found";
            case OptimizationResult::NOTHING_CHANGED: return "nothing-changed";
            case OptimizationResult::SOMETHING_DID_CHANGE: return "something-did-change";
            default:
                JS_ASSERT(false);
                return "(invalid)";
        }
    }

} // namespace JS
