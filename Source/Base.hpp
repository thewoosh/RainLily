/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#if defined(__GNUC__) && !defined(__GXX_RTTI)
#   define JS_DYNAMIC_CAST static_cast
#else
#   define JS_DYNAMIC_CAST dynamic_cast
#endif

#ifndef JS_DISCARD
#   define JS_DISCARD(...) static_cast<void>(__VA_ARGS__)
#endif

#include <cassert>
#define JS_ASSERT assert

#define JS_PRIVATE_VISIBILITY private

namespace JS {

    using SignedInt = long long signed int;
    using FloatType = double;

    /**
     * Only used for niche cases such as the zero-fill(unsigned) right shift
     * operator.
     */
    using UnsignedInt = long long signed int;

    class Null{};
    class Undefined{};

    struct Declaration;
    struct FunctionDeclaration;

    struct Expression;
    struct Token;
    struct Value;

    struct AssignmentExpression;
    struct BiExpression;
    struct CallExpression;
    struct FunctionExpression;
    struct MemberExpression;
    struct ObjectLiteral;
    template <typename Contained>
    struct PrimaryExpression;
    struct UpdateExpression;

    namespace AST {

        struct Node;
        struct ParentNode;
        struct RootNode;
        struct DeclarationStatementNode;
        struct ExpressionStatementNode;
        struct ReturnStatementNode;
        struct FunctionDeclarationNode;

        class Optimizer;

    } // namespace AST;

    struct Object;
    class GlobalObject;

    namespace Objects {

        class MathObject;
        class StringObject;

    } // namespace Objects

    struct NativeCallInformation;
    struct NativeFunctionDescriptor;

    struct ExtensionSettings;
    struct GeneratorSettings;
    struct InterpreterSettings;

} // namespace JS
