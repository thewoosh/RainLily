/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Include/text/UnicodeBlocks/ArabicPresentationFormsB.hpp"
#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Include/text/UnicodeBlocks/C1ControlsAndLatin1Supplement.hpp"
#include "Include/text/UnicodeBlocks/GeneralPunctuation.hpp"
#include "Include/text/Tools.hpp"

namespace JS::LexerRules {

    template<typename T>
    [[nodiscard]] inline bool
    isIdentifierStart(T codePoint) {
        return text::isASCIIAlpha(codePoint)
            || codePoint == text::LOW_LINE
            || codePoint == text::DOLLAR_SIGN;
    }

    template<typename T>
    [[nodiscard]] inline bool
    isIdentifierContinue(T codePoint) {
        return isIdentifierStart(codePoint)
            || text::isDigit(codePoint);
    }

    template<typename T>
    [[nodiscard]] inline bool
    isWhitespace(T codePoint) {
        return codePoint == text::CHARACTER_TABULATION
            || codePoint == text::LINE_TABULATION
            || codePoint == text::FORM_FEED
            || codePoint == text::SPACE
            || codePoint == text::NO_BREAK_SPACE
            || codePoint == text::ZERO_WIDTH_NO_BREAK_SPACE;
    }

    template<typename T>
    [[nodiscard]] inline bool
    isLineSeparator(T codePoint) {
        return codePoint == text::LINE_FEED
               || codePoint == text::CARRIAGE_RETURN
               || codePoint == text::LINE_SEPARATOR
               || codePoint == text::PARAGRAPH_SEPARATOR;
    }

} // namespace JS::LexerRules
