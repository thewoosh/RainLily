/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory> // for std::unique_ptr
#include <optional>
#include <string>

#include "Source/Base.hpp"
#include "Source/Expressions/Expression.hpp"

namespace JS {

    struct Declaration {
    private:
        std::string m_identifier;

        // nullable
        std::unique_ptr<Expression> m_expression;

    public:
        [[nodiscard]] inline explicit
        Declaration(std::string &&identifier, std::unique_ptr<Expression> &&expression = {}) noexcept
                : m_identifier(std::move(identifier))
                , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline explicit
        Declaration(std::string_view identifier, std::unique_ptr<Expression> &&expression = {}) noexcept
                : m_identifier(identifier)
                , m_expression(std::move(expression)) {
        }

        Declaration(Declaration &&) = default;
        Declaration &operator=(Declaration &&) = default;

        [[nodiscard]] inline std::unique_ptr<Expression> &
        data() noexcept {
            return m_expression;
        }

        [[nodiscard]] inline const Expression *
        expression() const noexcept {
            return m_expression.get();
        }

        [[nodiscard]] inline const std::string &
        identifier() const noexcept {
            return m_identifier;
        }
    };

    struct FunctionDeclaration {
    private:
        std::optional<std::string> m_name;
        std::vector<std::string> m_parameterNames{};

    public:
        [[nodiscard]] inline explicit
        FunctionDeclaration(std::optional<std::string> &&name={},
                            std::vector<std::string> &&parameterNames={}) noexcept
                : m_name(std::move(name))
                , m_parameterNames(std::move(parameterNames)) {
        }

        [[nodiscard]] FunctionDeclaration(FunctionDeclaration &&) noexcept = default;
        FunctionDeclaration &operator=(FunctionDeclaration &&) noexcept = default;

        [[nodiscard]] inline bool
        isAnonymous() const noexcept {
            return !m_name.has_value();
        }

        [[nodiscard]] inline const std::optional<std::string> &
        name() const noexcept {
            return m_name;
        }

        [[nodiscard]] inline const std::vector<std::string> &
        parameterNames() const noexcept {
            return m_parameterNames;
        }

        [[nodiscard]] inline std::string_view
        toString() const noexcept {
            if (m_name.has_value())
                return m_name.value();
            return "<anonymous function>";
        }
    };

} // namespace JS
