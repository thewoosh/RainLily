/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <utility> // for std::move

#include "Source/Value.hpp"

namespace JS {

    struct Property {
    private:
        Value m_value{Undefined{}};
        bool m_writable{true};
        bool m_enumerable{true};
        bool m_configurable{true};

    public:
        [[nodiscard]] inline explicit
        Property(Value &&value) noexcept
                : m_value(std::move(value)) {
        }

        [[nodiscard]] inline explicit
        Property(const Value &value) noexcept
                : m_value(value) {
        }

        [[nodiscard]] inline constexpr bool
        isConfigurable() const noexcept {
           return m_configurable;
        }

        [[nodiscard]] inline constexpr bool
        isEnumerable() const noexcept {
            return m_enumerable;
        }

        [[nodiscard]] inline constexpr bool
        isWritable() const noexcept {
            return m_writable;
        }

        inline constexpr Property &
        setConfigurable(bool value) noexcept {
            m_configurable = value;
            return *this;
        }

        inline constexpr Property &
        setEnumerable(bool value) noexcept {
            m_enumerable = value;
            return *this;
        }

        inline constexpr Property &
        setWritable(bool value) noexcept {
            m_writable = value;
            return *this;
        }

        [[nodiscard]] inline Value &
        value() noexcept {
            return m_value;
        }

        [[nodiscard]] inline const Value &
        value() const noexcept {
            return m_value;
        }

        [[nodiscard]] inline bool
        value(Value &&value) noexcept {
            m_value = std::move(value);
            return true;
        }

        [[nodiscard]] inline bool
        value(const Value &value) noexcept {
            m_value = value;
            return true;
        }
    };

} // namespace JS

