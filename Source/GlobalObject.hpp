/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <numeric>

#include "Source/Settings/ExtensionSettings.hpp"
#include "Source/Object.hpp"

namespace JS {

    class GlobalObject : public Object {
        const ExtensionSettings &m_extensionSettings;

        void
        addBasicProperties() noexcept;

        void
        addStringPrototype() noexcept;

    public:
        /**
         * https://tc39.es/ecma262/multipage/global-object.html#sec-value-properties-of-the-global-object
         */
        [[nodiscard]] explicit
        GlobalObject(const ExtensionSettings &) noexcept;
    };

} // namespace JS
