/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory>
#include <string>
#include <variant>

#include <cstddef> // for std::size_t

#include "Source/Enums/UpdateExpressionType.hpp"
#include "Source/Base.hpp"
#include "Source/Constants.hpp"
#include "Source/NativeFunctionDescriptor.hpp"

namespace JS {

    namespace ValueIndex {

        constexpr const std::size_t Undefined   = 0;
        constexpr const std::size_t Null        = 1;
        constexpr const std::size_t Bool        = 2;

        constexpr std::size_t SignedInt         = 3;
        constexpr std::size_t Float             = 4;

        constexpr std::size_t String            = 5;

        constexpr std::size_t ObjectRef         = 6;
        constexpr std::size_t FunctionReference = 7;
        constexpr std::size_t FunctionExpr      = 8;
        constexpr std::size_t NativeFunction    = 9;

    } // namespace ValueIndex

    struct Value
        : public std::variant<Undefined,
                              Null,
                              bool,
                              SignedInt,
                              FloatType,
                              std::string,
                              std::shared_ptr<Object>,
                              const AST::FunctionDeclarationNode *,
                              const FunctionExpression *,
                              NativeFunctionDescriptor
            > {
    public:
        [[nodiscard]] inline
        Value() noexcept
                : variant(Undefined{}) {
        }

        Value(Value &&) = default;
        Value(const Value &) = default;
        Value &operator=(Value &&) = default;
        Value &operator=(const Value &) = default;

        template <typename...T>
        [[nodiscard]] inline
        Value(T &&...t) noexcept
            : variant(t...) {
        }

        [[nodiscard]] inline FloatType
        asFloat() const noexcept {
            if (index() == ValueIndex::SignedInt)
                return static_cast<FloatType>(std::get<SignedInt>(*this));
            if (index() == ValueIndex::Float)
                return std::get<FloatType>(*this);
            return 0;
        }

        [[nodiscard]] inline std::uint32_t
        asUint32() const noexcept {
            if (index() == ValueIndex::SignedInt)
                return static_cast<std::uint32_t>(std::get<SignedInt>(*this));
            if (index() == ValueIndex::Float)
                return static_cast<std::uint32_t>(std::get<FloatType>(*this));
            return 0;
        }

        [[nodiscard]] Value
        doUpdate(UpdateExpressionType) noexcept;

        [[nodiscard]] inline bool
        isNumeric() const noexcept {
            return index() <= ValueIndex::String;
        }

        [[nodiscard]] bool
        toBoolean() const noexcept;

        [[nodiscard]] std::string
        toString() noexcept;

        /**
         * Converts the value to a primitive type.
         *
         * https://tc39.es/ecma262/multipage/abstract-operations.html#sec-toprimitive
         */
        [[nodiscard]] Value
        toPrimitive() const noexcept;

        [[nodiscard]] inline FloatType
        toFloat() const noexcept {
            if (index() == ValueIndex::Float)
                return std::get<FloatType>(*this);
            if (index() == ValueIndex::SignedInt)
                return static_cast<FloatType>(std::get<SignedInt>(*this));
            return Constants::nan;
        }

        /**
         * Converts the value to a number.
         *
         * https://tc39.es/ecma262/multipage/abstract-operations.html#sec-tonumber
         */
        [[nodiscard]] inline Value
        toNumber() const noexcept {
            switch (index()) {
                undef:
                case ValueIndex::Undefined:
                    return Constants::nan;
                case ValueIndex::Null:
                    return static_cast<FloatType>(+0);
                case ValueIndex::SignedInt:
                case ValueIndex::Float:
                    return *this;
                case ValueIndex::ObjectRef:
                    return toPrimitive().toNumber();
                default:
                    // TODO
                    goto undef;
            }
        }

        /**
         * Converts the value to a numeric type.
         *
         * https://tc39.es/ecma262/multipage/abstract-operations.html#sec-tonumeric
         */
        [[nodiscard]] inline Value
        toNumeric() const noexcept {
            return toPrimitive().toNumber();
        }

        [[nodiscard]] inline SignedInt
        toSignedInt() const noexcept {
            if (index() == ValueIndex::SignedInt)
                return std::get<SignedInt>(*this);
            if (index() == ValueIndex::Float)
                return static_cast<SignedInt>(std::get<FloatType>(*this));
            return toNumber().toSignedInt();
        }

    };

} // namespace JS
