/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Prototypes/StringPrototype.hpp"

#include "Source/LexerRules.hpp"
#include "Source/NativeCallInformation.hpp"
#include "Source/Object.hpp"

namespace JS {

    enum class StringTrimLocation {
        START, END, START_END,
    };

    [[nodiscard]] std::string
    trimString(const std::string &str, StringTrimLocation where) noexcept {
        std::string result{};
        result.reserve(std::size(str));

        bool start{where == StringTrimLocation::START || where == StringTrimLocation::START_END};
        for (auto character : str) {
            if (start && LexerRules::isWhitespace(character)) {
                continue;
            }
            start = false;
            result += character;
        }

        if (where == StringTrimLocation::END || where == StringTrimLocation::START_END) {
            auto it = std::end(result);
            while (it != std::begin(result)) {
                if (!LexerRules::isWhitespace(*it))
                    break;
                --it;
            }
            if (it != std::end(result))
                result.erase(it, std::end(result));
        }

        result.shrink_to_fit();
        return result;
    }

    void
    Prototypes::fillStringPrototype(Object *object) noexcept {
        object->addProperty("charAt", Property{NativeFunctionDescriptor{
                0,
                "String.prototype.charAt",
                [] (NativeCallInformation call, const std::vector<Value> &params) -> Value {
                    if (call.thisArgument().index() == ValueIndex::Undefined || std::empty(params))
                        return "";
                    const auto index = params[0].toSignedInt();
                    auto string = call.thisArgument().toString();
                    if (index < 0 || static_cast<std::size_t>(index) >= std::size(string))
                        return "";
                    return std::string(1, string[static_cast<std::size_t>(index)]);
                }
        }});
        object->addProperty("trim", Property{NativeFunctionDescriptor{
                0,
                "String.prototype.trim",
                [] (NativeCallInformation call, const std::vector<Value> &) -> Value {
                    if (call.thisArgument().index() == ValueIndex::Undefined)
                        return "";
                    return trimString(call.thisArgument().toString(), StringTrimLocation::START_END);
                }
        }});
        object->addProperty("trimEnd", Property{NativeFunctionDescriptor{
                0,
                "String.prototype.trimEnd",
                [] (NativeCallInformation call, const std::vector<Value> &) -> Value {
                    if (call.thisArgument().index() == ValueIndex::Undefined)
                        return "";
                    return trimString(call.thisArgument().toString(), StringTrimLocation::END);
                }
        }});
        object->addProperty("trimStart", Property{NativeFunctionDescriptor{
                0,
                "String.prototype.trimStart",
                [] (NativeCallInformation call, const std::vector<Value> &) -> Value {
                    if (call.thisArgument().index() == ValueIndex::Undefined)
                        return "";
                    return trimString(call.thisArgument().toString(), StringTrimLocation::START);
                }
        }});
    }

} // namespace JS
