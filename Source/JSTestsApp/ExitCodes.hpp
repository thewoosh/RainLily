/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace JS::ExitCodes {

    constexpr int Success = 0;
    constexpr int IncorrectUsage = 1;
    constexpr int FailedToReadFile = 2;
    constexpr int LexerFailure = 3;
    constexpr int ASTGeneratorFailure = 4;
    constexpr int ASTInterpreterFailure = 5;

} // namespace JS::ExitCodes
