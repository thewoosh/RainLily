/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Include/spdlog.hpp"

#include "Source/IO/ReadFile.hpp"

#include "Source/JSTestsApp/App.hpp"
#include "Source/JSTestsApp/ExitCodes.hpp"

int main(int argc, const char *argv[]) {
    spdlog::set_level(spdlog::level::trace);

    JS::ExtensionSettings extensionSettings{};
    JS::GeneratorSettings generatorSettings{};
    JS::InterpreterSettings interpreterSettings{};

    std::string_view fileName{};

    for (std::size_t i = 1; i < static_cast<std::size_t>(argc); ++i) {
        const std::string_view arg{argv[i]};
        if (!arg.starts_with("--")) {
            fileName = arg;
            break;
        }

        if (arg == "--print-ast") {
            generatorSettings.printAST = true;
        } else if (arg == "--print-tokens") {
            generatorSettings.printTokens = true;
        } else if (arg == "--announce-declarations") {
            interpreterSettings.announceDeclarations = true;
        } else if (arg == "--announce-node-result") {
            interpreterSettings.announceNodeResult = true;
        } else {
            spdlog::error("Unknown argument: {}", arg);
            return JS::ExitCodes::IncorrectUsage;
        }
    }

    if (std::empty(fileName)) {
        spdlog::error("JSTestsApp: No file specified");
        return JS::ExitCodes::IncorrectUsage;
    }

    JS::IO::ReadFile file{fileName};
    auto contents = file.read();
    if (!contents.second) {
        spdlog::error("JSTestsApp: Failed to read file: {}", JS::toString(file.error()));
        return JS::ExitCodes::FailedToReadFile;
    }

    std::string_view lexerInput{std::data(contents.first), std::size(contents.first)};
    JS::JSTestsApp app{fileName, lexerInput, extensionSettings, generatorSettings, interpreterSettings};
    return app.start();
}
