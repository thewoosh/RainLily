/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>
#include <vector>

#include "Source/Settings/ExtensionSettings.hpp"
#include "Source/Settings/GeneratorSettings.hpp"
#include "Source/Settings/InterpreterSettings.hpp"
#include "Source/Base.hpp"
#include "Source/Lexer.hpp"

namespace JS {

    class JSTestsApp {
        std::string_view m_fileName{};
        std::string_view m_input{};
        ExtensionSettings m_extensionSettings{};
        GeneratorSettings m_generatorSettings{};
        InterpreterSettings m_interpreterSettings{};

        static void
        printTokenList(const std::vector<Token> &list) noexcept;

        [[nodiscard]] bool
        runASTGenerator(const std::vector<Token> &, AST::RootNode *) const noexcept;

        [[nodiscard]] bool
        runInterpreter(AST::RootNode &) noexcept;

        [[nodiscard]] bool
        runLexer(std::vector<Token> *out) noexcept;

    public:
        [[nodiscard]] inline constexpr
        JSTestsApp(std::string_view fileName,
                   std::string_view input,
                   ExtensionSettings extensionSettings,
                   GeneratorSettings generatorSettings,
                   InterpreterSettings interpreterSettings) noexcept
                   : m_fileName(fileName)
                   , m_input(input)
                   , m_extensionSettings(extensionSettings)
                   , m_generatorSettings(generatorSettings)
                   , m_interpreterSettings(interpreterSettings) {
        }

        [[nodiscard]] int
        start() noexcept;
    };

} // namespace JS
