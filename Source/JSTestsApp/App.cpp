/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/JSTestsApp/App.hpp"

#include "Include/spdlog.hpp"

#include "Source/AST/Node.hpp"
#include "Source/Expressions/AssignmentExpression.hpp"
#include "Source/Expressions/BiExpression.hpp"
#include "Source/Expressions/CallExpression.hpp"
#include "Source/Expressions/MemberExpression.hpp"
#include "Source/Expressions/PrimaryExpression.hpp"
#include "Source/MainApp/ExitCodes.hpp"
#include "Source/Utils/StringUtils.hpp"
#include "Source/ASTGenerator.hpp"
#include "Source/ASTInterpreter.hpp"
#include "Source/Lexer.hpp"
#include "Source/NativeCallInformation.hpp"

namespace JS {

    bool
    JSTestsApp::runASTGenerator(const std::vector<Token> &tokens, AST::RootNode *outRootNode) const noexcept {
        AST::Generator generator{tokens, m_fileName, m_input};
        if (!generator.generate())
            return false;
        *outRootNode = std::move(generator.rootNode());
        return true;
    }

    bool
    JSTestsApp::runInterpreter(AST::RootNode &rootNode) noexcept {
        AST::Interpreter interpreter{&rootNode, &m_extensionSettings, &m_interpreterSettings};

        auto &property = interpreter.globalObject().addProperty("assert", Property{NativeFunctionDescriptor{
                0, "console.debug",
                [&](NativeCallInformation, const std::vector<Value> &parameters) -> Value {
                    if (std::size(parameters) != 0 && !parameters[0].toBoolean()) {
                        spdlog::info("failed => {}", parameters[0].toBoolean());
                        interpreter.markStopFlag();
                    }
                    return JS::Undefined{};
                }
        }});
        property.setConfigurable(true)
                .setEnumerable(false)
                .setWritable(false);

        return interpreter.run();
    }

    bool
    JSTestsApp::runLexer(std::vector<Token> *out) noexcept {
        Lexer lexer{m_input};

        if (!lexer.parse())
            return false;

        *out = std::move(lexer.tokens());
        return true;
    }

    int
    JSTestsApp::start() noexcept {
        std::vector<Token> tokens;
        if (!runLexer(&tokens))
            return ExitCodes::LexerFailure;

        AST::RootNode rootNode;
        if (!runASTGenerator(tokens, &rootNode))
            return ExitCodes::ASTGeneratorFailure;

        if (!runInterpreter(rootNode))
            return ExitCodes::ASTGeneratorFailure;

        return ExitCodes::Success;
    }

} // namespace JS
