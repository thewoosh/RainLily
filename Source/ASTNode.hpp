/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <memory> // for std::unique_ptr
#include <optional>
#include <vector>

#include "Source/AST/Node.hpp"
#include "Source/Enums/ASTNodeType.hpp"
#include "Source/Enums/DeclarationType.hpp"
#include "Source/Declaration.hpp"

namespace JS::AST {

    struct DeclarationStatementNode
            : public Node {
    private:
        DeclarationType m_declarationType;
        std::vector<Declaration> m_declarations{};

    public:
        [[nodiscard]] inline explicit
        DeclarationStatementNode(DeclarationType type, std::vector<Declaration> &&declarations={}) noexcept
                : Node(NodeType::DECLARATION_STATEMENT)
                , m_declarationType(type)
                , m_declarations(std::move(declarations)) {
        }

        [[nodiscard]] inline constexpr DeclarationType
        declarationType() const noexcept {
            return m_declarationType;
        }

        [[nodiscard]] inline std::vector<Declaration> &
        declarations() noexcept {
            return m_declarations;
        }

        [[nodiscard]] inline const std::vector<Declaration> &
        declarations() const noexcept {
            return m_declarations;
        }
    };

    struct ExpressionStatementNode
            : public Node {
    private:
        std::unique_ptr<Expression> m_expression;

    public:
        [[nodiscard]] inline explicit
        ExpressionStatementNode(std::unique_ptr<Expression> &&expression) noexcept
            : Node(NodeType::EXPRESSION_STATEMENT)
            , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        data() noexcept {
            return m_expression;
        }

        [[nodiscard]] inline Expression *
        expression() noexcept {
            return m_expression.get();
        }

        [[nodiscard]] inline const Expression *
        expression() const noexcept {
            return m_expression.get();
        }
    };

    struct ReturnStatementNode
            : public Node {
    private:
        std::unique_ptr<Expression> m_expression;

    public:
        [[nodiscard]] inline explicit
        ReturnStatementNode(std::unique_ptr<Expression> &&expression) noexcept
                : Node(NodeType::RETURN_STATEMENT)
                , m_expression(std::move(expression)) {
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        data() noexcept {
            return m_expression;
        }

        [[nodiscard]] inline Expression *
        expression() noexcept {
            return m_expression.get();
        }

        [[nodiscard]] inline const Expression *
        expression() const noexcept {
            return m_expression.get();
        }
    };

    struct FunctionDeclarationNode
            : public ParentNode {
    private:
        FunctionDeclaration m_functionExpression;

    public:
        [[nodiscard]] inline explicit
        FunctionDeclarationNode(FunctionDeclaration &&functionExpression) noexcept
                : ParentNode(NodeType::FUNCTION_DECLARATION)
                , m_functionExpression(std::move(functionExpression)) {
        }

        [[nodiscard]] inline const FunctionDeclaration &
        functionExpression() const noexcept {
            return m_functionExpression;
        }
    };

    struct IfStatementNode
            : public Node {
    private:
        // if (...)
        std::unique_ptr<Expression> m_condition;

        // statement when condition resolves to true-ish
        std::unique_ptr<Node> m_firstStatement;

        // statement when first statement isn't executed, can be null
        std::unique_ptr<Node> m_secondStatement;

    public:
        [[nodiscard]] inline explicit
        IfStatementNode(std::unique_ptr<Expression> &&condition,
                        std::unique_ptr<Node> &&firstStatement,
                        std::unique_ptr<Node> &&secondStatement) noexcept
                : Node(NodeType::IF_STATEMENT)
                , m_condition(std::move(condition))
                , m_firstStatement(std::move(firstStatement))
                , m_secondStatement(std::move(secondStatement)) {
        }

        [[nodiscard]] inline Expression *
        condition() noexcept {
            return m_condition.get();
        }

        [[nodiscard]] inline const Expression *
        condition() const noexcept {
            return m_condition.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        conditionData() noexcept {
            return m_condition;
        }

        [[nodiscard]] inline Node *
        firstStatement() noexcept {
            return m_firstStatement.get();
        }

        [[nodiscard]] inline const Node *
        firstStatement() const noexcept {
            return m_firstStatement.get();
        }

        [[nodiscard]] inline Node *
        secondStatement() noexcept {
            return m_secondStatement.get();
        }

        [[nodiscard]] inline const Node *
        secondStatement() const noexcept {
            return m_secondStatement.get();
        }
    };

    struct WhileStatementNode
            : public Node {
    private:
        // while (...)
        std::unique_ptr<Expression> m_condition;

        // statement when condition resolves to true-ish
        std::unique_ptr<Node> m_body;

    public:
        [[nodiscard]] inline explicit
        WhileStatementNode(std::unique_ptr<Expression> &&condition,
                           std::unique_ptr<Node> &&body) noexcept
                : Node(NodeType::WHILE_STATEMENT)
                , m_condition(std::move(condition))
                , m_body(std::move(body))  {
        }

        [[nodiscard]] inline Node *
        body() noexcept {
            return m_body.get();
        }

        [[nodiscard]] inline const Node *
        body() const noexcept {
            return m_body.get();
        }

        [[nodiscard]] inline Expression *
        condition() noexcept {
            return m_condition.get();
        }

        [[nodiscard]] inline const Expression *
        condition() const noexcept {
            return m_condition.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        conditionData() noexcept {
            return m_condition;
        }
    };

    struct ForStatementNode
            : public Node {
    private:
        std::unique_ptr<DeclarationStatementNode> m_declaration{};
        std::unique_ptr<Expression> m_firstExpression{};
        std::unique_ptr<Expression> m_secondExpression;
        std::unique_ptr<Expression> m_thirdExpression;

        // statement when condition resolves to true-ish
        std::unique_ptr<Node> m_body;

    public:
        [[nodiscard]] inline explicit
        ForStatementNode(std::unique_ptr<Expression> &&firstExpression,
                         std::unique_ptr<Expression> &&secondExpression,
                         std::unique_ptr<Expression> &&thirdExpression,
                         std::unique_ptr<Node> &&body) noexcept
                : Node(NodeType::FOR_STATEMENT)
                , m_firstExpression(std::move(firstExpression))
                , m_secondExpression(std::move(secondExpression))
                , m_thirdExpression(std::move(thirdExpression))
                , m_body(std::move(body)) {
        }

        [[nodiscard]] inline explicit
        ForStatementNode(std::unique_ptr<DeclarationStatementNode> &&declaration,
                         std::unique_ptr<Expression> &&secondExpression,
                         std::unique_ptr<Expression> &&thirdExpression,
                         std::unique_ptr<Node> &&body) noexcept
                : Node(NodeType::FOR_STATEMENT)
                , m_declaration(std::move(declaration))
                , m_secondExpression(std::move(secondExpression))
                , m_thirdExpression(std::move(thirdExpression))
                , m_body(std::move(body)) {
        }

        [[nodiscard]] inline const Node *
        body() const noexcept {
            return m_body.get();
        }

        [[nodiscard]] inline Node *
        body() noexcept {
            return m_body.get();
        }

        [[nodiscard]] inline const DeclarationStatementNode *
        declaration() const noexcept {
            return m_declaration.get();
        }

        [[nodiscard]] inline const Expression *
        firstExpression() const noexcept {
            return m_firstExpression.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        firstExpressionData() noexcept {
            return m_firstExpression;
        }

        [[nodiscard]] inline const Expression *
        secondExpression() const noexcept {
            return m_secondExpression.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        secondExpressionData() noexcept {
            return m_secondExpression;
        }

        [[nodiscard]] inline const Expression *
        thirdExpression() const noexcept {
            return m_thirdExpression.get();
        }

        [[nodiscard]] inline std::unique_ptr<Expression> &
        thirdExpressionData() noexcept {
            return m_thirdExpression;
        }
    };

} // namespace JS::AST
