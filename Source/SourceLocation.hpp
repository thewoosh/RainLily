/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <cstddef>

namespace JS {

    struct SourceLocation {
    private:
        std::size_t m_line;
        std::size_t m_column;
        std::size_t m_length;

    public:
        [[nodiscard]] inline constexpr
        SourceLocation(std::size_t line, std::size_t column, std::size_t length=1) noexcept
                : m_line(line)
                , m_column(column)
                , m_length(length) {
        }

        [[nodiscard]] SourceLocation() noexcept = default;
        [[nodiscard]] SourceLocation(SourceLocation &&) noexcept = default;
        [[nodiscard]] SourceLocation(const SourceLocation &) noexcept = default;
        SourceLocation &operator=(SourceLocation &&) noexcept = default;
        SourceLocation &operator=(const SourceLocation &) noexcept = default;

        [[nodiscard]] inline constexpr std::size_t &
        column() noexcept {
            return m_column;
        }

        [[nodiscard]] inline constexpr const std::size_t &
        column() const noexcept {
            return m_column;
        }

        [[nodiscard]] inline constexpr std::size_t &
        length() noexcept {
            return m_length;
        }

        [[nodiscard]] inline constexpr const std::size_t &
        length() const noexcept {
            return m_length;
        }

        [[nodiscard]] inline constexpr std::size_t &
        line() noexcept {
            return m_line;
        }

        [[nodiscard]] inline constexpr const std::size_t &
        line() const noexcept {
            return m_line;
        }
    };

} // namespace JS
