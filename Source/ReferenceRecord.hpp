/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <variant>

#include <cstddef>

#include "Source/Object.hpp"
#include "Source/Property.hpp"

namespace JS {

    struct ReferenceRecord
            : public std::variant<std::nullptr_t, Undefined, std::shared_ptr<Object>, PropertyReference> {
    private:
        std::shared_ptr<Object> m_parent;

    public:
        template <typename...Args>
        [[nodiscard]] inline explicit
        ReferenceRecord(std::nullptr_t) noexcept
                : variant(nullptr) {
        }

        [[nodiscard]] inline explicit
        ReferenceRecord(Undefined undefined) noexcept
                : variant(undefined) {
        }

        [[nodiscard]] inline explicit
        ReferenceRecord(PropertyReference &&reference) noexcept
                : variant(std::move(reference))
                , m_parent(std::get<PropertyReference>(*this).parent()) {
        }

        [[nodiscard]] inline explicit
        ReferenceRecord(const PropertyReference &reference) noexcept
                : variant(reference)
                , m_parent(std::get<PropertyReference>(*this).parent()) {
        }

        template <typename...Args>
        [[nodiscard]] inline explicit
        ReferenceRecord(const std::shared_ptr<Object> &parent, Args &&...args) noexcept
                : variant(args...)
                , m_parent(parent) {
        }

        [[nodiscard]] inline bool
        isNull() const noexcept {
            return index() == 0;
        }

        [[nodiscard]] inline bool
        isUndefined() const noexcept {
            return index() == 1;
        }

        [[nodiscard]] inline bool
        isObject() const noexcept {
            return index() == 2;
        }

        [[nodiscard]] inline bool
        isProperty() const noexcept {
            return index() == 3;
        }

        [[nodiscard]] inline std::shared_ptr<Object> &
        parent() noexcept {
            return m_parent;
        }

        [[nodiscard]] inline const std::shared_ptr<Object> &
        parent() const noexcept {
            return m_parent;
        }

        [[nodiscard]] inline Value &
        toValue() noexcept {
            if (isProperty())
                return std::get<PropertyReference>(*this).property()->value();
            return std::get<std::shared_ptr<Object>>(*this)->value();
        }
    };

} // namespace JS
