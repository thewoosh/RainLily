/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Source/Enums/GeneratorError.hpp"

std::string_view
JS::getFormatString(JS::GeneratorError error) noexcept {
    switch (error) {
        case GeneratorError::NONE:
            return "NONE";

        case GeneratorError::FUNCTION_DECLARATION_EXPECT_ARGUMENT_NAME:
            return "Expected parameter name, instead got: {}";

        case GeneratorError::GENERIC_EXPECTED_GOT:
            return "Expected '{}', instead got: {}";
        case GeneratorError::GENERIC_EXPECTED_GOT_2:
            return "Expected '{}' or '{}', instead got: {}";

        case GeneratorError::LEXICAL_DECLARATION_NO_EQUALS_COMMA_OR_SEMICOLON:
            return "Expected '=', ',' or ';' after lexical/variable declaration of '{} {}', instead got: {}";

        case GeneratorError::NO_COMMA_BETWEEN_FUNCTION_DECLARATION_ARGUMENTS:
            return "Expected ',' between parameters, not {}";
        case GeneratorError::NO_IDENTIFIER_AFTER_LEXICAL_OR_VARIABLE_DECLARATION:
            return "Expected identifier after {}, instead got: {}";
        case GeneratorError::NO_IDENTIFIER_AFTER_MEMBER_OR_OPTIONAL_EXPRESSION:
            return "Expected identifier after member/optional-chaining expression, instead got: {}";
        case GeneratorError::NO_SEMICOLON_AFTER_DEBUGGER_STATEMENT:
            return "Expected ';' after 'debugger' statement, instead got: {}";
        case GeneratorError::NO_SEMICOLON_AFTER_EXPRESSION_STATEMENT:
            return "Expected ';' after expression statement, instead got: {}";

        case GeneratorError::OBJECT_EXPRESSION_UNEXPECTED_TOKEN:
            return "Expected ',' or '}}', instead got: {}";

        case GeneratorError::UNEXPECTED_EOF:
            return "Unexpected end-of-file!";
        case GeneratorError::UNEXPECTED_RIGHT_CURLY_BRACKET:
            return "Unexpected '}'! It doesn't match with any '{'";
        case GeneratorError::UNKNOWN_START_OF_EXPRESSION:
            return "Unknown start of expression: {}";

        default: return "(invalid)";
    }
}
