/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

namespace JS {

    enum class DeclarationErrorType {
        NONE,

        KEYWORD,
        RESERVED,
        BUILTIN,
        GLOBAL_DEFINITION,
        LEXICAL_DEFINITION,

        NON_CONFIGURABLE,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(DeclarationErrorType declarationError) noexcept {
        switch (declarationError) {
            case DeclarationErrorType::NONE: return "none";
            case DeclarationErrorType::KEYWORD: return "keyword";
            case DeclarationErrorType::RESERVED: return "reserved";
            case DeclarationErrorType::BUILTIN: return "built-in";
            case DeclarationErrorType::GLOBAL_DEFINITION: return "global-definition";
            case DeclarationErrorType::LEXICAL_DEFINITION: return "lexical-definition";
            case DeclarationErrorType::NON_CONFIGURABLE: return "non-configurable";
            default: return "(invalid)";
        }
    }

} // namespace JS
