/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class GeneratorError {
        NONE,
        FUNCTION_DECLARATION_EXPECT_ARGUMENT_NAME,
        GENERIC_EXPECTED_GOT,
        GENERIC_EXPECTED_GOT_2,
        LEXICAL_DECLARATION_NO_EQUALS_COMMA_OR_SEMICOLON,
        NO_COMMA_BETWEEN_FUNCTION_DECLARATION_ARGUMENTS,
        NO_IDENTIFIER_AFTER_LEXICAL_OR_VARIABLE_DECLARATION,
        NO_IDENTIFIER_AFTER_MEMBER_OR_OPTIONAL_EXPRESSION,
        NO_SEMICOLON_AFTER_DEBUGGER_STATEMENT,
        NO_SEMICOLON_AFTER_EXPRESSION_STATEMENT,
        OBJECT_EXPRESSION_UNEXPECTED_TOKEN,
        UNEXPECTED_EOF,
        UNEXPECTED_RIGHT_CURLY_BRACKET,
        UNKNOWN_START_OF_EXPRESSION,
    };

    [[nodiscard]] std::string_view
    getFormatString(GeneratorError) noexcept;

} // namespace JS
