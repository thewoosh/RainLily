/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Enums/AssignmentType.hpp"
#include "Source/Enums/ExpressionType.hpp"

#include <array>
#include <string_view>

#include <cstddef> // for std::size_t

namespace JS {

    enum class KeywordType {
        AWAIT, BREAK, CASE, CATCH, CLASS, CONST, CONTINUE, DEBUGGER, DEFAULT,
        DELETE, DO, ELSE, ENUM, EXPORT, EXTENDS, FALSE, FINALLY, FOR, FUNCTION,
        IF, IMPORT, IN, INSTANCEOF, NEW, KW_NULL, RETURN, SUPER, SWITCH, THIS,
        THROW, TRUE, TRY, TYPEOF, VAR, VOID, WHILE, WITH, YIELD,

        INVALID,
    };

    constexpr const std::array<std::string_view, 38> g_keywordList{
            "await", "break", "case", "catch", "class", "const", "continue",
            "debugger", "default", "delete", "do", "else", "enum", "export",
            "extends", "false", "finally", "for", "function", "if", "import",
            "in", "instanceof", "new", "null", "return", "super", "switch",
            "this", "throw", "true", "try", "typeof", "var", "void", "while",
            "with", "yield"
    };

    static_assert(std::size(g_keywordList) == static_cast<std::size_t>(KeywordType::INVALID));

    [[nodiscard]] inline constexpr KeywordType
    convertStringToKeyword(std::string_view string) noexcept {
        const auto it = std::find(std::cbegin(g_keywordList), std::cend(g_keywordList), string);

        if (it == std::cend(g_keywordList)) {
            return KeywordType::INVALID;
        }

        return static_cast<KeywordType>(std::distance(std::cbegin(g_keywordList), it));
    }

    [[nodiscard]] inline constexpr std::string_view
    toString(KeywordType type) noexcept {
        switch (type) {
            case KeywordType::AWAIT: return "await";
            case KeywordType::BREAK: return "break";
            case KeywordType::CASE: return "case";
            case KeywordType::CATCH: return "catch";
            case KeywordType::CLASS: return "class";
            case KeywordType::CONST: return "const";
            case KeywordType::CONTINUE: return "continue";
            case KeywordType::DEBUGGER: return "debugger";
            case KeywordType::DEFAULT: return "default";
            case KeywordType::DELETE: return "delete";
            case KeywordType::DO: return "do";
            case KeywordType::ELSE: return "else";
            case KeywordType::ENUM: return "enum";
            case KeywordType::EXPORT: return "export";
            case KeywordType::EXTENDS: return "extends";
            case KeywordType::FALSE: return "false";
            case KeywordType::FINALLY: return "finally";
            case KeywordType::FOR: return "for";
            case KeywordType::FUNCTION: return "function";
            case KeywordType::IF: return "if";
            case KeywordType::IMPORT: return "import";
            case KeywordType::IN: return "in";
            case KeywordType::INSTANCEOF: return "instanceof";
            case KeywordType::NEW: return "new";
            case KeywordType::KW_NULL: return "null";
            case KeywordType::RETURN: return "return";
            case KeywordType::SUPER: return "super";
            case KeywordType::SWITCH: return "switch";
            case KeywordType::THIS: return "this";
            case KeywordType::THROW: return "throw";
            case KeywordType::TRUE: return "true";
            case KeywordType::TRY: return "try";
            case KeywordType::TYPEOF: return "typeof";
            case KeywordType::VAR: return "var";
            case KeywordType::VOID: return "void";
            case KeywordType::WHILE: return "while";
            case KeywordType::WITH: return "with";
            case KeywordType::YIELD: return "yield";
            default: return "(invalid)";
        }
    }

} // namespace JS
