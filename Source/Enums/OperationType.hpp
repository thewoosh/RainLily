/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Enums/AssignmentType.hpp"
#include "Source/Enums/ExpressionType.hpp"

#include <string_view>

namespace JS {

    enum class OperationType {
        ADD,                  //   +
        SUBTRACT,             //   -
        MULTIPLY,             //   *
        DIVIDE,               //   /
        REMAINDER,            //   %
        EXPONENT,             //  **
        LEFT_SHIFT,           //  <<
        RIGHT_SHIFT,          //  >>
        UNSIGNED_RIGHT_SHIFT, // >>>
        BITWISE_AND,          //   &
        BITWISE_XOR,          //   ^
        BITWISE_OR,           //   |
        LOGICAL_AND,          //  &&
        LOGICAL_OR,           //  ||
        LOGICAL_NULLISH,      //  ??
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(OperationType type) noexcept {
        switch (type) {
            case OperationType::ADD: return "add";
            case OperationType::SUBTRACT: return "subtract";
            case OperationType::MULTIPLY: return "multiply";
            case OperationType::DIVIDE: return "divide";
            case OperationType::REMAINDER: return "remainder";
            case OperationType::EXPONENT: return "exponent";
            case OperationType::LEFT_SHIFT: return "left-shift";
            case OperationType::RIGHT_SHIFT: return "right-shift";
            case OperationType::UNSIGNED_RIGHT_SHIFT: return "unsigned-right-shift";
            case OperationType::BITWISE_AND: return "bitwise-and";
            case OperationType::BITWISE_XOR: return "bitwise-xor";
            case OperationType::BITWISE_OR: return "bitwise-or";
            case OperationType::LOGICAL_AND: return "logical-and";
            case OperationType::LOGICAL_OR: return "logical-or";
            case OperationType::LOGICAL_NULLISH: return "logical-nullish";
            default: return "(invalid)";
        }
    }

    [[nodiscard]] inline constexpr OperationType
    translateAssignmentTypeToOperator(AssignmentType type) {
        return static_cast<OperationType>(
                static_cast<std::size_t>(type) - static_cast<std::size_t>(AssignmentType::ADD)
        );
    }

    [[nodiscard]] inline constexpr OperationType
    translateExpressionTypeToOperator(ExpressionType type) {
        return static_cast<OperationType>(
                static_cast<std::size_t>(type) - static_cast<std::size_t>(ExpressionType::BI_ADD)
        );
    }

} // namespace JS
