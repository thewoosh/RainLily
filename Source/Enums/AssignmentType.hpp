/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Enums/PunctuatorType.hpp"

#include <string_view>

namespace JS {

    enum class AssignmentType {
        REGULAR,              //    =
        ADD,                  //   +=
        SUBTRACT,             //   -=
        MULTIPLY,             //   *=
        DIVIDE,               //   /=
        REMAINDER,            //   %=
        EXPONENT,             //  **=
        LEFT_SHIFT,           //  <<=
        RIGHT_SHIFT,          //  >>=
        UNSIGNED_RIGHT_SHIFT, // >>>=
        BITWISE_AND,          //   &=
        BITWISE_XOR,          //   ^=
        BITWISE_OR,           //   |=
        LOGICAL_AND,          //  &&=
        LOGICAL_OR,           //  ||=
        LOGICAL_NULLISH,      //  ??=
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(AssignmentType type) noexcept {
        switch (type) {
            case AssignmentType::REGULAR: return "regular";
            case AssignmentType::ADD: return "add";
            case AssignmentType::SUBTRACT: return "subtract";
            case AssignmentType::MULTIPLY: return "multiply";
            case AssignmentType::DIVIDE: return "divide";
            case AssignmentType::REMAINDER: return "remainder";
            case AssignmentType::EXPONENT: return "exponent";
            case AssignmentType::LEFT_SHIFT: return "left-shift";
            case AssignmentType::RIGHT_SHIFT: return "right-shift";
            case AssignmentType::UNSIGNED_RIGHT_SHIFT: return "unsigned-right-shift";
            case AssignmentType::BITWISE_AND: return "bitwise-and";
            case AssignmentType::BITWISE_XOR: return "bitwise-xor";
            case AssignmentType::BITWISE_OR: return "bitwise-or";
            case AssignmentType::LOGICAL_AND: return "logical-and";
            case AssignmentType::LOGICAL_OR: return "logical-or";
            case AssignmentType::LOGICAL_NULLISH: return "logical-nullish";
            default: return "(invalid)";
        }
    }

    static_assert(
            (static_cast<std::size_t>(AssignmentType::LOGICAL_NULLISH)
                - static_cast<std::size_t>(AssignmentType::REGULAR))
            == (static_cast<std::size_t>(PunctuatorType::ASSIGNMENT_LOGICAL_NULLISH)
                - static_cast<std::size_t>(PunctuatorType::ASSIGNMENT)));

    [[nodiscard]] inline constexpr AssignmentType
    translatePunctuatorToAssignmentType(PunctuatorType punctuatorType) {
        return static_cast<AssignmentType>(
                static_cast<std::size_t>(punctuatorType) - static_cast<std::size_t>(PunctuatorType::ASSIGNMENT)
        );
    }

} // namespace JS
