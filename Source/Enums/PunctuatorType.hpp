/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Base.hpp"

#include <string_view>

namespace JS {

    enum class PunctuatorType {
        INVALID,

        ADD,                                //    +
        SUBTRACT,                           //    -
        MULTIPLY,                           //    *
        DIVIDE,                             //    /
        REMAINDER,                          //    %
        EXPONENT,                           //   **
        LEFT_SHIFT,                         //   <<
        RIGHT_SHIFT,                        //   >>
        ZERO_FILL_RIGHT_SHIFT,              //  >>>
        BITWISE_AND,                        //    &
        BITWISE_XOR,                        //    ^
        BITWISE_OR,                         //    |
        LOGICAL_AND,                        //   &&
        LOGICAL_OR,                         //   ||
        LOGICAL_NULLISH,                    //   ??

        //
        // Unary
        //
        INCREMENT,                          //   ++
        DECREMENT,                          //   --
        BITWISE_NOT,                        //    ~

        //
        // Assignment
        //
        ASSIGNMENT,                         //    =
        ASSIGNMENT_ADD,                     //   +=
        ASSIGNMENT_SUBTRACT,                //   -=
        ASSIGNMENT_MULTIPLY,                //   *=
        ASSIGNMENT_DIVIDE,                  //   /=
        ASSIGNMENT_REMAINDER,               //   %=
        ASSIGNMENT_EXPONENT,                //  **=
        ASSIGNMENT_LEFT_SHIFT,              //  <<=
        ASSIGNMENT_RIGHT_SHIFT,             //  >>=
        ASSIGNMENT_UNSIGNED_RIGHT_SHIFT,    // >>>=
        ASSIGNMENT_BITWISE_AND,             //   &=
        ASSIGNMENT_BITWISE_XOR,             //   ^=
        ASSIGNMENT_BITWISE_OR,              //   |=
        ASSIGNMENT_LOGICAL_AND,             //  &&=
        ASSIGNMENT_LOGICAL_OR,              //  ||=
        ASSIGNMENT_LOGICAL_NULLISH,         //  ??=

        //
        // Comparison
        //
        EQUAL,                              //  ==
        NOT_EQUAL,                          //  !=
        STRICT_EQUAL,                       // ===
        STRICT_NOT_EQUAL,                   // !==
        GREATER_THAN,                       //   >
        GREATER_THAN_OR_EQUAL,              //  >=
        LESS_THAN,                          //   <
        LESS_THAN_OR_EQUAL,                 //  <=

        LEFT_PARENTHESIS,                   //   (
        RIGHT_PARENTHESIS,                  //   )
        LEFT_SQUARE_BRACKET,                //   {
        RIGHT_SQUARE_BRACKET,               //   }
        LEFT_CURLY_BRACKET,                 //   {
        RIGHT_CURLY_BRACKET,                //   }
        COMMA,                              //   ,
        SEMICOLON,                          //   ;
        FULL_STOP,                          //   .
        ELLIPSIS,                           // ...
        OPTIONAL_CHAINING,                  //  ?.
        COLON,                              //   :
        EXCLAMATION_MARK,                   //   !
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(PunctuatorType type) noexcept {
        switch (type) {
            case PunctuatorType::ADD: return "add";
            case PunctuatorType::SUBTRACT: return "subtract";
            case PunctuatorType::MULTIPLY: return "multiply";
            case PunctuatorType::DIVIDE: return "divide";
            case PunctuatorType::REMAINDER: return "remainder";
            case PunctuatorType::EXPONENT: return "exponent";
            case PunctuatorType::LEFT_SHIFT: return "left-shift";
            case PunctuatorType::RIGHT_SHIFT: return "right-shift";
            case PunctuatorType::ZERO_FILL_RIGHT_SHIFT: return "zero-fill-right-shift";
            case PunctuatorType::BITWISE_AND: return "bitwise-and";
            case PunctuatorType::BITWISE_XOR: return "bitwise-xor";
            case PunctuatorType::BITWISE_OR: return "bitwise-or";
            case PunctuatorType::LOGICAL_AND: return "logical-and";
            case PunctuatorType::LOGICAL_OR: return "logical-or";
            case PunctuatorType::LOGICAL_NULLISH: return "logical-nullish";

            case PunctuatorType::INCREMENT: return "increment";
            case PunctuatorType::DECREMENT: return "decrement";
            case PunctuatorType::BITWISE_NOT: return "bitwise-not";

            case PunctuatorType::ASSIGNMENT: return "assignment";
            case PunctuatorType::ASSIGNMENT_ADD: return "assignment-add";
            case PunctuatorType::ASSIGNMENT_SUBTRACT: return "assignment-subtract";
            case PunctuatorType::ASSIGNMENT_MULTIPLY: return "assignment-multiply";
            case PunctuatorType::ASSIGNMENT_DIVIDE: return "assignment-divide";
            case PunctuatorType::ASSIGNMENT_REMAINDER: return "assignment-remainder";
            case PunctuatorType::ASSIGNMENT_EXPONENT: return "assignment-exponent";
            case PunctuatorType::ASSIGNMENT_LEFT_SHIFT: return "assignment-left-shift";
            case PunctuatorType::ASSIGNMENT_RIGHT_SHIFT: return "assignment-right-shift";
            case PunctuatorType::ASSIGNMENT_UNSIGNED_RIGHT_SHIFT: return "assignment-unsigned-right-shift";
            case PunctuatorType::ASSIGNMENT_BITWISE_AND: return "assignment-bitwise-and";
            case PunctuatorType::ASSIGNMENT_BITWISE_XOR: return "assignment-bitwise-xor";
            case PunctuatorType::ASSIGNMENT_BITWISE_OR: return "assignment-bitwise-or";
            case PunctuatorType::ASSIGNMENT_LOGICAL_AND: return "assignment-logical-and";
            case PunctuatorType::ASSIGNMENT_LOGICAL_OR: return "assignment-logical-or";
            case PunctuatorType::ASSIGNMENT_LOGICAL_NULLISH: return "assignment-logical-nullish";

            case PunctuatorType::EQUAL: return "equal";
            case PunctuatorType::NOT_EQUAL: return "not-equal";
            case PunctuatorType::STRICT_EQUAL: return "strict-equal";
            case PunctuatorType::STRICT_NOT_EQUAL: return "strict-not-equal";
            case PunctuatorType::GREATER_THAN: return "greater-than";
            case PunctuatorType::GREATER_THAN_OR_EQUAL: return "greater-than-or-equal";
            case PunctuatorType::LESS_THAN: return "less-than";
            case PunctuatorType::LESS_THAN_OR_EQUAL: return "less-than-or-equal";

            case PunctuatorType::LEFT_PARENTHESIS: return "left-parenthesis";
            case PunctuatorType::RIGHT_PARENTHESIS: return "right-parenthesis";
            case PunctuatorType::LEFT_SQUARE_BRACKET: return "left-square-bracket";
            case PunctuatorType::RIGHT_SQUARE_BRACKET: return "right-square-bracket";
            case PunctuatorType::LEFT_CURLY_BRACKET: return "left-curly-bracket";
            case PunctuatorType::RIGHT_CURLY_BRACKET: return "right-curly-bracket";
            case PunctuatorType::COMMA: return "comma";
            case PunctuatorType::SEMICOLON: return "semicolon";
            case PunctuatorType::FULL_STOP: return "full-stop";
            case PunctuatorType::ELLIPSIS: return "ellipsis";
            case PunctuatorType::OPTIONAL_CHAINING: return "optional-chaining";
            case PunctuatorType::COLON: return "colon";
            case PunctuatorType::EXCLAMATION_MARK: return "exclamation-mark";

            default: return "(invalid)";
        }
    }

    [[nodiscard]] inline constexpr bool
    isAssignmentPunctuator(PunctuatorType type) noexcept {
        return type >= PunctuatorType::ASSIGNMENT
            && type <= PunctuatorType::ASSIGNMENT_LOGICAL_NULLISH;
    }

} // namespace JS
