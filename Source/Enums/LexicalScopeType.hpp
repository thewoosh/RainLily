/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class LexicalScopeType {
        FOR_STATEMENT,
        FUNCTION,
        GENERIC,
        ROOT,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(LexicalScopeType lexicalScopeType) {
        switch (lexicalScopeType) {
            case LexicalScopeType::FOR_STATEMENT: return "for-statement";
            case LexicalScopeType::FUNCTION: return "function";
            case LexicalScopeType::GENERIC: return "generic";
            case LexicalScopeType::ROOT: return "root";
            default: return "(invalid)";
        }
    }

} // namespace JS
