/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include "Source/Base.hpp"

#include <string_view>

namespace JS {

    enum class TokenType {
        FLOAT,
        INT,
        IDENTIFIER,
        KEYWORD,
        PUNCTUATOR,
        STRING,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(TokenType type) noexcept {
        switch (type) {
            case TokenType::FLOAT:      return "float";
            case TokenType::INT:        return "int";
            case TokenType::IDENTIFIER: return "identifier";
            case TokenType::KEYWORD:    return "keyword";
            case TokenType::PUNCTUATOR: return "punctuator";
            case TokenType::STRING:     return "string";
            default:                    return "(invalid)";
        }
    }

} // namespace JS
