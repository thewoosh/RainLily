/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <string_view>

namespace JS {

    enum class LexicalDeclarationType {
        NONE,
        CONST,
        LET,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(LexicalDeclarationType type) noexcept {
        switch (type) {
            case LexicalDeclarationType::CONST: return "const";
            case LexicalDeclarationType::LET:   return "let";
            default:                            return "(invalid)";
        }
    }

} // namespace JS
