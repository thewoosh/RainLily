/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class ExpressionType {
        INVALID,

        ASSIGNMENT,

        BI_ADD,
        BI_SUBTRACT,
        BI_MULTIPLY,
        BI_DIVIDE,
        BI_REMAINDER,
        EXPONENTIATION,
        BI_LEFT_SHIFT,
        BI_RIGHT_SHIFT,
        BI_UNSIGNED_RIGHT_SHIFT,

        // TODO bitwise and logical

        PRIMARY_IDENTIFIER,
        PRIMARY_SIGNED_INT,
        PRIMARY_STRING,
        UPDATE,

        BI_EQUAL,
        BI_GREATER_THAN,
        BI_GREATER_THAN_OR_EQUAL,
        BI_LESS_THAN,
        BI_LESS_THAN_OR_EQUAL,
        BI_NOT_EQUAL,
        BI_NOT_STRICT_EQUAL,
        BI_STRICT_EQUAL,

        CALL_MEMBER,
        CALL_PROPERTY_ACCESS,
        CALL_REGULAR,
        FUNCTION,
        MEMBER,
        OBJECT_LITERAL,

        LITERAL_NULL,
        LITERAL_FALSE,
        LITERAL_TRUE,

        UNI_PLUS,
        UNI_MINUS,
        UNI_BITWISE_NOT,
        UNI_LOGICAL_NOT,

        RELATIONAL_IN,
        RELATIONAL_INSTANCEOF,

        COALESCE,
        LOGICAL_OR,
        LOGICAL_AND,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(ExpressionType type) noexcept {
        switch (type) {
            case ExpressionType::ASSIGNMENT:                return "assignment";
            case ExpressionType::BI_ADD:                    return "bi-add";
            case ExpressionType::BI_DIVIDE:                 return "bi-divide";
            case ExpressionType::BI_EQUAL:                  return "bi-equal";
            case ExpressionType::BI_GREATER_THAN:           return "bi-greater-than";
            case ExpressionType::BI_GREATER_THAN_OR_EQUAL:  return "bi-greater-than-or-equal";
            case ExpressionType::BI_LEFT_SHIFT:             return "bi-left-shift";
            case ExpressionType::BI_LESS_THAN:              return "bi-less-than";
            case ExpressionType::BI_LESS_THAN_OR_EQUAL:     return "bi-less-than-or-equal";
            case ExpressionType::BI_MULTIPLY:               return "bi-multiply";
            case ExpressionType::BI_NOT_EQUAL:              return "bi-not-equal";
            case ExpressionType::BI_NOT_STRICT_EQUAL:       return "bi-not-strict-equal";
            case ExpressionType::BI_REMAINDER:              return "bi-remainder";
            case ExpressionType::BI_RIGHT_SHIFT:            return "bi-right-shift";
            case ExpressionType::BI_STRICT_EQUAL:           return "bi-strict-equal";
            case ExpressionType::BI_SUBTRACT:               return "bi-subtract";
            case ExpressionType::BI_UNSIGNED_RIGHT_SHIFT:   return "bi-unsigned-right-shift";
            case ExpressionType::CALL_MEMBER:               return "call-member";
            case ExpressionType::CALL_PROPERTY_ACCESS:      return "call-property-access";
            case ExpressionType::CALL_REGULAR:              return "call-regular";
            case ExpressionType::COALESCE:                  return "coalesce";
            case ExpressionType::EXPONENTIATION:            return "exponentiation";
            case ExpressionType::FUNCTION:                  return "function";
            case ExpressionType::LITERAL_NULL:              return "literal-null";
            case ExpressionType::LITERAL_FALSE:             return "literal-false";
            case ExpressionType::LITERAL_TRUE:              return "literal-true";
            case ExpressionType::LOGICAL_AND:               return "logical-and";
            case ExpressionType::LOGICAL_OR:                return "logical-or";
            case ExpressionType::MEMBER:                    return "member";
            case ExpressionType::OBJECT_LITERAL:            return "object-literal";
            case ExpressionType::PRIMARY_IDENTIFIER:        return "primary-identifier";
            case ExpressionType::PRIMARY_SIGNED_INT:        return "primary-signed-int";
            case ExpressionType::PRIMARY_STRING:            return "primary-string";
            case ExpressionType::RELATIONAL_IN:             return "relational-in";
            case ExpressionType::RELATIONAL_INSTANCEOF:     return "relational-instanceof";
            case ExpressionType::UPDATE:                    return "update";
            case ExpressionType::UNI_PLUS:                  return "uni-plus";
            case ExpressionType::UNI_MINUS:                 return "uni-minus";
            case ExpressionType::UNI_BITWISE_NOT:           return "uni-bitwise-not";
            case ExpressionType::UNI_LOGICAL_NOT:           return "uni-logical-not";
            default:                                        return "(invalid)";
        }
    }

} // namespace JS
