/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class VariableType {
        FUNCTION_ARGUMENT,
        LEXICAL_CONST,
        LEXICAL_LET,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(VariableType type) noexcept {
        switch (type) {
            case VariableType::FUNCTION_ARGUMENT: return "function-argument";
            case VariableType::LEXICAL_CONST:     return "lexical-const";
            case VariableType::LEXICAL_LET:       return "lexical-let";
            default:                              return "(invalid)";
        }
    }

} // namespace JS
