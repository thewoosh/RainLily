/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class TokenProcessError {
        NONE,

        END_OF_INTENDED_SCOPE_REACHED,

        GENERIC,
        MISPLACED_RIGHT_CURLY_BRACKET,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(TokenProcessError type) noexcept {
        switch (type) {
            case TokenProcessError::NONE:                           return "none";
            case TokenProcessError::END_OF_INTENDED_SCOPE_REACHED:  return "end-of-intended-scope-reached";
            case TokenProcessError::GENERIC:                        return "generic";
            case TokenProcessError::MISPLACED_RIGHT_CURLY_BRACKET:  return "misplaced-right-curly-bracket";
            default:                                                return "(invalid)";
        }
    }

} // namespace JS
