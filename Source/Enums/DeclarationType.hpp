/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class DeclarationType {
        FUNCTION,
        LEXICAL_CONST,
        LEXICAL_LET,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(DeclarationType type) noexcept {
        switch (type) {
            case DeclarationType::FUNCTION:      return "function";
            case DeclarationType::LEXICAL_CONST: return "lexical-const";
            case DeclarationType::LEXICAL_LET:   return "lexical-let";
            default:                             return "(invalid)";
        }
    }

} // namespace JS
