/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    namespace AST {
        enum class NodeType {
            ROOT,

            BLOCK,
            DEBUGGER_STATEMENT,
            DECLARATION_STATEMENT,
            EXPRESSION_STATEMENT,
            FOR_STATEMENT,
            FUNCTION_DECLARATION,
            FUNCTION_EXPRESSION,
            IF_STATEMENT,
            RETURN_STATEMENT,
            WHILE_STATEMENT,
        };
    } // namespace AST

    [[nodiscard]] inline constexpr std::string_view
    toString(AST::NodeType type) noexcept {
        switch (type) {
            case AST::NodeType::ROOT:                   return "root";
            case AST::NodeType::BLOCK:                  return "block";
            case AST::NodeType::DEBUGGER_STATEMENT:     return "debugger-statement";
            case AST::NodeType::DECLARATION_STATEMENT:  return "declaration-statement";
            case AST::NodeType::EXPRESSION_STATEMENT:   return "expression-statement";
            case AST::NodeType::FOR_STATEMENT:          return "for-statement";
            case AST::NodeType::FUNCTION_DECLARATION:   return "function-declaration";
            case AST::NodeType::FUNCTION_EXPRESSION:    return "function-expression";
            case AST::NodeType::IF_STATEMENT:           return "if-statement";
            case AST::NodeType::RETURN_STATEMENT:       return "return-statement";
            case AST::NodeType::WHILE_STATEMENT:        return "while-statement";
            default:                                    return "(invalid)";
        }
    }

} // namespace JS
