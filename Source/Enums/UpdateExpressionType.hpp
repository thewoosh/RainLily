/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <string_view>

namespace JS {

    enum class UpdateExpressionType {
        PRE_INCREMENT,
        PRE_DECREMENT,
        POST_INCREMENT,
        POST_DECREMENT,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(UpdateExpressionType type) noexcept {
        switch (type) {
            case UpdateExpressionType::PRE_INCREMENT:  return "pre-increment";
            case UpdateExpressionType::PRE_DECREMENT:  return "pre-decrement";
            case UpdateExpressionType::POST_INCREMENT: return "post-increment";
            case UpdateExpressionType::POST_DECREMENT: return "post-decrement";
            default:                                   return "(invalid)";
        }
    }

} // namespace JS
