/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <functional>

#include "Source/Base.hpp"

namespace JS {

    struct NativeFunctionDescriptor {
        using InternalType = std::function<Value(NativeCallInformation, const std::vector<Value> &)>;

    private:
        std::size_t m_argumentCount;
        std::string_view m_debugName;
        InternalType m_data;

    public:
        [[nodiscard]] inline
        NativeFunctionDescriptor(std::size_t argumentCount, std::string_view debugName, InternalType &&data) noexcept
                : m_argumentCount(argumentCount)
                , m_debugName(debugName)
                , m_data(std::forward<InternalType>(data)) {
        }

        [[nodiscard]] inline
        NativeFunctionDescriptor(std::size_t argumentCount, std::string_view debugName, const InternalType &data) noexcept
                : m_argumentCount(argumentCount)
                , m_debugName(debugName)
                , m_data(data) {
        }

        [[nodiscard]] inline constexpr std::size_t
        argumentCount() const noexcept {
            return m_argumentCount;
        }

        [[nodiscard]] inline constexpr std::string_view
        debugName() const noexcept {
            return m_debugName;
        }

        [[nodiscard]] inline const InternalType &
        data() const noexcept {
            return m_data;
        }
    };

} // namespace JS
