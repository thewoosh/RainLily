/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <map>
#include <string_view>
#include <utility> // for std::move

#include "Source/Property.hpp"
#include "Source/Value.hpp"

namespace JS {

    struct Object {
    private:
        Value m_value;

        std::map<std::string_view, Property> m_properties;

    public:
        [[nodiscard]] inline explicit
        Object(Value &&value) noexcept
                : m_value(std::move(value)) {
        }

        [[nodiscard]] inline explicit
        Object(const Value &value) noexcept
                : m_value(value) {
        }

        [[nodiscard]] Object() noexcept = default;
        [[nodiscard]] Object(Object &&) noexcept = default;
        [[nodiscard]] Object(const Object &) noexcept = default;
        Object &operator=(Object &&) noexcept = default;
        Object &operator=(const Object &) noexcept = default;

        void
        addOrSetProperty(std::string_view name, Property &&property) noexcept {
            auto it = m_properties.find(name);

            if (it != std::end(m_properties)) {
                it->second = std::move(property);
            } else {
                addProperty(name, std::move(property));
            }
        }

        void
        addOrSetProperty(std::string_view name, const Property &property) noexcept {
            auto it = m_properties.find(name);

            if (it != std::end(m_properties)) {
                it->second = property;
            } else {
                addProperty(name, property);
            }
        }

        inline Property &
        addProperty(std::string_view name, const Property &property) {
            return m_properties.emplace(name, property).first->second;
        }

        inline Property &
        addProperty(std::string_view name, Property &&property) {
            return m_properties.emplace(name, std::move(property)).first->second;
        }

        [[nodiscard]] inline const Property *
        findProperty(std::string_view name) const noexcept {
            auto it = m_properties.find(name);
            if (it == std::cend(m_properties))
                return nullptr;

            return &it->second;
        }

        [[nodiscard]] inline Property *
        findProperty(std::string_view name) noexcept {
            return const_cast<Property *>(
                static_cast<const Object *>(this)->findProperty(name)
            );
        }

        [[nodiscard]] inline constexpr std::map<std::string_view, Property> &
        properties() noexcept {
            return m_properties;
        }

        [[nodiscard]] inline constexpr const std::map<std::string_view, Property> &
        properties() const noexcept {
            return m_properties;
        }

        [[nodiscard]] inline bool
        value(Value &&newValue) noexcept {
            m_value = std::move(newValue);
            return true;
        }

        [[nodiscard]] inline constexpr Value &
        value() noexcept {
            return m_value;
        }

        [[nodiscard]] inline constexpr const Value &
        value() const noexcept {
            return m_value;
        }
    };

} // namespace JS
