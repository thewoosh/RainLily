assert(Math.abs(1) == 1);
assert(Math.abs(-1) == 1);
assert(Math.abs(Infinity) == Infinity);
assert(Math.abs(-Infinity) == Infinity);