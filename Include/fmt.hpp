/**
 * Copyright (C) 2021 Tristan Gerritsen <tristan-legal@outlook.com>
 * All Rights Reserved
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#include <fmt/core.h>

#if FMT_VERSION < 80000
#   define FMTLIB_WRAPPER
#else
#   define FMTLIB_WRAPPER fmt::runtime
#endif
