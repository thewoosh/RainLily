/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace text {

    template <typename...Args>
    inline constexpr bool
    isAnyOf(std::string_view sv, Args &&...args) noexcept {
        bool vals[] = {(args == sv)...};
        for (std::size_t i = 0; i < sizeof...(args); ++i)
            if (vals[i])
                return true;
        return false;
    }

    [[nodiscard]] inline constexpr std::string_view
    trim(std::string_view sv) noexcept {
        std::string_view::size_type begin = 0,
                                    end = std::size(sv);

        while (begin != end) {
            const auto character = sv[begin];
            if (character == ' ' || character == '\n' || character == '\r' || character == '\t') {
                ++begin;
                continue;
            }

            break;
        }

        while (end != begin) {
            const auto character = sv[end];
            if (character == ' ' || character == '\n' || character == '\r' || character == '\t') {
                --end;
                continue;
            }

            break;
        }

        return sv.substr(begin, end);
    }

} // namespace text
